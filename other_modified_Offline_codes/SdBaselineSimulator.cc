// $Id: SdBaselineSimulator.cc 34014 2021-01-29 13:32:51Z darko $

// SdBaselineSimulator - take fadc trace and add pedestal
// 25 Feb 2004 T. Ohnuki UCLA
// revised Jun 2004  T. Paul

#include <sstream>

#include "SdBaselineSimulator.h"

#include <evt/Event.h>

#include <det/Detector.h>

#include <sdet/SDetector.h>
#include <sdet/PMTConstants.h>

#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sevt/StationSimData.h>
#include <sevt/PMT.h>
#include <sevt/PMTSimData.h>

#include <fwk/CentralConfig.h>
#include <fwk/RandomEngineRegistry.h>

#include <utl/RandomEngine.h>
#include <utl/ErrorLogger.h>
#include <utl/Reader.h>

#include <CLHEP/Random/Randomize.h>

using namespace utl;
using namespace fwk;
using namespace sevt;
using namespace std;
using namespace SdBaselineSimulatorOG;


VModule::ResultFlag
SdBaselineSimulator::Init()
{
  fRandomEngine = &RandomEngineRegistry::GetInstance().Get(RandomEngineRegistry::eDetector);
  return eSuccess;
}


VModule::ResultFlag
SdBaselineSimulator::Run(evt::Event& event)
{
  INFO(".");
  SEvent& sEvent = event.GetSEvent();

  for (auto& station : sEvent.StationsRange()) {

    // skip station if no particles
    if (!station.HasSimData())
      continue;

    const sdet::Station& dStation = det::Detector::GetInstance().GetSDetector().GetStation(station);
    const unsigned int saturationValue = dStation.GetSaturationValue();
    const unsigned int traceLength = dStation.GetFADCTraceLength();

    for (auto& pmt : station.PMTsRange(sdet::PMTConstants::eAnyType)) {

      if (!pmt.HasSimData())
        continue;

      PMTSimData& pmtSim = pmt.GetSimData();

      if (!pmtSim.HasFADCTrace())
        continue;

      const sdet::PMT& dPMT = dStation.GetPMT(pmt);

      if (!pmtSim.HasFADCTrace(StationConstants::eTotalNoSaturation))
        pmtSim.MakeFADCTrace(StationConstants::eTotalNoSaturation);

      // HG
      const double pedHG = dPMT.GetBaseline(sdet::PMTConstants::eHighGain);
      const double noiseHG = dPMT.GetBaselineRMS(sdet::PMTConstants::eHighGain);
      TimeDistributionI& hgTrace = pmtSim.GetFADCTrace(PMTConstants::eHighGain, StationConstants::eTotal);
      TimeDistributionD& hgTrace_d = pmtSim.GetFADCTraceD(PMTConstants::eHighGain, StationConstants::eTotal);
      TimeDistributionI& hgUnSatTrace = pmtSim.GetFADCTrace(PMTConstants::eHighGain, StationConstants::eTotalNoSaturation);
      AddPedestal(hgTrace, hgTrace_d, hgUnSatTrace, saturationValue, pedHG, noiseHG, traceLength);

      // LG
      const double pedLG = dPMT.GetBaseline(sdet::PMTConstants::eLowGain);
      const double noiseLG = dPMT.GetBaselineRMS(sdet::PMTConstants::eLowGain);
      TimeDistributionI& lgTrace = pmtSim.GetFADCTrace(PMTConstants::eLowGain, StationConstants::eTotal);
      TimeDistributionD& lgTrace_d = pmtSim.GetFADCTraceD(PMTConstants::eLowGain, StationConstants::eTotal);
      TimeDistributionI& lgUnSatTrace = pmtSim.GetFADCTrace(PMTConstants::eLowGain, StationConstants::eTotalNoSaturation);
      AddPedestal(lgTrace, lgTrace_d, lgUnSatTrace, saturationValue, pedLG, noiseLG, traceLength);

    }

  }

  return eSuccess;
}


VModule::ResultFlag
SdBaselineSimulator::Finish()
{
  return eSuccess;
}


void
SdBaselineSimulator::AddPedestal(TimeDistributionI& trace,
                                 TimeDistributionD& doubleTrace,
                                 const unsigned int saturationValue,
                                 const double ped, const double noise,
                                 const unsigned int traceLength)
  const
{
  TimeDistributionI dummy(trace.GetBinning());
  AddPedestal(trace, doubleTrace, dummy, saturationValue, ped, noise, traceLength);
}


void
SdBaselineSimulator::AddPedestal(TimeDistributionI& trace,
                                 TimeDistributionD& doubleTrace,
                                 TimeDistributionI& unsaturatedTrace,
                                 const unsigned int saturationValue,
                                 const double pedestal, const double noise,
                                 const unsigned int traceLength)
  const
{
  const int firstBin = trace.GetStart() - traceLength/3;
  const int lastBin = trace.GetStop() + traceLength;
  
  if(noise < 1e-6){
    for (int i = firstBin; i <= lastBin; ++i) {
      const double value = max(doubleTrace.At(i) + pedestal, 0.);
      const int valueFloored = floor(value);
      doubleTrace.SetTime(i, min(value, double(saturationValue)));
      trace.SetTime(i, min(valueFloored, int(saturationValue)));
      unsaturatedTrace.SetTime(i, valueFloored);
    }
  }
  else{
    RandomEngine::RandomEngineType* const randomEngine = &fRandomEngine->GetEngine();
    const double ped = CLHEP::RandFlat::shoot(randomEngine, pedestal-0.5, pedestal+0.5);
    for (int i = firstBin; i <= lastBin; ++i) {
      const double diff = CLHEP::RandGauss::shoot(randomEngine, ped, noise);
      const double value = max(doubleTrace.At(i) + diff, 0.);
      const int valueFloored = floor(value);
      doubleTrace.SetTime(i, min(value, double(saturationValue)));
      trace.SetTime(i, min(valueFloored, int(saturationValue)));
      unsaturatedTrace.SetTime(i, valueFloored);
    }
  }
}
