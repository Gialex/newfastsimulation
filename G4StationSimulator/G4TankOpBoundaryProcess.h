// $Id: G4TankOpBoundaryProcess.h 33725 2020-07-23 14:44:13Z darko $
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4TankOpBoundaryProcess.h 33725 2020-07-23 14:44:13Z darko $
// GEANT4 tag $Name: geant4-09-00-patch-01 $
//
// 
////////////////////////////////////////////////////////////////////////
// Optical Photon Boundary Process Class Definition
////////////////////////////////////////////////////////////////////////
//
// File:        G4TankOpBoundaryProcess.hh
// Description: Discrete Process -- reflection/refraction at
//                                  optical interfaces
// Version:     1.1
// Created:     1997-06-18
// Modified:    2005-07-28 add G4ProcessType to constructor
//              1999-10-29 add method and class descriptors
//              1999-10-10 - Fill NewMomentum/NewPolarization in 
//                           DoAbsorption. These members need to be
//                           filled since DoIt calls 
//                           aParticleChange.SetMomentumChange etc.
//                           upon return (thanks to: Clark McGrew)
//
// Author:      Peter Gumplinger
//              adopted from work by Werner Keil - April 2/96
// mail:        gum@triumf.ca
//
// CVS version tag: 
////////////////////////////////////////////////////////////////////////

#ifndef _G4StationSimulatorOG_G4TankOpBoundaryProcess_h_
#define _G4StationSimulatorOG_G4TankOpBoundaryProcess_h_

#include "globals.hh"
#include "templates.hh"
#include "geomdefs.hh"
#include "Randomize.hh"
#include "G4Step.hh"
#include "G4VDiscreteProcess.hh"
#include "G4DynamicParticle.hh"
#include "G4Material.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
#include "G4OpticalPhoton.hh"
#include "G4TransportationManager.hh"
#include "G4PhysicalConstants.hh"

// Discrete Process -- reflection/refraction at optical interfaces.
// Class inherits publicly from G4VDiscreteProcess.                  


namespace G4StationSimulatorOG {

  enum G4TankOpBoundaryProcessStatus {
    Undefined,
    FresnelRefraction,
    FresnelReflection,
    TotalInternalReflection,
    LambertianReflection,
    LobeReflection,
    SpikeReflection,
    BackScattering,
    Absorption,
    Detection,
    NotAtBoundary,
    SameMaterial,
    StepTooSmall,
    NoRINDEX
  };


  class G4TankOpBoundaryProcess : public G4VDiscreteProcess {

  public:
    G4TankOpBoundaryProcess(const G4String& processName = "OpBoundary",
                            const G4ProcessType type = fOptical);

    virtual ~G4TankOpBoundaryProcess() { }

  public:
    // Returns true -> 'is applicable' only for an optical photon.
    virtual G4bool IsApplicable(const G4ParticleDefinition& particleType) override
    { return &particleType == G4OpticalPhoton::OpticalPhoton(); }

    // Returns infinity; i. e. the process does not limit the step, but sets
    // the 'Forced' condition for the DoIt to be invoked at every step.
    // However, only at a boundary will any action be taken.
    virtual G4double GetMeanFreePath(const G4Track& /*track*/,
                                     const G4double /*previousStepSize*/,
                                     G4ForceCondition* const condition) override
    { *condition = Forced; return DBL_MAX; }

    // This is the method implementing boundary processes.
    virtual G4VParticleChange* PostStepDoIt(const G4Track& track, const G4Step& step) override;

    // Set the optical surface model to be followed (glisur || unified).
    void SetModel(const G4OpticalSurfaceModel model) { fModel = model; }

   private:
    G4bool G4BooleanRand(const G4double prob) const { return G4UniformRand() < prob; }
    // Returns a random boolean variable with the specified probability

    G4ThreeVector G4IsotropicRand() const;

    G4ThreeVector G4LambertianRand(const G4ThreeVector& normal) const;

    G4ThreeVector G4PlaneVectorRand(const G4ThreeVector& normal) const;

    G4ThreeVector GetFacetNormal(const G4ThreeVector& momentum, const G4ThreeVector& normal) const;

    void DielectricMetal();

    void DielectricDielectric();

    void ChooseReflection();

    void DoAbsorption();

    void DoReflection();

  private:
    G4double fPhotonMomentum = 0;

    G4ThreeVector fOldMomentum;
    G4ThreeVector fOldPolarization;

    G4ThreeVector fNewMomentum;
    G4ThreeVector fNewPolarization;

    G4ThreeVector fGlobalNormal;
    G4ThreeVector fFacetNormal;

    G4Material* fMaterial1 = nullptr;
    G4Material* fMaterial2 = nullptr;

    G4OpticalSurface* fOpticalSurface = nullptr;

    G4double fRIndex1 = 0;
    G4double fRIndex2 = 0;

    /*G4double cost1 = 0;
    G4double cost2 = 0;
    G4double sint1 = 0;
    G4double sint2 = 0;*/

    G4TankOpBoundaryProcessStatus fStatus = Undefined;
    G4OpticalSurfaceModel fModel = glisur;
    G4OpticalSurfaceFinish fFinish = polished;

    G4double fReflectivity = 0;
    G4double fEfficiency = 0;
    G4double fProb_sl = 0;
    G4double fProb_ss = 0;
    G4double fProb_bs = 0;
    G4double kCarTolerance = 0;

  };

}


#endif
