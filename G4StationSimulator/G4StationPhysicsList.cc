// $Id: G4StationPhysicsList.cc 33729 2020-07-23 21:46:41Z darko $
#include "G4StationPhysicsList.h"

#include <G4ParticleDefinition.hh>
#include <G4ParticleTypes.hh>
#include <G4ParticleWithCuts.hh>
#include <G4ParticleTable.hh>
#include <G4Material.hh>
#include <G4MaterialTable.hh>
#include <G4ProcessManager.hh>
#include <G4ProcessVector.hh>
#include <G4IonConstructor.hh>
#include <G4ComptonScattering.hh>
#include <G4GammaConversion.hh>
#include <G4PhotoElectricEffect.hh>
#include <G4MuonMinusCapture.hh>
#include <G4eMultipleScattering.hh>
#include <G4MuMultipleScattering.hh>
#include <G4hMultipleScattering.hh>
#include <G4Decay.hh>
#include <G4eIonisation.hh>
#include <G4eBremsstrahlung.hh>
#include <G4eplusAnnihilation.hh>
#include <G4MuIonisation.hh>
#include <G4MuBremsstrahlung.hh>
#include <G4MuPairProduction.hh>
#include <G4hIonisation.hh>
#include <G4Cerenkov.hh>
#include <G4OpAbsorption.hh>
#include <G4OpRayleigh.hh>
#include <G4OpWLS.hh>
#include <G4LossTableManager.hh>
#include <G4EmSaturation.hh>
#include <G4Cerenkov.hh>
#include <G4Scintillation.hh>
#include <G4BaryonConstructor.hh>
#include <G4MesonConstructor.hh>
#include <G4LeptonConstructor.hh>
#include <G4PhysicalConstants.hh>
#include <G4SystemOfUnits.hh>
#include <G4OpBoundaryProcess.hh>


#include "G4TankOpBoundaryProcess.h"
#include "G4TankFastCerenkov.h"
#include "G4StationFastCerenkov.h"
#include "G4StationSimulator.h"

#include <iostream>

using namespace std;


namespace G4StationSimulatorOG {

  G4StationPhysicsList::G4StationPhysicsList(const bool fastCerenkov) :
    G4VUserPhysicsList(),
    fFastCerenkov(fastCerenkov),
    fG4StationSimulator(
      dynamic_cast<G4StationSimulator&>(fwk::RunController::GetInstance().GetModule("G4StationSimulatorOG"))
    )
  { }


  void
  G4StationPhysicsList::ConstructParticle()
  {
    ConstructBosons();
    ConstructLeptons();
    ConstructMesons();
    ConstructBaryons();
  }


  void
  G4StationPhysicsList::ConstructBosons()
  {
    // pseudo-particle: useful for testing
    G4Geantino::GeantinoDefinition();
    // gamma
    G4Gamma::GammaDefinition();
    // optical photon
    G4OpticalPhoton::OpticalPhotonDefinition();
  }


  void
  G4StationPhysicsList::ConstructLeptons()
  {
    G4LeptonConstructor::ConstructParticle();
  }


  void
  G4StationPhysicsList::ConstructMesons()
  {
    G4MesonConstructor::ConstructParticle();
  }


  void
  G4StationPhysicsList::ConstructBaryons()
  {
    G4BaryonConstructor::ConstructParticle();
  }


  void
  G4StationPhysicsList::ConstructProcess()
  {
    AddTransportation();
    ConstructGeneral();
    ConstructEM();
    ConstructOp();
  }


  void
  G4StationPhysicsList::ConstructGeneral()
  {
    G4Decay* const decayProcess = new G4Decay();

    G4ProcessManager& mumManager = *G4MuonMinus::MuonMinusDefinition()->GetProcessManager();
    mumManager.AddDiscreteProcess(decayProcess);
    mumManager.SetProcessOrdering(decayProcess, idxPostStep);
    mumManager.SetProcessOrdering(decayProcess, idxAtRest);

    G4ProcessManager& mupManager = *G4MuonPlus::MuonPlusDefinition()->GetProcessManager();
    mupManager.AddDiscreteProcess(decayProcess);
    mupManager.SetProcessOrdering(decayProcess, idxPostStep);
    mupManager.SetProcessOrdering(decayProcess, idxAtRest);
  }


  void
  G4StationPhysicsList::ConstructEM()
  {
    G4ParticleTable::G4PTblDicIterator* it = G4ParticleTable::GetParticleTable()->GetIterator();
    it->reset();
    while ((*it)()) {
      const G4ParticleDefinition& particle = *it->value();
      G4ProcessManager& pManager = *particle.GetProcessManager();
      const G4String& particleName = particle.GetParticleName();

      if (particleName == "gamma") {

        // gamma
        pManager.AddDiscreteProcess(new G4PhotoElectricEffect);
        pManager.AddDiscreteProcess(new G4ComptonScattering);
        pManager.AddDiscreteProcess(new G4GammaConversion);

      } else if (particleName == "e-") {

        // electron
        pManager.AddProcess(new G4eMultipleScattering, -1, 1, 1);
        pManager.AddProcess(new G4eIonisation,         -1, 2, 2);
        pManager.AddProcess(new G4eBremsstrahlung,     -1, 3, 3);

      } else if (particleName == "e+") {

        // positron
        pManager.AddProcess(new G4eMultipleScattering, -1,  1, 1);
        pManager.AddProcess(new G4eIonisation,         -1,  2, 2);
        pManager.AddProcess(new G4eBremsstrahlung,     -1,  3, 3);
        pManager.AddProcess(new G4eplusAnnihilation,    0, -1, 4);

      } else if (particleName == "mu+" || particleName == "mu-") {

        // muon
        pManager.AddProcess(new G4MuMultipleScattering, -1, 1, 1);
        pManager.AddProcess(new G4MuIonisation,         -1, 2, 2);
        pManager.AddProcess(new G4MuBremsstrahlung,     -1, 3, 3);
        pManager.AddProcess(new G4MuPairProduction,     -1, 4, 4);
        if (particleName == "mu-" && G4StationSimulator::fgMuCapture)
          pManager.AddProcess(new G4MuonMinusCapture);

      } else if (particle.GetPDGCharge() && particle.GetParticleName() != "chargedgeantino") {
        // all others charged particles except geantino
        pManager.AddProcess(new G4hMultipleScattering, -1, 1, 1);
        pManager.AddProcess(new G4hIonisation,         -1, 2, 2);
      }
    }
  }


  void
  G4StationPhysicsList::ConstructOp()
  {
    G4OpAbsorption* const absorption = new G4OpAbsorption();
    G4OpRayleigh* const rayleighScattering = new G4OpRayleigh();
    G4OpBoundaryProcess* const boundary = new G4OpBoundaryProcess();
    boundary->SetVerboseLevel(0);
    G4OpWLS* const wlsProcess = new G4OpWLS();
    wlsProcess->UseTimeProfile("exponential");

    G4ProcessManager& pManager =
      *G4OpticalPhoton::OpticalPhotonDefinition()->GetProcessManager();

    pManager.AddDiscreteProcess(absorption);
    pManager.AddDiscreteProcess(rayleighScattering);
    pManager.AddDiscreteProcess(boundary);
    pManager.AddDiscreteProcess(wlsProcess);

    G4ParticleTable::G4PTblDicIterator* it = G4ParticleTable::GetParticleTable()->GetIterator();

    // Cerenkov process
    if (!fFastCerenkov) {
      G4Cerenkov* const cerenkov = new G4Cerenkov("Cerenkov");
      // NOTE: At low energy, where dE/dX is large for the muon, it is best
      // to set the following number to something low
      const G4int maxNumPhotons = 3;
      cerenkov->SetTrackSecondariesFirst(true);
      cerenkov->SetMaxNumPhotonsPerStep(maxNumPhotons);
      cerenkov->SetVerboseLevel(0);
      // Scintillation process
      G4Scintillation* const scintProcess = new G4Scintillation("Scintillation");
      scintProcess->SetScintillationYieldFactor(1.);
      scintProcess->SetTrackSecondariesFirst(true);
      scintProcess->SetVerboseLevel(0);
      // Use Birks Correction in the Scintillation process
      // G4EmSaturation* emSaturation = G4LossTableManager::Instance()->EmSaturation();
      // ScintProcess->AddSaturation(emSaturation);

      it->reset();
      while ((*it)()) {
        const G4ParticleDefinition& particle = *it->value();
        G4ProcessManager& pManager = *particle.GetProcessManager();
        if (scintProcess->IsApplicable(particle)) {
          pManager.AddProcess(scintProcess);
          pManager.SetProcessOrderingToLast(scintProcess, idxAtRest);
          pManager.SetProcessOrderingToLast(scintProcess, idxPostStep);
        }
        if (cerenkov->IsApplicable(particle)) {
          pManager.AddProcess(cerenkov);
          pManager.SetProcessOrdering(cerenkov, idxPostStep);
        }
      }
    } else {
      //G4TankFastCerenkov* const cerenkov = new G4TankFastCerenkov("Cerenkov");
      G4StationFastCerenkov* const cerenkov = new G4StationFastCerenkov("Cerenkov");
      // NOTE: At low energy, where dE/dX is large for the muon, it is best
      // to set the following number to something low
      const G4int maxNumPhotons = 3;
      cerenkov->SetTrackSecondariesFirst(true);
      cerenkov->SetMaxNumPhotonsPerStep(maxNumPhotons);

      G4Electron::Electron()->GetProcessManager()->AddContinuousProcess(cerenkov);
      G4Positron::Positron()->GetProcessManager()->AddContinuousProcess(cerenkov);
      G4MuonMinus::MuonMinusDefinition()->GetProcessManager()->AddContinuousProcess(cerenkov);
      G4MuonPlus::MuonPlusDefinition()->GetProcessManager()->AddContinuousProcess(cerenkov);
    }
  }


  void
  G4StationPhysicsList::SetCuts()
  {
    const double meterConv = CLHEP::meter / utl::meter;
    defaultCutValue = fG4StationSimulator.fRangeCutDefault * meterConv;

    const double cutForGamma = fG4StationSimulator.fRangeCutGamma * meterConv;
    const double cutForElectron = fG4StationSimulator.fRangeCutElectron * meterConv;
    const double cutForPositron = fG4StationSimulator.fRangeCutPositron * meterConv;

    const double cutForMuonPlus = fG4StationSimulator.fRangeCutMuonPlus * meterConv;
    const double cutForMuonMinus = fG4StationSimulator.fRangeCutMuonMinus * meterConv;
    const double cutForOpticalPhotons = fG4StationSimulator.fRangeCutOpticalPhoton * meterConv;

    SetVerboseLevel(0);
    DumpCutValuesTable(0);

    // set cut values for gamma at first and for e- second and next for e+,
    // because some processes for e+/e- need cut values for gamma
    SetCutValue(cutForGamma, "gamma");
    SetCutValue(cutForElectron, "e-");
    SetCutValue(cutForPositron, "e+");
    SetCutValue(cutForMuonPlus, "mu+");
    SetCutValue(cutForMuonMinus, "mu-");
    SetCutValue(cutForOpticalPhotons, "opticalphoton");
  }

}
