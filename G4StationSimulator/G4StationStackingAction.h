#ifndef _G4StationSimulatorOG_G4StationStackingAction_h_
#define _G4StationSimulatorOG_G4StationStackingAction_h_

#include <utl/TabulatedFunction.h>

#include <G4UserStackingAction.hh>
#include <G4ClassificationOfNewTrack.hh>


class G4Track;

namespace sdet {
  class Station;
}

namespace sevt {
  class Station;
}

 namespace utl {
  class RandomEngine;
}

namespace G4StationSimulatorOG {

  class G4StationSimulator;


  /**
    \class G4StationStackingAction

    \brief Geant4 Stacking user action class

    \author Tom McCauley
    \date 08 April 2004
    \version $Id: G4StationStackingAction.h 33718 2020-07-21 20:20:07Z darko $
  */

  class G4StationStackingAction : public G4UserStackingAction {
  public:
    G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* const track) override;
    void NewStage() override { }
    void PrepareNewEvent() override { }
  };

}


#endif
