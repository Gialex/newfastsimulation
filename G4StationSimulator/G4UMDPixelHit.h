// $Id: G4UMDPixelHit.h 33725 2020-07-23 14:44:13Z darko $
#ifndef _G4StationSimulatorOG_G4UMDPixelHit_h_
#define _G4StationSimulatorOG_G4UMDPixelHit_h_

#include <G4VHit.hh>
#include <G4THitsCollection.hh>
#include <G4Allocator.hh>
#include <G4ThreeVector.hh>
#include <G4VPhysicalVolume.hh>


namespace G4StationSimulatorOG {

  class G4UMDPixelHit : public G4VHit {

  public:
    G4UMDPixelHit() { }
    virtual ~G4UMDPixelHit() { }

    //G4UMDPixelHit(const G4UMDPixelHit& right);
    //const G4UMDPixelHit& operator=(const G4UMDPixelHit &right);
    G4int operator==(const G4UMDPixelHit &right) const;

    /*inline void *operator new(size_t);
    inline void operator delete(void* hit);*/

    // This methods should be implemented to draw and print with customized style
    void Draw() override;
    void Print() override { }

  private:
    std::vector<G4double> fTimes;
    G4int fModuleId = 0;
    G4int fPixelId = 0;
    G4String fPixelName = "unnamed";
    G4bool fDrawIt = false;
    G4VPhysicalVolume* fHitPhysVol = nullptr;

  public:
    // Setter methods
    void AddHit(const G4double t) { fTimes.push_back(t); }
    void SetModuleId(const G4int id) { fModuleId = id; }
    void SetPixelId(const G4int id) { fPixelId  = id; }
    void SetPixelName(const G4String name) { fPixelName = name; }
    void SetDrawIt(const G4bool draw) { fDrawIt = draw; }
    void SetUMDPixelPhysVol(G4VPhysicalVolume* const physVol) { fHitPhysVol = physVol; }

    // Getters methods
    const std::vector<G4double>& GetTimes() const { return fTimes; }
    const G4String& GetPixelName() const { return fPixelName; }
    G4int GetPixelId() const { return fPixelId; }
    G4int GetModuleId() const { return fModuleId; }
    G4bool GetDrawIt() const { return fDrawIt; }
    G4VPhysicalVolume* GetUMDPixelPhysVol() const { return fHitPhysVol; }

  };


  typedef G4THitsCollection<G4StationSimulatorOG::G4UMDPixelHit> G4UMDPixelHitCollection;


  // Class for fast allocation of objects to the heap
  extern G4Allocator<G4StationSimulatorOG::G4UMDPixelHit> gG4UMDPixelHitAllocator;


#if 0
  // Operators new and delete must be provided
  inline
  void*
  G4UMDPixelHit::operator new(size_t /*size*/)
  {
    return gG4UMDPixelHitAllocator.MallocSingle();
  }


  inline
  void
  G4UMDPixelHit::operator delete(void* const aHit)
  {
    gG4UMDPixelHitAllocator.FreeSingle((G4UMDPixelHit*)aHit);
  }
#endif

}


#endif
