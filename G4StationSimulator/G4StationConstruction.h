#ifndef _G4StationSimulatorOG_G4StationConstruction_h_
#define _G4StationSimulatorOG_G4StationConstruction_h_

#include "G4StationSimulator.h"
#include "G4TankPMTAction.h"
#include "G4ScintillatorAction.h"
#include "G4UMDPixelAction.h"
#include "G4UMDScintStripAction.h"
#include "G4SoilAction.h"

#include <utl/TabulatedFunction.h>

#include <sdet/SDetector.h>
#include <mdet/MDetector.h>
#include <cdet/CDetector.h>

// Geant 4 header files
#include <G4VUserDetectorConstruction.hh>
#include <G4VSolid.hh>
#include <G4Box.hh>
#include <G4Colour.hh>
#include <G4Element.hh>
#include <G4ElementTable.hh>
#include <G4LogicalBorderSurface.hh>
#include <G4LogicalVolume.hh>
#include <G4Material.hh>
#include <G4MaterialTable.hh>
#include <G4PVPlacement.hh>
#include <G4SDManager.hh>
#include <G4Sphere.hh>
#include <G4ThreeVector.hh>
#include <G4Tubs.hh>
#include <G4VisAttributes.hh>
#include <G4GeometryManager.hh>
#include <G4PhysicalVolumeStore.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4SolidStore.hh>
#include <G4RunManager.hh>
#include <CLHEP/Units/SystemOfUnits.h>
#include <G4SubtractionSolid.hh>
#include <G4UnionSolid.hh>
#include <G4NistManager.hh>
#include "G4TankOpBoundaryProcess.h"
#include <G4Ellipsoid.hh>


namespace G4StationSimulatorOG {

  class UMDScintiBox {
  public:
    void Box(const mdet::Scintillator&, const double w, const double l, const double h)
    { fW = w; fL = l; fH = h; }
    double GetLength() const { return fL; }
    double GetWidth() const { return fW; }
    double GetHeight() const { return fH; }
    void GetDimensions(double& l, double& w, double& h) const
    { l = fL; w = fW; h = fH; }
  private:
     double fL = 0;
     double fW = 0;
     double fH = 0;
  };


  class UMDFiberTube {
  public:
    void Cylinder(const mdet::Fiber&, const double l, const double r)
    { fL = l; fR = r; }
    double GetLength() const { return fL; }
    double GetRadius() const { return fR; }
    void GetDimensions(double& l, double& r) const
    { l = fL; r = fR; }
  private:
     double fL = 0;
     double fR = 0;
  };


  // DV: this class is not well designed, not clear if it should be owning or not
  struct PMTConstruction {
    void
    Clear()
    {
      delete fPMT_log;
      fPMT_log = nullptr;
      delete fPMT_log1;
      fPMT_log1 = nullptr;
      delete fInterface_log;
      fInterface_log = nullptr;
      delete fDome_log;
      fDome_log = nullptr;
      delete fPMT_phys;
      fPMT_phys = nullptr;
      delete fPMT_phys1;
      fPMT_phys1 = nullptr;
      delete fDome_phys;
      fDome_phys = nullptr;
      delete fInterface_phys;
      fInterface_phys = nullptr;
      delete fInner_phys;
      fInner_phys = nullptr;
    }

    unsigned int fId = 0;
    int fPMTType = 0;
    G4ThreeVector fPMTPosition;

    G4LogicalVolume* fPMT_log = nullptr;
    G4LogicalVolume* fPMT_log1 = nullptr;
    G4LogicalVolume* fInterface_log = nullptr;
    G4LogicalVolume* fDome_log = nullptr;

    G4PVPlacement* fPMT_phys = nullptr;
    G4PVPlacement* fPMT_phys1 = nullptr;
    G4PVPlacement* fDome_phys = nullptr;
    G4PVPlacement* fInterface_phys = nullptr;

    G4PVPlacement* fInner_phys = nullptr;
  };


  class G4StationConstruction : public G4VUserDetectorConstruction {

  public:
    G4StationConstruction(const double simRadius, const bool umdEnabled, const double scintYield, const bool martaEnabled);
    virtual ~G4StationConstruction() { }

    virtual G4VPhysicalVolume* Construct() override;
    void GetDataForThisTank();

  private:
    G4NistManager* fNistManager = nullptr;

    G4StationSimulator& fG4StationSimulator;
    bool fHasSmallPMT = false;
    bool fHasScintillator = false;

    friend class G4TankFastCerenkov;
    friend class G4StationFastCerenkov;
    friend class G4StationSteppingAction;

    void SetRequiredParameters();
    void SetDetectorParameters();
    void SetUMDParameters();
    void SetMARTAParameters();
    void SetXMLParameters();
    void CreateElements();
    void CreateMaterials();
    void CleanupFreestore();
    G4VPhysicalVolume* CreateTank();

    void CreateAir();
    void CreateWater();
    void CreateVacuum();
    void CreatePyrex();
    void CreatePyrex1();
    void CreateLucite();
    void CreateInterface();
    void CreateHDPE();
    void CreateLiner();
    void CreateAluminium();
    void CreateDirt();
    void CreatePolystyrene();
    void CreateExpandedPolystyreneFoam();
    void CreateExtrudedPolystyreneFoam();
    void CreatePVC();
    void CreateScintillator();
    void CreateWLS();

    void CreateConcrete();
    void CreateAcrylic();
    void CreateSodaLimeGlass();
    void CreateR134A();
    void CreateBakelite();

    void SetScintPropertyTable();
    void SetScintCoatingProperties();

    void CreatePrimitives();
    void CreatePrimitivesUMD();
    void CreatePrimitivesMARTA();
    void CreateHall();
    void AssembleStation();
    void AssembleUMD();
    void AssembleMARTA();
    void AssembleTankSupport();

    bool fUMDEnabled = false;
    bool fMARTAEnabled = false;
    bool fElecBoxEnable = false;
    bool fGroundEnable = false;
    bool fSolarPanelEnable = false;
    double fSimulationRadius = 0;

    G4Element* elN = nullptr;
    G4Element* elO = nullptr;
    G4Element* elAl = nullptr;
    G4Element* elFe = nullptr;
    G4Element* elH = nullptr;
    G4Element* elSi = nullptr;
    G4Element* elB = nullptr;
    G4Element* elNa = nullptr;
    G4Element* elC = nullptr;
    G4Element* elCl = nullptr;
    G4Element* elTi = nullptr;
    G4Material* Air = nullptr;
    G4Material* Water = nullptr;
    G4Material* Vacuum = nullptr;
    G4Material* SiO2 = nullptr;
    G4Material* B2O2 = nullptr;
    G4Material* Na2O = nullptr;
    G4Material* Pyrex = nullptr;
    G4Material* Pyrex1 = nullptr;
    G4Material* CH3 = nullptr;
    G4Material* TiO2 = nullptr;
    G4Material* CH2 = nullptr;
    G4Material* C = nullptr;
    G4Material* CO2 = nullptr;
    G4Material* Lucite = nullptr;
    G4Material* Interface = nullptr;
    G4Material* HDPE = nullptr;
    G4Material* Al = nullptr;
    G4Material* Dirt = nullptr;
    G4Material* Polystyrene = nullptr;
    G4Material* ExpandedPolystyreneFoam = nullptr;
    G4Material* ExtrudedPolystyreneFoam = nullptr;
    G4Material* PVC = nullptr;
    G4Material* PPO = nullptr;
    G4Material* POPOP = nullptr;
    G4Material* ScintMat = nullptr;
    G4Material* ScintCoating = nullptr;
    G4Material* PMMA = nullptr;
    G4Material* Pethylene = nullptr;
    G4Material* FPethylene = nullptr;
    G4MaterialPropertiesTable* airMPT = nullptr;
    G4MaterialPropertiesTable* waterMPT = nullptr;
    G4MaterialPropertiesTable* pmtfaceMPT = nullptr;
    G4MaterialPropertiesTable* pmtfaceMPT1 = nullptr;
    G4MaterialPropertiesTable* pmtdomeMPT = nullptr;
    G4MaterialPropertiesTable* interfaceMPT = nullptr;
    G4MaterialPropertiesTable* linerMPT = nullptr;
    G4MaterialPropertiesTable* linerOpticalMPT = nullptr;
    G4Material* fG4_Concrete = nullptr;
    G4Material* fG4_Acrylic = nullptr;
    G4Material* fSoda_lime_glass = nullptr;
    G4Material* fR134a = nullptr;
    G4Material* fG4_Aluminum = nullptr;
    G4Material* fG4_Bakelite = nullptr;

    G4OpticalSurface* OpLinerSurface = nullptr;
    G4LogicalBorderSurface* topsurface = nullptr;
    G4LogicalBorderSurface* bottomsurface = nullptr;
    G4LogicalBorderSurface* sidesurface = nullptr;
    G4LogicalBorderSurface* umd_topsurface = nullptr;
    G4LogicalBorderSurface* umd_bottomsurface = nullptr;
    G4LogicalBorderSurface* umd_sidesurface = nullptr;

    G4Box* expHall_box = nullptr;
    G4Box* solarPanel_solid = nullptr;
    G4Box* elecBox_solid = nullptr;
    G4Box* scin_casing_solid = nullptr;
    G4Box* scin_sandwich_solid = nullptr;
    G4Box* scin_styro_solid = nullptr;
    G4Box* scin_solid = nullptr;
    G4Tubs* tank_solid = nullptr;
    G4Tubs* top_solid = nullptr;
    G4Tubs* side_solid = nullptr;
    G4Tubs* ground_solid = nullptr;
    std::vector<PMTConstruction> fPMTs; // container for all PMTs
    // standard PMTs
    G4Ellipsoid* inner_solid = nullptr;
    G4Ellipsoid* interface_in_aux = nullptr;
    G4Ellipsoid* interface_out_aux = nullptr;
    G4Ellipsoid* pmt_aux = nullptr;
    G4Ellipsoid* pmt_aux1 = nullptr;
    G4Ellipsoid* dome_in_aux = nullptr;
    G4Ellipsoid* dome_out_aux = nullptr;
    G4SubtractionSolid* pmt_solid = nullptr;
    G4SubtractionSolid* pmt_solid1 = nullptr;
    G4SubtractionSolid* interface_solid = nullptr;
    G4SubtractionSolid* dome_solid = nullptr;
    G4LogicalVolume* inner_log = nullptr;
    // small PMT
    //G4Tubs* s_inner_solid = nullptr;
    G4Tubs* s_pmt_solid = nullptr;
    G4Tubs* s_pmt_aux = nullptr;
    G4Tubs* s_pmt_aux1 = nullptr;
    G4SubtractionSolid* s_pmt_solid1 = nullptr;
    G4Tubs* s_interface_solid = nullptr;
    G4Tubs* s_dome_solid = nullptr;
    //G4LogicalVolume* s_inner_log = nullptr;

    // AMIGA-UMD
    // Solids and surfaces for modules (10m2+5m2)
    G4Box* umd_casing_solid_large = nullptr;
    G4Box* umd_casing_solid_small = nullptr;
    G4Box* umd_strip_solid_large = nullptr;
    G4Box* umd_strip_solid_small = nullptr;
    G4Box* umd_strip_surface_coat_large = nullptr;

    G4Box* umd_top_bot_coat_solid_large = nullptr;
    G4Box* umd_side_coat_solid_large = nullptr;
    G4Box* umd_top_bot_coat_solid_small = nullptr;
    G4Box* umd_side_coat_solid_small = nullptr;

    G4Box* umd_back_side_coat_solid = nullptr;

    G4Tubs* umd_fiber_core_solid_large = nullptr;
    G4Tubs* umd_fiber_clad1_solid_large = nullptr;
    G4Tubs* umd_fiber_clad2_solid_large = nullptr;
    G4Tubs* umd_fiber_core_solid_small = nullptr;
    G4Tubs* umd_fiber_clad1_solid_small = nullptr;
    G4Tubs* umd_fiber_clad2_solid_small = nullptr;

    G4Tubs* extra_fiber_solid = nullptr;
    G4Tubs* extra_clad1_solid = nullptr;
    G4Tubs* extra_clad2_solid = nullptr;

    G4Tubs* pixel_solid = nullptr;

    // Logical and physical volumes
    G4LogicalVolume* umd_casing_log_large = nullptr;
    G4LogicalVolume* umd_casing_log_small = nullptr;
    G4PVPlacement* umd_casing_phys_large = nullptr;
    G4PVPlacement* umd_casing_phys_small = nullptr;

    G4LogicalVolume* umd_strip_log_large = nullptr;
    G4LogicalVolume* umd_strip_log_small = nullptr;
    G4PVPlacement* umd_strip_phys_large = nullptr;
    G4PVPlacement* umd_strip_phys_small = nullptr;

    G4LogicalVolume* umd_fiber_core_log_small = nullptr;
    G4LogicalVolume* umd_fiber_clad1_log_small = nullptr;
    G4LogicalVolume* umd_fiber_clad2_log_small = nullptr;
    G4PVPlacement* umd_fiber_core_phys_small = nullptr;
    G4PVPlacement* umd_fiber_clad1_phys_small = nullptr;
    G4PVPlacement* umd_fiber_clad2_phys_small = nullptr;

    G4LogicalVolume* umd_fiber_core_log_large = nullptr;
    G4LogicalVolume* umd_fiber_clad1_log_large = nullptr;
    G4LogicalVolume* umd_fiber_clad2_log_large = nullptr;
    G4PVPlacement* umd_fiber_core_phys_large = nullptr;
    G4PVPlacement* umd_fiber_clad1_phys_large = nullptr;
    G4PVPlacement* umd_fiber_clad2_phys_large = nullptr;

    G4LogicalVolume* extra_fiber_log = nullptr;
    G4LogicalVolume* extra_clad1_log = nullptr;
    G4LogicalVolume* extra_clad2_log = nullptr;
    G4PVPlacement* extra_fiber_phy = nullptr;
    G4PVPlacement* extra_clad1_phy = nullptr;
    G4PVPlacement* extra_clad2_phy = nullptr;

    G4LogicalVolume* pixel_log = nullptr;
    G4PVPlacement* pixel_phy = nullptr;

    // Logical and physicals for surfaces
    G4LogicalVolume* umd_top_bot_coat_log_large = nullptr;
    G4LogicalVolume* umd_top_bot_coat_log_small = nullptr;
    G4PVPlacement* umd_top_coat_phys_large = nullptr;
    G4PVPlacement* umd_top_coat_phys_small = nullptr;
    G4PVPlacement* umd_bot_coat_phys_large = nullptr;
    G4PVPlacement* umd_bot_coat_phys_small = nullptr;

    G4LogicalVolume* umd_side_coat_log_large = nullptr;
    G4LogicalVolume* umd_side_coat_log_small = nullptr;
    G4PVPlacement* umd_side1_coat_phys_large = nullptr;
    G4PVPlacement* umd_side1_coat_phys_small = nullptr;
    G4PVPlacement* umd_side2_coat_phys_large = nullptr;
    G4PVPlacement* umd_side2_coat_phys_small = nullptr;

    G4LogicalVolume* umd_back_side_coat_log = nullptr;
    G4PVPlacement* umd_back_side_coat_phys = nullptr;

    G4LogicalVolume* umd_strip_coat_log_large = nullptr;
    G4LogicalVolume* umd_strip_coat_log_small = nullptr;
    G4VPhysicalVolume* umd_strip_coat_phys_large = nullptr;
    G4VPhysicalVolume* umd_strip_coat_phys_small = nullptr;

    G4OpticalSurface* fUMDScintSkinSurf = nullptr;
    G4OpticalSurface* fUMDScintSkinSurfBack = nullptr;

    // MARTA
    // Solids
    G4Box* rpc_solid = nullptr;
    G4Box* glass_solid = nullptr;
    G4Box* gas_solid = nullptr;
    G4Box* pcb_solid = nullptr;
    G4Box* spacer_solid = nullptr;
    G4SubtractionSolid* AlBox_solid = nullptr;

    // Logical volumes
    G4LogicalVolume* al_box_log = nullptr;
    G4LogicalVolume* rpc_log = nullptr;
    G4LogicalVolume* gas_log = nullptr;
    G4LogicalVolume* glass_log = nullptr;
    G4LogicalVolume* pcb_log = nullptr;
    G4LogicalVolume* spacer_log = nullptr;
    // end MARTA

    G4LogicalVolume* expHall_log = nullptr;
    G4LogicalVolume* tank_log = nullptr;
    G4LogicalVolume* top_log = nullptr;
    G4LogicalVolume* bottom_log = nullptr;
    G4LogicalVolume* side_log = nullptr;

    G4LogicalVolume* ground_log = nullptr;
    G4LogicalVolume* solarPanel_log = nullptr;
    G4LogicalVolume* elecBox_log = nullptr;
    G4LogicalVolume* scin_casing_log = nullptr;
    G4LogicalVolume* scin_sandwich_log = nullptr;
    G4LogicalVolume* scin_styro_log = nullptr;
    G4LogicalVolume* scin_log = nullptr;

    G4PVPlacement* expHall_phys = nullptr;
    G4PVPlacement* tank_phys = nullptr;
    G4PVPlacement* top_phys = nullptr;
    G4PVPlacement* bottom_phys = nullptr;
    G4PVPlacement* side_phys = nullptr;

    G4PVPlacement* ground_phys = nullptr;
    G4PVPlacement* solarPanel_phys = nullptr;
    G4PVPlacement* elecBox_phys = nullptr;
    G4PVPlacement* scin_casing_phys = nullptr;
    G4PVPlacement* scin_sandwich_phys = nullptr;
    G4PVPlacement* scin_styro_phys = nullptr;
    G4PVPlacement* scin_phys1 = nullptr;
    G4PVPlacement* scin_phys2 = nullptr;

    static G4ThreeVector fgTankCenter;

    double fFaceRadius = 0;
    double fFaceRadiusz = 0;
    double fFaceActiveRadius = 0;

    double fFaceRadius_SPMT = 0;
    double fFaceActiveRadius_SPMT = 0;
    double fWindowRadius_SPMT = 0;

    double fExpHall_x = 0;
    double fExpHall_y = 0;
    double fExpHall_z = 0;
    double fGroundThickness = 0;

    static double fgTankRadius;
    static double fgTankHalfHeight;
    static double fgTankThickness;
    double fTankPos_x = 0;
    double fTankPos_y = 0;
    double fTankPos_z = 0;

    double fPmtRmin = 0;
    static double fgPmtRmax;
    double fInterfaceRmin = 0;
    static double fgInterfaceRmax;
    double fDomeRmin = 0;
    static double fgDomeRmax;
    double fInterfaceThickness = 0;
    double fGlassThickness = 0;
    double fDomeThickness = 0;

    static double fPmtRmin_SPMT;
    static double fPmtRmax_SPMT;
    static double fInterfaceRadius_SPMT;
    static double fDomeRadius_SPMT;
    static double fInterfaceThickness_SPMT;
    static double fGlassThickness_SPMT;
    static double fDomeThickness_SPMT;

    double fMinPhi = 0;
    double fMaxPhi = 0;
    double fMinTheta = 0;
    double fMaxTheta = 0;
    double fPmtRzmin = 0;
    static double fgPmtRzmax;
    double fInterfaceRzmin = 0;
    static double fgInterfaceRzmax;
    double fDomeRzmin = 0;
    static double fgDomeRzmax;

    double fPmtZ_SPMT = 0;
    double fInterfaceZ_SPMT = 0;
    double fDomeZ_SPMT = 0;

    //double fAlpha;
    static double fgHeightz;

    utl::TabulatedFunction fPmtfaceRINDEX;
    static utl::TabulatedFunction fgInterfaceRINDEX;
    static utl::TabulatedFunction fgPmtdomeRINDEX;
    utl::TabulatedFunction fPmtfaceABSORPTION;
    static utl::TabulatedFunction fgInterfaceABSORPTION;
    static utl::TabulatedFunction fgPmtdomeABSORPTION;

    utl::TabulatedFunction fPmtSfaceRINDEX;
    utl::TabulatedFunction fSInterfaceRINDEX;
    utl::TabulatedFunction fPmtSdomeRINDEX;
    utl::TabulatedFunction fPmtSfaceABSORPTION;
    utl::TabulatedFunction fSInterfaceABSORPTION;
    utl::TabulatedFunction fPmtSdomeABSORPTION;
    static double fgSIGMA_ALPHA;
    static utl::TabulatedFunction fgLinerREFLECTIVITY;
    static utl::TabulatedFunction fgLinerSPECULARLOBECONSTANT;
    static utl::TabulatedFunction fgLinerSPECULARSPIKECONSTANT;
    utl::TabulatedFunction fLinerTYVEK_RINDEX;
    utl::TabulatedFunction fLinerABSORPTION;
    static utl::TabulatedFunction fgLinerBACKSCATTERCONSTANT;
    static utl::TabulatedFunction fgWaterABSORPTION;
    utl::TabulatedFunction fWaterRINDEX;

    // Solar panel setup
    //------------------

    double fSolarPanelLength = 0;
    double fSolarPanelWidth = 0;
    double fSolarPanelThickness = 0;

    double fSolarPanelX = 0;
    double fSolarPanelY = 0;
    double fSolarPanelZ = 0;
    double fSolarPanelTiltAngle = 0;

    // electronics box setup
    // ---------------------

    double fElecBoxLength = 0;
    double fElecBoxWidth = 0;
    double fElecBoxThickness = 0;

    double fElecBoxX = 0;
    double fElecBoxY = 0;
    double fElecBoxZ = 0;
    double fElecBoxTiltAngle = 0;

    // PMT setup
    //----------
    /*
      The "PMT setup" consists of the dome,
      a thin interface layer, and the face.
      The pmt domes are hemispherical
      shells with the same surface area as the
      the actual domes.
      The pmt "faces" are hemispherical domes
      that have the same active area as the
      photocathodes.
      The origin of the following
      coordinates is the center of the
      tank -> (0.0, 0.0, 60.0) cm
      Why? Because in order to have the
      optical properties of the dome, etc.
      work correctly, the volumes must
      be defined and placed in the tank volume
      (as opposed to the experimental hall volume)
      Since the dome, interface, and face are
      defined as hemispherical shells placed in
       one another, they have they same
       x-y-z coordinates.
    */

    // PMT Optical properties

    /*
      Tyvek Refractive Index on liner surface vs. momentum
      (NOTE: refractive index is set to zero for the
      following reason.  The only surfaces that work
      with the unified model are ground and
      groundbackpainted (others are limited to either
      purely specular or Lambertian reflection.)  Both
      ground and groundbackpainted necessarily calculate
      transmissin coefficients based on index of refraction
      of the next material.  For groundbackpainted, you
      are allowd to define a 'thin' layer of dielectric
      between the ground surface and the painted one.
      If this is defined to be zero, then all the photons
      undergo total internal reflection from the ground
      surface and the unfied model is used.  Note that
      this is a kludge, and the author of this code
      needs to be contacted to discuss providing some
      additional flexibility in the optical model.
    */

    // scintillator setup
    // ------------------

    double fScintillatorBarLength = 0;
    double fScintillatorBarWidth = 0;
    double fScintillatorBarThickness = 0;
    unsigned int fNScintillatorBars = 0;
    double fScintillatorGap = 0;
    double fScintillatorHousingLength = 0;
    double fScintillatorHousingWidth = 0;
    double fScintillatorHousingThickness = 0;
    double fScintillatorCasingThickness = 0;
    double fScintillatorSandwichPanelThickness = 0;
    G4ThreeVector fScintillatorPosition;
    double fScintillatorCasingPadding = 0;

    // UMD
    std::map<mdet::Module::AreaKind, double> fModAreaLenghts;
    std::map<unsigned int, double> fFiberLenghts;
    unsigned int fUMDScints = 0;
    double fUMDManifoldL = 0;
    double fUMDThicknessCasing = 0;
    double fCoatingThickness = 0;
    double fCladingThickness = 0;
    double fUMDScintsW = 0;
    double fUMDScintsH = 0;
    double fUMDFiberRadius = 0;
    double fPixelL = 0;
    double fScintYield = 0;  // for visualization or real simulation

    // MARTA
    // RPC parameters
    // Concrete tank support
    // ---------------------

    // New precast design
    bool fMakeTankSupport = false;

    G4ThreeVector fTankSupportTopSlabDimensions;
    G4ThreeVector fTankSupportOuterFootDimensions;
    G4ThreeVector fTankSupportOuterFootBaseDimensions;
    double fTankSupportOuterFootDistanceToCenter = 0;
    G4ThreeVector fTankSupportCentralFootDimensions;
    G4ThreeVector fTankSupportCentralFootBaseDimensions;

    std::vector<G4ThreeVector> fRPCPositions;
    std::vector<G4double> fRPCRotations;
    G4ThreeVector fAlBoxInnerDimensions;
    G4ThreeVector fAlBoxThickness;
    double fPCBThickness = 0;
    double fSpacerThickness = 0;

    double fRPCsizeX = 0;
    double fRPCsizeY = 0;

  };

}


#endif
