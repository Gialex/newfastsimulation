#ifndef _G4StationSimulatorOG_G4StationSimulator_h_
#define _G4StationSimulatorOG_G4StationSimulator_h_

#include "G4StationStackingAction.h"

#include <fwk/VModule.h>

#include <utl/TimeDistribution.h>
#include <utl/TabulatedFunction.h>

#include <sevt/SEvent.h>
#include <sevt/StationSimData.h>

#include <mevt/MEvent.h>
#include <mevt/Counter.h>

#include <cevt/CEvent.h>


class G4RunManager;
class G4UImanager;
class G4VisManager;

namespace sdet {
  class Station;
}

namespace mdet {
  class Counter;
}

namespace mevt {
  class Counter;
}

namespace G4StationSimulatorOG {

  enum SignalSeparationMode {
    eStandard,
    eUniversality
  };

  class G4StationPrimaryGenerator;
  class G4StationStackingAction;
  class G4StationConstruction;
  class G4StationPhysicsListCustomization;

  // container of station id, weight
  typedef std::map<int, double> AccumulatedWeights;


  /**
    \class G4StationSimulator

    \brief class that handles Geant4 SD Station simulation adopted from G4TankSimulator

    \authors D. Schmidt, E. Mayotte and T. McCauley (altered by P. Gabriel for MARTA integration)
    \date 07 October 2003
    \version $Id: G4StationSimulator.h 33725 2020-07-23 14:44:13Z darko $
    \ingroup SDSimModules
  */

  class G4StationSimulator : public fwk::VModule {

  public:
    G4StationSimulator() { }
    virtual ~G4StationSimulator() { }

    fwk::VModule::ResultFlag Init();
    fwk::VModule::ResultFlag Run(evt::Event& theEvent);
    fwk::VModule::ResultFlag Finish();

    std::string GetSVNId() const
    { return std::string("$Id: G4StationSimulator.h 33725 2020-07-23 14:44:13Z darko $"); }

    class Setup {
      // Resource acquisition is initialization (RAII) via object G4StationSimulator::fgCurrent
    public:
      Setup()
      {
        if (fgLock)
          throw utl::DoesNotComputeException("Setup already locked by another object!");
        fgLock = true;
        Clear();
      }

      template<class T>
      Setup(T& x) : Setup() { Set(x); }

      ~Setup() { Clear(); fgLock = false; }

      void Set(const sdet::Station& s) const { G4StationSimulator::fgCurrent.fDetectorStation = &s; }

      void Set(sevt::Station& s) const { G4StationSimulator::fgCurrent.fEventStation = &s; }

      void Set(const mdet::Counter& c) const { G4StationSimulator::fgCurrent.fDetectorUMDCounter = &c; }

      void Set(mevt::Counter& c) const { G4StationSimulator::fgCurrent.fEventUMDCounter = &c; }

      void Set(utl::Particle& p) const { G4StationSimulator::fgCurrent.fParticle = &p; }

      void Clear() const { G4StationSimulator::fgCurrent.Clear(); }

    private:
      static bool fgLock;
    };

    class Current {
    public:
      // public getters prevent direct assigment to the members
      int GetStationID() const { return fDetectorStation->GetId(); }
      const sdet::Station& GetDetectorStation() const { return *fDetectorStation; }
      sevt::Station& GetEventStation() const { return *fEventStation; }
      const mdet::Counter& GetDetectorUMDCounter() const { return *fDetectorUMDCounter; }
      mevt::Counter& GetEventUMDCounter() const { return *fEventUMDCounter; }
      utl::Particle& GetParticle() const { return *fParticle; }

    private:
      const sdet::Station* fDetectorStation = nullptr;
      sevt::Station* fEventStation = nullptr;
      const mdet::Counter* fDetectorUMDCounter = nullptr;
      mevt::Counter* fEventUMDCounter = nullptr;
      utl::Particle* fParticle = nullptr;

      void
      Clear()
      {
        fDetectorStation = nullptr;
        fEventStation = nullptr;
        fDetectorUMDCounter = nullptr;
        fEventUMDCounter = nullptr;
        fParticle = nullptr;
      }

      friend class Setup;
    };

  private:
    void ConstructTraces(sevt::Station& station) const;
    /// peTime in Auger units!
    void AddPhoton(const int nPMT, const double peTime) const;

    // Method to add photons to UMD
    void AddUMDPhoton(const size_t modId, const size_t pixId, const double peTime) const;
    // Method to add injected particles to UMD
    void AddInjectedParticle(const size_t modId, const size_t pixId, const utl::Particle& injectedParticle) const;

    // Method to add simulation particles to CEvent simulation data
    void FillRPCSimData(cevt::StationSimData& simData) const;

    fwk::VModule::ResultFlag RunFull(evt::Event& event);
    fwk::VModule::ResultFlag RunFast(evt::Event& event);

    bool IsUMDEnabled() const { return fUMDEnabled; }
    bool OnlyMuonsInUMD() const { return fUMDOnlyMuons; }
    bool IsMARTAEnabled() const { return fMARTAEnabled; }

    // static stuff
    static Current fgCurrent;

    static bool fgMuCapture;

    //

    G4RunManager* fRunManager = nullptr;
    G4UImanager* fUImanager = nullptr;
    G4VisManager* fVisManager = nullptr;
    G4StationStackingAction* fStackingAction = nullptr;

    bool fGeoVisOn = false;
    bool fTrajVisOn = false;
    unsigned int fVerbosity = 0;
    std::string fRenderFile;
    bool fUseGlobalPhysicsList = false;
    SignalSeparationMode fSignalSeparationMode = eStandard;

    bool fDetectorConstructed = false;
    bool fFastMode = false;
    bool fUMDFastMode = false;
    bool fUMDFastProp = false;
    // Injects exclusively (anti)muons in the scintillators
    // (basically avoids electrons and positrons from ionization that produces further signals, many time as corner-clipping)
    bool fUMDOnlyMuons = false;

    double fUMDMuonsCut = 0;
    double fUMDElectCut = 0;
    double fUMDGammaCut = 0;
    double fUMDOtherCut = 0;

    bool fTrackMode = false;
    double fSimulationRadius = 5*CLHEP::m;
    bool fUMDEnabled = false;
    double fScintYield = 0;
    bool fMARTAEnabled = 0;

    //G4 Range cut values
    double fRangeCutDefault = 0;
    double fRangeCutGamma = 0;
    double fRangeCutElectron = 0;
    double fRangeCutPositron = 0;
    double fRangeCutMuonPlus = 0;
    double fRangeCutMuonMinus = 0;
    double fRangeCutOpticalPhoton = 0;

    G4StationConstruction* fStationConstruction = nullptr;

    std::string fEventId;

    std::string fSimulatorSignature;

    friend class G4StationPrimaryGenerator;
    friend class G4TankFastCerenkov;
    friend class G4StationFastCerenkov;
    friend class G4StationConstruction;
    friend class G4TankPMTAction;
    friend class G4StationStackingAction;
    friend class G4ScintillatorAction;
    friend class G4StationPhysicsList;
    friend class G4StationEventAction;
    friend class G4UMDScintStripAction;
    friend class G4SoilAction;
    friend class G4StationSteppingAction;
    friend class G4StationPhysicsListCustomization;

    REGISTER_MODULE("G4StationSimulatorOG", G4StationSimulator);

  };

}


#endif
