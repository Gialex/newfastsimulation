// $Id: G4StationEventAction.cc 33725 2020-07-23 14:44:13Z darko $
#include "G4StationEventAction.h"
#include "G4StationTrackingAction.h"
#include "G4UMDPixelAction.h"

#include <fwk/RunController.h>

#include <G4Event.hh>
#include <G4Timer.hh>
#include <G4SDManager.hh>

#include <string>


namespace G4StationSimulatorOG {

  G4StationEventAction::G4StationEventAction(const bool umdEnabled) :
    fG4StationSimulator(
      dynamic_cast<G4StationSimulator&>(fwk::RunController::GetInstance().GetModule("G4StationSimulatorOG"))
    ),
    fTimer(new G4Timer),
    fIsUMDEnabled(umdEnabled)
  { }


  G4StationEventAction::~G4StationEventAction()
  {
    delete fTimer;
  }


  void
  G4StationEventAction::BeginOfEventAction(const G4Event* const /*event*/)
  {
    fTimer->Start();
    if (fIsUMDEnabled && !fG4StationSimulator.fUMDFastMode && fUMDPixelCollectionID < 0)
      fUMDPixelCollectionID = G4SDManager::GetSDMpointer()->GetCollectionID("pixelCollection");
  }


  void
  G4StationEventAction::EndOfEventAction(const G4Event* const event)
  {
    G4StationTrackingAction::fNumC = 0;
    G4StationTrackingAction::fNumCDelta = 0;
    G4StationTrackingAction::fNumBounces = 0;

    if (fIsUMDEnabled) {
      if (!G4StationSimulator::fgCurrent.GetDetectorStation().ExistsAssociatedCounter())
        return;
      ProcessUMD(event);
    }

    fUMDPixelCollectionID = -1;
    fTimer->Stop();
  }


  void
  G4StationEventAction::ProcessUMD(const G4Event* const event)
  {
    if (fUMDPixelCollectionID < 0)
      return;

    // retrieve the hit collections of this event
    G4HCofThisEvent* const hce = event->GetHCofThisEvent();
    if (!hce)
      return;

    G4UMDPixelHitCollection* const phcp = (G4UMDPixelHitCollection*)hce->GetHC(fUMDPixelCollectionID);
    if (!phcp)
      return;

    auto phc = *phcp;
    // Pixel collection
    const size_t pixel_hit = phc.entries();

    for (size_t i = 0; i < pixel_hit; ++i) {
      const int pId = phc[i]->GetPixelId();
      const int mId = phc[i]->GetModuleId();
      const auto& times = phc[i]->GetTimes();
      for (const auto& t : times)
        fG4StationSimulator.AddUMDPhoton(mId, pId, t);
    }

    phc.DrawAllHits();
  }

}
