#ifndef _G4StationSimulatorOG_G4StationPhysicsList_h_
#define _G4StationSimulatorOG_G4StationPhysicsList_h_

#include "G4StationSimulator.h"

#include <G4VUserPhysicsList.hh>

#include <fwk/RunController.h>


namespace G4StationSimulatorOG {

  /**
    \class G4TankPhysicsList

    \brief class that declares and registers the Geant4 physics classes

    \author T. McCauley
    \date 07 October 2003
    \version $Id: G4StationPhysicsList.h 33729 2020-07-23 21:46:41Z darko $
  */

  class G4StationPhysicsList : public G4VUserPhysicsList {

  public:
    G4StationPhysicsList(const bool fastCerenkov = false);
    ~G4StationPhysicsList() { }

  protected:
    void ConstructParticle();
    void ConstructProcess();
    void SetCuts();

    void ConstructBosons();
    void ConstructLeptons();
    void ConstructMesons();
    void ConstructBaryons();

    void ConstructGeneral();
    void ConstructEM();
    void ConstructOp();

  private:
    bool fFastCerenkov = false;

    G4StationSimulator& fG4StationSimulator;

  };

}


#endif
