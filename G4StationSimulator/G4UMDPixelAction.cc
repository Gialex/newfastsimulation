// $Id: G4UMDPixelAction.cc 33725 2020-07-23 14:44:13Z darko $
#include "G4UMDPixelAction.h"
#include "G4UMDPixelHit.h"

#include <G4Step.hh>
#include <G4HCofThisEvent.hh>
#include <G4TouchableHistory.hh>
#include <G4ios.hh>
#include <G4SystemOfUnits.hh>

#include <utl/AugerUnits.h>


namespace G4StationSimulatorOG {

  G4UMDPixelAction::G4UMDPixelAction(const G4String& name) :
    G4VSensitiveDetector(name)
  {
    collectionName.insert("pixelCollection");
  }


  void
  G4UMDPixelAction::Initialize(G4HCofThisEvent* const hce)
  {
    static int hcid = -1;
    fPixelCollection = new G4UMDPixelHitCollection(SensitiveDetectorName, collectionName[0]);
    if (hcid < 0) {
      hcid = GetCollectionID(0);
    }
    hce->AddHitsCollection(hcid, fPixelCollection);
  }


  G4bool
  G4UMDPixelAction::ProcessHits(G4Step* const step, G4TouchableHistory* const)
  {
    const G4String& particleName = step->GetTrack()->GetDefinition()->GetParticleName();
    // If not a opticalphoton, continue
    if (particleName != "opticalphoton")
      return false;

    // Retrieve current physical volumen and its name (i.e. the current physical pixel)
    G4VPhysicalVolume* const physVol = step->GetPreStepPoint()->GetPhysicalVolume();
    const G4String& pixelName = physVol->GetName();
    const G4int pixelId = physVol->GetCopyNo();
    // Retrieve mother volume of the current physical volumen and its name (i.e. the current physical module)
    G4VPhysicalVolume* const motherPhysVol = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume(1);
    //const G4String& moduleName = motherPhysVol->GetName();
    const G4int moduleId = motherPhysVol->GetCopyNo();

    auto& pc = *fPixelCollection;
    // Check if this pixel was previously hit
    const G4int collEntries = pc.entries();
    G4UMDPixelHit* pixelHit = nullptr;
    // Loop over the pixel hit collection and control the pixel name
    for (G4int i = 0; i < collEntries; ++i) {
      if (pc[i]->GetPixelName() == pixelName) {
        pixelHit = pc[i];
        break;
      }
    }

    if (!pixelHit) {  // this pixel wasn't previously hit in this event
      // Creates a new hit
      pixelHit = new G4UMDPixelHit;
      // Stores information of this hit
      pixelHit->SetPixelName(pixelName);
      pixelHit->SetModuleId(moduleId);
      pixelHit->SetPixelId(pixelId);
      pixelHit->SetDrawIt(true);
      pixelHit->SetUMDPixelPhysVol(physVol);
      pc.insert(pixelHit);
    }

    const G4double time = step->GetTrack()->GetGlobalTime() * (utl::ns/CLHEP::ns);
    pixelHit->AddHit(time);

    return true;
  }

}
