#ifndef _G4StationSimulatorOG_G4StationRunAction_h_
#define _G4StationSimulatorOG_G4StationRunAction_h_

#include <G4UserRunAction.hh>


class G4Run;

namespace G4StationSimulatorOG {

  /**
    \class G4TankRunAction

    \brief Geant4 Run user action class

    \author T. McCauley
    \date 07 October 2003
    \version $Id: G4StationRunAction.h 33729 2020-07-23 21:46:41Z darko $
  */

  class G4StationRunAction : public G4UserRunAction {

  public:
    G4StationRunAction() { }
    virtual ~G4StationRunAction() { }

    void BeginOfRunAction(const G4Run* const /*run*/) override { }
    void EndOfRunAction(const G4Run* const /*run*/) override { }

  };

}


#endif
