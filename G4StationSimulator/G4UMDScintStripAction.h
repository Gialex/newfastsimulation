#ifndef _G4StationSimulatorOG_G4UMDScintStripAction_h_
#define _G4StationSimulatorOG_G4UMDScintStripAction_h_

#include "G4StationSimulator.h"
#include <fwk/RunController.h>
#include <G4VSensitiveDetector.hh>


class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

namespace G4StationSimulatorOG {

  class G4UMDScintStripAction : public G4VSensitiveDetector {
  public:
    G4UMDScintStripAction(const G4String& name);
    virtual ~G4UMDScintStripAction() { }

    virtual void Initialize(G4HCofThisEvent* const /*hce*/) override { }
    virtual G4bool ProcessHits(G4Step* const step, G4TouchableHistory* const rOhist) override;
    virtual void EndOfEvent(G4HCofThisEvent* const /*hce*/) override { }

  private:
    G4StationSimulator& fG4StationSimulator;
  };

}


#endif
