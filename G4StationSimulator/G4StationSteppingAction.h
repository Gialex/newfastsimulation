#ifndef _G4StationSimulatorOG_G4StationSteppingAction_h_
#define _G4StationSimulatorOG_G4StationSteppingAction_h_

#include <G4UserSteppingAction.hh>
#include <G4Track.hh>

#include <utl/Particle.h>


class G4Step;

namespace G4StationSimulatorOG {

  class G4StationSimulator;
  class G4StationConstruction;


  /**
    \class G4StationSteppingAction

    \brief Geant4 Stepping user action class

    \author T. McCauley
    \author Altered by P. Gabriel for MARTA integration
    \date 07 October 2003
    \version $Id: G4StationSteppingAction.h 33718 2020-07-21 20:20:07Z darko $
  */

  class G4StationSteppingAction : public G4UserSteppingAction {

  public:
    G4StationSteppingAction(const bool isMARTAEnabled) : fMARTAEnabled(isMARTAEnabled) { }
    virtual ~G4StationSteppingAction() { }

    void UserSteppingAction(const G4Step* const step) override;

    static int GetCurrentParticleId() { return fgParticleId; }

    typedef std::vector<int> RPCTrackIDList;
    typedef std::vector<utl::Particle> RPCParticleList;

    typedef std::map<int, RPCTrackIDList> RPCTrackIDListMap;
    typedef std::map<int, RPCParticleList> RPCParticleListMap;

    static RPCTrackIDListMap fgRPCTrackIDListMap;
    static RPCParticleListMap fgRPCParticleListMap;

  private:
    static int fgParticleId;
    G4StationSimulator* fG4StationSimulator = nullptr;

    bool fMARTAEnabled = false;

  };

}


#endif
