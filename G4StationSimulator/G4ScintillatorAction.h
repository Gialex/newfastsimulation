#ifndef _G4StationSimulatorOG_G4ScintillatorAction_h_
#define _G4StationSimulatorOG_G4ScintillatorAction_h_

#include <G4VSensitiveDetector.hh>
#include <utl/RandomEngine.h>


class G4HCofThisEvent;
class G4Step;
class G4TouchableHistory;

namespace utl {
  class RandomEngine;
}

namespace G4StationSimulatorOG {

  class G4StationSimulator;
  class G4StationConstruction;


  /**
    \class G4ScintillatorAction

    \brief class that handles hits to scintillator bars

    \author D. Schmidt
    \date 7 October 2016
    \version $Id: G4ScintillatorAction.h 33718 2020-07-21 20:20:07Z darko $
  */

  class G4ScintillatorAction : public G4VSensitiveDetector {

  public:
    G4ScintillatorAction(const G4String& name);
    virtual ~G4ScintillatorAction() { }

    virtual void Initialize(G4HCofThisEvent* const /*hce*/) override { }
    virtual G4bool ProcessHits(G4Step* const step, G4TouchableHistory* const rOHist) override;
    virtual void EndOfEvent(G4HCofThisEvent* const /*hce*/) override { }

  private:
    G4StationSimulator& fG4StationSimulator;

  };

}


#endif
