// $Id: G4StationPrimaryGenerator.cc 33729 2020-07-23 21:46:41Z darko $
#include "G4StationPrimaryGenerator.h"
#include "G4StationSimulator.h"
#include "G4Utils.h"
#include <utl/Particle.h>
#include <utl/AugerUnits.h>

#include <G4Event.hh>
#include <G4ParticleGun.hh>
#include <G4ParticleDefinition.hh>
#include <G4ParticleTable.hh>
#include <G4SystemOfUnits.hh>

#include <sstream>


namespace G4StationSimulatorOG {

  G4StationPrimaryGenerator::G4StationPrimaryGenerator() :
    fParticleGun(new G4ParticleGun(1)),
    fParticleTable(G4ParticleTable::GetParticleTable())
  { }


  G4StationPrimaryGenerator::~G4StationPrimaryGenerator()
  {
    delete fParticleGun;
  }


  void
  G4StationPrimaryGenerator::GeneratePrimaries(G4Event* const event)
  {
    const utl::Particle& currentParticle = G4StationSimulator::fgCurrent.GetParticle();
    G4ParticleDefinition* const particleDef = fParticleTable->FindParticle(currentParticle.GetName());
    if (!particleDef) {
      std::ostringstream msg;
      msg << "Undefined particle type: " << currentParticle.GetName();
      WARNING(msg);
      return;
    }

    const sdet::Station& currentDetectorStation = G4StationSimulator::fgCurrent.GetDetectorStation();
    const auto stationCS = currentDetectorStation.GetLocalCoordinateSystem();
    const G4ThreeVector position = ToG4Vector(currentParticle.GetPosition(), stationCS, meter/utl::meter);
    const G4ThreeVector direction = ToG4Vector(currentParticle.GetDirection(), stationCS, 1);

    // set properties and pass to particle gun
    fParticleGun->SetParticleDefinition(particleDef);
    fParticleGun->SetParticlePosition(position);
    fParticleGun->SetParticleMomentumDirection(direction);

    // Convert from Auger units to G4 units
    fParticleGun->SetParticleEnergy(currentParticle.GetKineticEnergy() * (CLHEP::eV / utl::eV));
    fParticleGun->SetParticleTime(currentParticle.GetTime().GetInterval());
    fParticleGun->GeneratePrimaryVertex(event);
  }

}
