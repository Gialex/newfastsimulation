// $Id: G4UMDPixelAction.h 33725 2020-07-23 14:44:13Z darko $
#ifndef _G4StationSimulatorOG_G4UMDPixelAction_h_
#define _G4StationSimulatorOG_G4UMDPixelAction_h_

#include "G4UMDPixelHit.h"

#include <G4VSensitiveDetector.hh>


class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

namespace G4StationSimulatorOG {

  class G4UMDPixelAction : public G4VSensitiveDetector {

  public:
    G4UMDPixelAction(const G4String& name);
    virtual ~G4UMDPixelAction() { }

    void Initialize(G4HCofThisEvent* const hce) override;
    G4bool ProcessHits(G4Step* const step, G4TouchableHistory* const rOhist) override;
    void EndOfEvent(G4HCofThisEvent* const /*hce*/) override { }

  private:
    G4UMDPixelHitCollection* fPixelCollection = nullptr;

 };

}


#endif
