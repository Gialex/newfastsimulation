// $Id: G4UMDPixelHit.cc 33729 2020-07-23 21:46:41Z darko $
#include "G4UMDPixelHit.h"

#include <G4VVisManager.hh>
#include <G4Circle.hh>
#include <G4Colour.hh>
#include <G4VisAttributes.hh>
#include <G4SystemOfUnits.hh>


namespace G4StationSimulatorOG {

  G4Allocator<G4UMDPixelHit> gG4UMDPixelHitAllocator;


  void
  G4UMDPixelHit::Draw()
  {
    // Re-draw only the UMDPixels that have a opticalphoton hit (i.e, only the triggered pixels)
    // Need a physical volume to be able to draw anything
    if (fDrawIt && fHitPhysVol) {

      G4VVisManager* const m = G4VVisManager::GetConcreteInstance();

      if (m) {
        G4VisAttributes attribs(G4Colour::Red());
        attribs.SetForceSolid(true);

        // Retrieve geometric characteristic of the hit volume.
        // If a rotation or traslation are defined, retrieve them
        G4RotationMatrix rot;
        if (fHitPhysVol->GetRotation())
          rot = *fHitPhysVol->GetRotation();
        G4Transform3D trans(rot, fHitPhysVol->GetTranslation());  // Create transform
        // Now draw it
        m->Draw(*fHitPhysVol, attribs, trans);
      }

    }
  }

}
