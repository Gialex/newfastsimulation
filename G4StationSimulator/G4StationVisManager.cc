// $Id: G4StationVisManager.cc 33726 2020-07-23 15:43:27Z darko $
#include "G4StationVisManager.h"

#include <G4VRML1File.hh>
#include <G4VRML2File.hh>
#include <G4DAWNFILE.hh>


namespace G4StationSimulatorOG {

  void
  G4StationVisManager::RegisterGraphicsSystems()
  {
    RegisterGraphicsSystem(new G4VRML1File);
    RegisterGraphicsSystem(new G4VRML2File);
    RegisterGraphicsSystem(new G4DAWNFILE);
  }

}
