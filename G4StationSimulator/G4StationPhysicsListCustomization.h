#ifndef _G4StationSimulatorOG_G4StationPhysicsListCustomization_h_
#define _G4StationSimulatorOG_G4StationPhysicsListCustomization_h_

#include <tls/G4VPhysicsListCustomization.h>


namespace G4StationSimulatorOG {

  /**
    \class G4StationPhysicsListCustomization
    \brief allow customization of the standard global PhysicsList that are handled by the Geant4Manager
    \author Martin Maur
    \date 15 May 2012
  */

  class G4StationPhysicsListCustomization : public tls::G4VPhysicsListCustomization {

  public:
    G4StationPhysicsListCustomization(const bool fastCerenkov = false);
    virtual ~G4StationPhysicsListCustomization() { }

    void SetCuts() override { }
    void ConstructParticle() override { }
    void ConstructProcess() override;

    void SetCustomCuts() override;
    bool ActivateCustomProcesses() override;
    bool InactivateCustomProcesses() override;

  private:
    bool fFastCerenkov = false;

  };

}


#endif
