#ifndef _G4StationSimulatorOG_G4StationTrackingAction_h_
#define _G4StationSimulatorOG_G4StationTrackingAction_h_

#include <G4UserTrackingAction.hh>
#include <G4ios.hh>

#include "G4UMDScintStripAction.h"
#include "G4SoilAction.h"


class G4Track;

namespace G4StationSimulatorOG {

  /**
    \class G4StationTrackingAction

    \brief Geant4 Tracking user action class

    \author T. McCauley
    \date 07 October 2003
    \version $Id: G4StationTrackingAction.h 33726 2020-07-23 15:43:27Z darko $
  */

  class G4StationTrackingAction : public G4UserTrackingAction {

  public:
    G4StationTrackingAction(const bool enabled);
    ~G4StationTrackingAction() { }

    virtual void PreUserTrackingAction(const G4Track* const track) override;
    virtual void PostUserTrackingAction(const G4Track* const track) override;

    bool GetFirstStepInVolume() const { return firstStepInVolume; }
    void SetFirstStepInVolume(const bool b) { firstStepInVolume = b; }

    static int fNumC;
    static int fNumCDelta;
    static int fNumBounces;

  private:
    bool fIsUMDEnabled = false;
    bool firstStepInVolume = true;

    friend class G4UMDScintStripAction;
    friend class G4SoilAction;

  };

}


#endif
