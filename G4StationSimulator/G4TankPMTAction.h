#ifndef _G4StationSimulatorOG_G4TankPMTAction_h_
#define _G4StationSimulatorOG_G4TankPMTAction_h_

#include <utl/TabulatedFunction.h>
#include <G4VSensitiveDetector.hh>
#include "Randomize.hh"


class G4HCofThisEvent;
class G4Step;
class G4TouchableHistory;

namespace utl {
  class RandomEngine;
}

namespace sdet {
  class PMT;
}
namespace sdet {
  class Station;
}

namespace G4StationSimulatorOG {

  class G4StationSimulator;
  class G4StationConstruction;


  /**
    \class G4TankPMTAction

    \brief class that handles PMT hits

    \author T. McCauley
    \date 07 October 2003
    \version $Id: G4TankPMTAction.h 33718 2020-07-21 20:20:07Z darko $
  */

  class G4TankPMTAction : public G4VSensitiveDetector {

  public:
    G4TankPMTAction(const G4String& name, const int pmtIndex);
    virtual ~G4TankPMTAction() { }

    void Initialize(G4HCofThisEvent* /*hce*/) override { }
    G4bool ProcessHits(G4Step* const step, G4TouchableHistory* const rOHist) override;
    void EndOfEvent(G4HCofThisEvent* /*hce*/) override { }

  private:
    const int fPMTIndex = 0;
    G4StationSimulator& fG4StationSimulator;

  };

}


#endif
