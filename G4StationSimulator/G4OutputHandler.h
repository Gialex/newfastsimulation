#ifndef _G4StationSimulatorOG_G4OutputHandler_h_
#define _G4StationSimulatorOG_G4OutputHandler_h_

#include <utl/ErrorLogger.h>
#include <G4UIsession.hh>
#include <G4UImanager.hh>
#include <G4String.hh>
#include <boost/algorithm/string/trim.hpp>


namespace G4StationSimulatorOG {

  /**
    \class G4OutputHandler

    \author Javier Gonzalez
    \version $Id: G4OutputHandler.h 34028 2021-02-16 16:03:52Z darko $
    \date 15 Apr 2011
  */

  class G4OutputHandler : public G4UIsession {

  public:
    G4OutputHandler() { G4UImanager::GetUIpointer()->SetCoutDestination(this); }

    virtual ~G4OutputHandler() { G4UImanager::GetUIpointer()->SetCoutDestination(nullptr); }

    G4int
    ReceiveG4cout(const G4String& str)
      override
    {
      G4String s = str;
      boost::algorithm::trim_right(s);
      LOGG4COUT(s);
      return 0;
    }

    G4int
    ReceiveG4cerr(const G4String& str)
      override
    {
      G4String s = str;
      boost::algorithm::trim_right(s);
      LOGG4CERR(s);
      return 0;
    }

  };

}


#endif
