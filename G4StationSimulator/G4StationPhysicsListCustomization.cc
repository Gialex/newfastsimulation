#include "G4StationPhysicsListCustomization.h"
#include <G4UImanager.hh>
#include "G4StationSimulator.h"

#include "G4TankOpBoundaryProcess.h"
#include "G4TankFastCerenkov.h"
#include "G4StationFastCerenkov.h"

#include <G4MuonMinusCapture.hh>
#include <G4ParticleDefinition.hh>
#include <G4ParticleTypes.hh>
#include <G4ProcessManager.hh>
#include <G4PhysicalConstants.hh>
#include <G4SystemOfUnits.hh>


namespace G4StationSimulatorOG {

  G4StationPhysicsListCustomization::G4StationPhysicsListCustomization(const bool fastCerenkov) :
    tls::G4VPhysicsListCustomization(),
    fFastCerenkov(fastCerenkov)
  {
    defaultCutValue = 0.1*mm;
    fUImanager = G4UImanager::GetUIpointer();
  }


  void
  G4StationPhysicsListCustomization::SetCustomCuts()
  {
    const double cutForGamma = 30*mm;
    const double cutForElectron = 0.5*mm;
    const double cutForPositron = 1e-5*mm;

    SetCutValue(cutForGamma, "gamma");
    SetCutValue(cutForElectron, "e-");
    SetCutValue(cutForPositron, "e+");
    SetCutValue(0, "proton"); //why??
  }


  bool
  G4StationPhysicsListCustomization::ActivateCustomProcesses()
  {
    G4cerr << "G4StationPhysicsListCustomization::ActivateCustomProcesses" << G4endl;

    if (fFastCerenkov) {
      //G4cerr << "activating TankFastCerenkov" << G4endl;
      //fUImanager->ApplyCommand("/process/activate TankFastCerenkov");
      G4cerr << "activating StationFastCerenkov" << G4endl;
      fUImanager->ApplyCommand("/process/activate StationFastCerenkov");
      G4cerr << "de-activating StandardCerenkov" << G4endl;
      fUImanager->ApplyCommand("/process/inactivate StandardCerenkov");
    }

    if (G4StationSimulator::fgMuCapture) {
      G4cerr << "activating muMinusCaptureAtRest" << G4endl;
      fUImanager->ApplyCommand("/process/activate muMinusCaptureAtRest");
    }

    G4cerr << "activating TankOpBoundaryProcess" << G4endl;
    fUImanager->ApplyCommand("/process/activate TankOpBoundaryProcess");

    return true;
  }


  bool
  G4StationPhysicsListCustomization::InactivateCustomProcesses()
  {
    //G4cerr << "G4StationPhysicsListCustomization::InactivateCustomProcesses\n"
    //          "de-activating TankFastCerenkov" << G4endl;
    //fUImanager->ApplyCommand("/process/inactivate TankFastCerenkov");

    G4cerr << "G4StationPhysicsListCustomization::InactivateCustomProcesses\n"
      "de-activating StationFastCerenkov" << G4endl;
    fUImanager->ApplyCommand("/process/inactivate StationFastCerenkov");


    if (fFastCerenkov) {
      G4cerr << "activating StandardCerenkov" << G4endl;
      fUImanager->ApplyCommand("/process/activate StandardCerenkov");
    }

    G4cerr << "de-activating muMinusCaptureAtRest" << G4endl;
    fUImanager->ApplyCommand("/process/inactivate muMinusCaptureAtRest");

    G4cerr << "de-activating TankOpBoundaryProcess" << G4endl;
    fUImanager->ApplyCommand("/process/inactivate TankOpBoundaryProcess");

    return true;
  }


  void
  G4StationPhysicsListCustomization::ConstructProcess()
  {
    const G4int maxNumPhotons = 3;

    //G4StationSimulatorOG::G4TankFastCerenkov* const fastCerenkov = new G4StationSimulatorOG::G4TankFastCerenkov("TankFastCerenkov");
    G4StationSimulatorOG::G4StationFastCerenkov* const fastCerenkov = new G4StationSimulatorOG::G4StationFastCerenkov("StationFastCerenkov");
    fastCerenkov->SetTrackSecondariesFirst(true);
    fastCerenkov->SetMaxNumPhotonsPerStep(maxNumPhotons);
    G4Electron::Electron()->GetProcessManager()->AddContinuousProcess(fastCerenkov);
    G4Positron::Positron()->GetProcessManager()->AddContinuousProcess(fastCerenkov);
    G4MuonMinus::MuonMinusDefinition()->GetProcessManager()->AddContinuousProcess(fastCerenkov);
    G4MuonPlus::MuonPlusDefinition()->GetProcessManager()->AddContinuousProcess(fastCerenkov);

    G4GenericIon::GenericIonDefinition();  // needed for G4hIonization. G4PhysicsListHelper causes exception (Rule 3)

    G4MuonMinus::MuonMinusDefinition()->GetProcessManager()->AddProcess(new G4MuonMinusCapture);

    G4ProcessManager& pManager = *G4OpticalPhoton::OpticalPhotonDefinition()->GetProcessManager();

    G4StationSimulatorOG::G4TankOpBoundaryProcess* const boundary = new G4StationSimulatorOG::G4TankOpBoundaryProcess("TankOpBoundaryProcess");
    const G4OpticalSurfaceModel model = unified;
    boundary->SetModel(model);
    pManager.AddDiscreteProcess(boundary);
  }

}
