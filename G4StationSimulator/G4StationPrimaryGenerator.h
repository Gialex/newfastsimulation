/**
   \class G4StationPrimaryGenerator
   
   \brief class that passes particles to Geant4 particle gun
     
   \author    T. McCauley
   \date      07 October 2003
   \version   $Id: G4StationPrimaryGenerator.h 29079 2016-07-11 15:30:54Z mayotte $

   \todo      Units and coordinate systems need to be checked
*/

#ifndef _G4StationSimulatorOG_G4StationPrimaryGenerator_h_
#define _G4StationSimulatorOG_G4StationPrimaryGenerator_h_

#include <G4VUserPrimaryGeneratorAction.hh>

class G4Event;
class G4ParticleGun;
class G4ParticleTable;

namespace G4StationSimulatorOG {

  class G4StationPrimaryGenerator : public G4VUserPrimaryGeneratorAction {

  public:	
    
    G4StationPrimaryGenerator();
    ~G4StationPrimaryGenerator();

    // This method is called each time 
    // G4RunManager* fRunManager->BeamOn() is called:

    void GeneratePrimaries(G4Event *TheEvent);
    
  protected:
    
  private:
    
    G4ParticleGun*   fParticleGun;
    G4ParticleTable* fParticleTable;    

  };

}

#endif 
