// $Id: G4SoilAction.cc 33729 2020-07-23 21:46:41Z darko $

#include "G4SoilAction.h"
#include "G4StationSimulator.h"

#include <G4Step.hh>
#include <G4TouchableHistory.hh>


namespace G4StationSimulatorOG {

  G4SoilAction::G4SoilAction(const G4String& name) :
    G4VSensitiveDetector(name),
    fG4StationSimulator(
      dynamic_cast<G4StationSimulator&>(fwk::RunController::GetInstance().GetModule("G4StationSimulatorOG"))
    )
  { }


  G4bool
  G4SoilAction::ProcessHits(G4Step* const step, G4TouchableHistory* const /*rOHist*/)
  {
    if (!fG4StationSimulator.fUMDFastProp)
      return true;

    // Particle
    const G4String& pName = step->GetTrack()->GetDefinition()->GetParticleName();
    const G4double kin = step->GetTrack()->GetKineticEnergy();

    // Only first step inside scintillator (PreStepPoint() == fGeomBoundary on:
    //
    // vol1  |         vol2 (Soil)                |      vol3
    //       |                                    |
    //       |     CurrStep                       |
    //       |o----------------o                  |
    //       |                                    |
    //       |PrePoint         PostPoint          |
    //       |                                    |
    //       |------------------------------------|
    //       |                                    |
    //       |                  o----------------o|
    //       |                                    |
    //                          PrePoint         PostPoint
    //
    // GeomBoundary Vol1-Vol2               GeomBoundary Vol2-Vol3

    if (step->GetPreStepPoint()->GetStepStatus() == fGeomBoundary) {

      if ((pName == "gamma"                   && kin < fG4StationSimulator.fUMDGammaCut) ||
          ((pName == "mu+" || pName == "mu-") && kin < fG4StationSimulator.fUMDMuonsCut) ||
          ((pName == "e+"  || pName == "e-" ) && kin < fG4StationSimulator.fUMDElectCut) ||
          (kin < fG4StationSimulator.fUMDOtherCut)) {
        step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);
        return false;
      }

    }

    return true;
  }

}
