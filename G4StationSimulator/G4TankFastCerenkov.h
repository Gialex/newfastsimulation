// $Id: G4TankFastCerenkov.h 33718 2020-07-21 20:20:07Z darko $
//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************

#ifndef _G4StationSimulatorOG_G4TankFastCerenkov_h_
#define _G4StationSimulatorOG_G4TankFastCerenkov_h_

#include <sdet/PMT.h>
#include <utl/Point.h>
#include <sevt/Station.h>

#include <globals.hh>
#include <templates.hh>
#include <Randomize.hh>
#include <G4ThreeVector.hh>
#include <G4ParticleMomentum.hh>
#include <G4Step.hh>
#include <G4VContinuousProcess.hh>
#include <G4OpticalPhoton.hh>
#include <G4DynamicParticle.hh>
#include <G4Material.hh>
#include <G4PhysicsTable.hh>
#include <G4MaterialPropertiesTable.hh>
#include <G4PhysicsOrderedFreeVector.hh>
#include <G4ios.hh>
#include <G4Poisson.hh>


namespace G4StationSimulatorOG {

  class G4StationSimulator;


  class G4TankFastCerenkov : public G4VContinuousProcess {

  public:
    G4TankFastCerenkov(const G4String& processName = "Cerenkov");
    virtual ~G4TankFastCerenkov();

    static void GetDataFromConstruction();

    G4bool IsApplicable(const G4ParticleDefinition& particle) override
    { return particle.GetPDGCharge(); }

    void SetTrackSecondariesFirst(const G4bool state)
    { fTrackSecondariesFirst = state; }

    void SetMaxNumPhotonsPerStep(const G4int numPhotons)
    { fMaxPhotons = numPhotons; }

    void DumpPhysicsTable() const;

    G4VParticleChange* AlongStepDoIt(const G4Track& track, const G4Step& step) override;

    G4double GetContinuousStepLimit(const G4Track&, G4double, G4double, G4double&) override;

  private:
    void BuildThePhysicsTable();
    G4double GetAverageNumberOfPhotons(const G4DynamicParticle*,
				       const G4Material*,
				       G4MaterialPropertyVector*) const;
    void TrackPhotonInTank();
    void PropagateInTank(G4int flag);

    template<int pmtId>
    G4bool TransitionToDome(G4int flag);

    template<int pmtId>
    G4bool PropagateInDome(G4int& flag);

    template<int pmtId>
    G4bool TransitionToInterface(G4int& flag);

    template<int pmtId>
    G4bool PropagateInInterface(G4int& flag);

    G4double GetSphereIntersect(const G4ThreeVector&, const G4double) const;

    G4double GetEllipsoidIntersect(const G4ThreeVector&,
				   const G4double, const G4double) const;

    void DoRayleighScatter();
    G4bool ScatterOffRoof();
    G4bool ScatterOffWall();
    G4bool ScatterOffFloor();
    void BackScatter();
    void DiffuseScatterHorizontal(const G4ThreeVector& normal);
    void DiffuseScatterVertical(const G4ThreeVector& normal);
    void SpikeScatter(const G4ThreeVector& normal);
    void LobeScatterHorizontal(const G4ThreeVector& normal);
    void LobeScatterVertical(const G4ThreeVector& normal);

    G4bool fTrackSecondariesFirst = false;
    G4int fMaxPhotons = 0;
    G4ThreeVector fPhotonMomentum;
    G4ThreeVector fPhotonPolarization;
    G4ThreeVector fPhotonPosition;
    G4double fPhotonTime;
    G4double fSampledMomentum;

    G4double fInvMFP;
    G4double fRayleighFrac;
    G4double fAbsorbFrac;

    static double fTankHeight;
    static double fTankHalfHeight;
    static double fTankThickness;
    static double fTankRadius;
    static double fTankRadiusSq;
    static double fDomeRadius;
    static double fInterfaceRadius;
    static double fPMTRadius;
    static double fHeightz;
    static double fDomeRadiusz;
    static double fInterfaceRadiusz;
    static double fPMTRadiusz;
    static double fDomeRadiusSq;
    static double fInterfaceRadiusSq;
    static double fPMTRadiusSq;
    static double fDomeRadiuszSq;
    static double fInterfaceRadiuszSq;
    static double fPMTRadiuszSq;

    static double fSigmaAlpha;

    static utl::TabulatedFunction fQE;

    static utl::TabulatedFunction fWaterAbsLength;
    static utl::TabulatedFunction fInterfaceRIndex;
    static utl::TabulatedFunction fDomeRIndex;

    static utl::TabulatedFunction fInterfaceAbsLength;
    static utl::TabulatedFunction fDomeAbsLength;
    static utl::TabulatedFunction fLinerReflectivity;
    static utl::TabulatedFunction fLinerSpecularLobe;
    static utl::TabulatedFunction fLinerSpecularSpike;
    static utl::TabulatedFunction fLinerBackscatter;

    // PMT positions (the zeroth position is dummy)
    static G4ThreeVector fPMTPos[4];
    // The height of the roof, referred to each PMT
    static G4double fRoofPos[4];

    G4double fSampledRI;
    G4double fRayleighConst;

    G4StationSimulator& fG4StationSimulator;

  protected:
    G4PhysicsTable* fPhysicsTable = nullptr;

  };

}


#endif
