// $Id: G4Utils.h 33718 2020-07-21 20:20:07Z darko $
#ifndef _G4StationSimulator_G4Utils_h_
#define _G4StationSimulator_G4Utils_h_

#include <utl/AugerUnits.h>
#include <utl/CoordinateSystemPtr.h>
#include <utl/Vector.h>
#include <G4ThreeVector.hh>


namespace G4StationSimulatorOG {

  template<class V>
  inline
  G4ThreeVector
  ToG4Vector(const V& v, const utl::CoordinateSystemPtr& cs, const double unitConversion)
  {
    const utl::Vector::Triple xyz = v.GetCoordinates(cs);
    return
      G4ThreeVector(
        unitConversion * xyz.get<0>(),
        unitConversion * xyz.get<1>(),
        unitConversion * xyz.get<2>()
      );
  }


  template<class V>
  inline
  V
  To(const G4ThreeVector& v, const utl::CoordinateSystemPtr& cs, const double unitConversion)
  {
    return
      V(
        unitConversion* v.x(),
        unitConversion* v.y(),
        unitConversion* v.z(),
        cs
      );
  }

}


#endif
