// $Id: G4StationTrackingAction.cc 33726 2020-07-23 15:43:27Z darko $
#include "G4StationTrackingAction.h"

#include <G4Track.hh>
#include <G4TrackingManager.hh>
#include <G4TrackStatus.hh>


namespace G4StationSimulatorOG {

  // Number of Cherenkov photons from injected particle
  int G4StationTrackingAction::fNumC = 0;
  // Number of Cherenkov photons NOT coming directly from the injected particle. (eg. delta rays)
  int G4StationTrackingAction::fNumCDelta = 0;
  int G4StationTrackingAction::fNumBounces = 0;


  G4StationTrackingAction::G4StationTrackingAction(const bool umdEnable) :
    fIsUMDEnabled(umdEnable)
  { }


  void
  G4StationTrackingAction::PreUserTrackingAction(const G4Track* const track)
  {
    firstStepInVolume = true;
    fNumBounces = 0;

    const int code = track->GetDefinition()->GetPDGEncoding();

    if (code == 0) {  // Optical photon

      const int parent = track->GetParentID();

      // Count number of Cerenkov photons
      if (parent != 1)  // not directly produced by the injected particle (eg delta ray)
        ++fNumCDelta;
      else // Produced by injected particle
        ++fNumC;
    }
  }


  void
  G4StationTrackingAction::PostUserTrackingAction(const G4Track* const /*track*/)
  {
    firstStepInVolume = false;
  }

}
