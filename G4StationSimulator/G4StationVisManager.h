#ifndef _G4StationSimulatorOG_G4StationVisManager_h_
#define _G4StationSimulatorOG_G4StationVisManager_h_

#include <G4VisManager.hh>


namespace G4StationSimulatorOG {

  /**
    \class G4StationVisManager

    \brief implementation of G4 visualization manager

    \author T. McCauley
    \date 06 May 2004
    \version $Id: G4StationVisManager.h 33726 2020-07-23 15:43:27Z darko $
  */

  class G4StationVisManager : public G4VisManager {

  public:
    G4StationVisManager() { }
    virtual ~G4StationVisManager() { }

    void RegisterGraphicsSystems();

  };

}


#endif
