// $Id: G4StationSteppingAction.cc 33718 2020-07-21 20:20:07Z darko $
#include "G4StationSteppingAction.h"
#include "G4StationTrackingAction.h"
#include "G4StationEventAction.h"
#include "G4StationSimulator.h"
#include "G4StationConstruction.h"
#include "G4Utils.h"

#include <det/Detector.h>
#include <sdet/SDetector.h>
#include <sdet/Station.h>
#include <sevt/PMT.h>
#include <sevt/PMTSimData.h>
#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sevt/StationSimData.h>
#include <utl/config.h>
#include <utl/ErrorLogger.h>
#include <utl/Particle.h>
#include <utl/Trace.h>
#include <utl/AugerCoordinateSystem.h>
#include <fwk/RunController.h>

#include <G4Step.hh>
#include <G4VProcess.hh>
#include <G4SteppingManager.hh>
#include <G4Track.hh>
#include <G4TrackVector.hh>
#include <G4Trajectory.hh>
#include <G4TrajectoryContainer.hh>

#include <sstream>
#include <vector>

using namespace std;


namespace G4StationSimulatorOG {

  G4StationSteppingAction::RPCTrackIDListMap G4StationSteppingAction::fgRPCTrackIDListMap;
  G4StationSteppingAction::RPCParticleListMap G4StationSteppingAction::fgRPCParticleListMap;

  int G4StationSteppingAction::fgParticleId = 0;


  void
  G4StationSteppingAction::UserSteppingAction(const G4Step* const step)
  {
    fgParticleId = step->GetTrack()->GetDefinition()->GetPDGEncoding();

    if (!fgParticleId &&
        step->GetTrack()->GetNextVolume() &&
        (step->GetTrack()->GetNextVolume()->GetName() == "top" ||
         step->GetTrack()->GetNextVolume()->GetName() == "side" ||
         step->GetTrack()->GetNextVolume()->GetName() == "bottom"))
      ++G4StationTrackingAction::fNumBounces;

    if (!fMARTAEnabled)
      return;

    const G4String& CurrentVolume = step->GetTrack()->GetVolume()->GetName();
    if (CurrentVolume != "rpc_gas")
      return;

    G4VPhysicalVolume* const mother = step->GetTrack()->GetTouchable()->GetVolume(1);
    G4VPhysicalVolume* const grandmother = step->GetTrack()->GetTouchable()->GetVolume(2);

    if (mother->GetName() != "rpc_glass" || grandmother->GetName() != "rpc") {
      ostringstream err;
      err << "RPC gas not found where expected : mother is "
          << mother->GetName() << " (should be rpc_glass) and grandmother is "
          << grandmother->GetName() << "  (should be rpc) ";
      throw utl::InvalidConfigurationException(err.str());
    }

    const G4int rpcId = grandmother->GetCopyNo();

    const int particleID = step->GetTrack()->GetDefinition()->GetPDGEncoding();
    const int trackID = step->GetTrack()->GetTrackID();

    const double ke1 = step->GetPreStepPoint()->GetKineticEnergy() / CLHEP::MeV * utl::MeV;
    const double dedx = step->GetTotalEnergyDeposit() / CLHEP::MeV * utl::MeV;
    const double eDepNonIoni = step->GetNonIonizingEnergyDeposit() / CLHEP::MeV * utl::MeV;
    const double eDepIoni = dedx - eDepNonIoni;

    double w = step->GetTrack()->GetWeight();

    const bool isSec = w < 0;

    G4TrackVector& secondary = *G4UserSteppingAction::fpSteppingManager->GetfSecondary();

    for (size_t i = 0; i < secondary.size(); ++i) {
      if (secondary[i]->GetVolume()->GetName() != "rpc_gas")
        continue;
      const double secW = secondary[i]->GetWeight();
      if (isSec)
        secondary[i]->SetWeight(secW);
      else
        secondary[i]->SetWeight(-trackID);
    }

    bool trackSave = true;
    if (isSec) {  // Particle it is a secondary, add dEdX to primary

      for (unsigned int t = 0; t < fgRPCTrackIDListMap[rpcId].size(); ++t) {
        if (fgRPCTrackIDListMap[rpcId][t] != -w)  // find trackID of the primary using W of secondary
          continue;

        if (eDepIoni > 0) {
          const double eLoss = fgRPCParticleListMap[rpcId][t].GetWeight();
          fgRPCParticleListMap[rpcId][t].SetWeight(eDepIoni + eLoss);
        }

        trackSave = false;
      }

    } else {

      for (unsigned int t = 0; t < fgRPCTrackIDListMap[rpcId].size(); ++t) {
        if (fgRPCTrackIDListMap[rpcId][t] != trackID)  // find trackID of the primary
          continue;

        if (eDepIoni > 0) {
          const double eLoss = fgRPCParticleListMap[rpcId][t].GetWeight();
          fgRPCParticleListMap[rpcId][t].SetWeight(eDepIoni + eLoss);
        }

        trackSave = false;
      }

    }

    // Save a new primary track which is not secondary!
    if (trackSave && !isSec) {

      fgRPCTrackIDListMap[rpcId].push_back(trackID);

      const sdet::Station& station = G4StationSimulator::fgCurrent.GetDetectorStation();
      const auto cs = station.GetLocalCoordinateSystem();

      // Position in Tank CS (centered in center of tank)
      const G4ThreeVector pos = step->GetPreStepPoint()->GetPosition() - G4StationConstruction::fgTankCenter;  // <-- this is not right
      const double time = step->GetPreStepPoint()->GetGlobalTime() / CLHEP::ns * utl::ns;
      const auto position = To<utl::Point>(pos, cs, utl::meter/CLHEP::meter);
      const auto direction = To<utl::Vector>(step->GetPreStepPoint()->GetMomentumDirection(), cs, 1);
      const auto particleType = utl::Particle::Type(particleID);

      utl::Particle& currentParticle = G4StationSimulator::fgCurrent.GetParticle();

      const utl::Particle::Source source = currentParticle.GetSource();
      utl::Particle newParticle(particleType, source, position, direction, time, eDepIoni, ke1);

      // Set parent particle (i.e. the particle entering the tank)

      if (step->GetTrack()->GetParentID())
        newParticle.SetParent(currentParticle);

      // Copy production point
      if (currentParticle.HasProductionPoint()) {
        const auto& pProd = currentParticle.GetProductionPoint();
        newParticle.SetProductionPoint(pProd);
      }

      fgRPCParticleListMap[rpcId].push_back(newParticle);

    }
  }

}
