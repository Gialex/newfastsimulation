#ifndef _G4StationSimulatorOG_G4StationEventAction_h_
#define _G4StationSimulatorOG_G4StationEventAction_h_

#include "G4StationSimulator.h"

#include <det/Detector.h>
#include <sdet/SDetector.h>
#include <mdet/MDetector.h>

#include <G4UserEventAction.hh>


class G4Event;
class G4Timer;

namespace G4StationSimulatorOG {

  /**
    \class G4StationEventAction

    \brief Geant4 Event user action class

    \author T. McCauley
    \date 07 October 2003
    \version $Id: G4StationEventAction.h 33725 2020-07-23 14:44:13Z darko $
  */

  class G4StationEventAction : public G4UserEventAction {

  public:
    G4StationEventAction(const bool umdEnabled);
    virtual ~G4StationEventAction();

    virtual void BeginOfEventAction(const G4Event* const event) override;
    virtual void EndOfEventAction(const G4Event* const event) override;

    void ProcessUMD(const G4Event* const event);

  private:
    G4StationSimulator& fG4StationSimulator;
    G4Timer* fTimer = nullptr;
    int fUMDPixelCollectionID = -1;
    bool fIsUMDEnabled = false;

  };

}


#endif
