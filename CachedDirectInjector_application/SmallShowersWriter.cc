// $Id: SdSimulationCalibrator.cc 33232 2020-03-04 17:46:51Z darko $

#include "SmallShowersWriter.h"

#include <det/Detector.h>

#include <sdet/SDetector.h>
#include <sdet/Station.h>
#include <sdet/PMTConstants.h>

#include <evt/Event.h>
#include <evt/ShowerSimData.h>

#include <fwk/CentralConfig.h>

#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sevt/StationSimData.h>
#include <sevt/StationCalibData.h>
#include <sevt/StationTriggerData.h>

#include <sevt/PMTCalibData.h>
#include <sevt/PMTSimData.h>
#include <sevt/PMT.h>

#include <utl/AugerUnits.h>
#include <utl/MathConstants.h>
#include <utl/Particle.h>
#include <utl/PhysicalConstants.h>
#include <utl/ReferenceEllipsoid.h>
#include <utl/TimeStamp.h>
#include <utl/ErrorLogger.h>
#include <utl/UTMPoint.h>
#include <utl/Point.h>
#include <utl/Vector.h>
#include <utl/GeometryUtilities.h>
#include <utl/Reader.h>
#include <utl/TimeDistribution.h>
#include <utl/TimeDistributionAlgorithm.h>
#include <utl/TabularStream.h>

using namespace utl;
using namespace sevt;
using namespace fwk;
using namespace evt;
using namespace det;
using namespace std;


namespace SdSimulationCalibratorOG {

  void
  MakeRootOutput(const std::string title)
  {

    TFile* f = new TFile(title.c_str(), "RECREATE");
    TTree* t = new TTree("simCalibData", "Simulated small showers calibration data");

    Int_t SdId;
    t->Branch("SdId", &SdId);
    Int_t n_selected;
    t->Branch("n_selected", &n_selected);
    Int_t N_simulated;
    t->Branch("N_simulated", &N_simulated);

    // Primary infos
    Int_t primaryType;
    t->Branch("primaryType", &primaryType);
    Double_t primaryEnergy;
    t->Branch("primaryEnergy", &primaryEnergy);
    Double_t primaryZenith;
    t->Branch("primaryZenith", &primaryZenith);
    Double_t primaryAzimuth;
    t->Branch("primaryAzimuth", &primaryAzimuth);
    Double_t coreX_stationCS;
    t->Branch("coreX_stationCS", &coreX_stationCS);
    Double_t coreY_stationCS;
    t->Branch("coreY_stationCS", &coreY_stationCS);
    Double_t coreAltitude;
    t->Branch("coreAltitude", &coreAltitude);

    // Shower particles infos
    UInt_t Nparticles;
    t->Branch("Nparticles", &Nparticles);
    vector<Int_t> particleType;
    t->Branch("particleType", &particleType);
    vector<Double_t> particleEnergy;
    t->Branch("particleEnergy", &particleEnergy);
    vector<Double_t> particlePositionX;
    t->Branch("particlePositionX", &particlePositionX);
    vector<Double_t> particlePositionY;
    t->Branch("particlePositionY", &particlePositionY);
    vector<Double_t> particlePositionZ;
    t->Branch("particlePositionZ", &particlePositionZ);
    vector<Double_t> particleZenith;
    t->Branch("particleZenith", &particleZenith);
    vector<Double_t> particleAzimuth;
    t->Branch("particleAzimuth", &particleAzimuth);
    vector<Double_t> particleTime;
    t->Branch("particleTime", &particleTime);

    // PMTs calibration infos
    Double_t baseline[5] = { 0 };
    t->Branch("baseline", &baseline[5],"baseline[5]/D");
    Double_t baselineRMS[5] = { 0 };
    t->Branch("baselineRMS", &baselineRMS[5],"baselineRMS[5]/D");

    Double_t VEMcharge[4] = { 0 };
    t->Branch("VEMcharge", &VEMcharge[4],"VEMcharge[4]/D");
    Double_t VEMpeak[4] = { 0 };
    t->Branch("VEMpeak", &VEMpeak[4],"VEMpeak[4]/D");
    Double_t HG_LG_ratio[4] = { 0 };
    t->Branch("HG_LG_ratio", &HG_LG_ratio[4],"HG_LG_ratio[4]/D");

    Int_t nPE[5] = { 0 };
    t->Branch("nPE", &nPE[5],"nPE[5]/I");
    
    vector<UInt_t> PEtrace_LPMT1_time;
    t->Branch("PEtrace_LPMT1_time", &PEtrace_LPMT1_time);
    vector<UInt_t> PEtrace_LPMT1;
    t->Branch("PEtrace_LPMT1", &PEtrace_LPMT1);

    vector<UInt_t> PEtrace_LPMT2_time;
    t->Branch("PEtrace_LPMT2_time", &PEtrace_LPMT2_time);
    vector<UInt_t> PEtrace_LPMT2;
    t->Branch("PEtrace_LPMT2", &PEtrace_LPMT2);

    vector<UInt_t> PEtrace_LPMT3_time;
    t->Branch("PEtrace_LPMT3_time", &PEtrace_LPMT3_time);
    vector<UInt_t> PEtrace_LPMT3;
    t->Branch("PEtrace_LPMT3", &PEtrace_LPMT3);

    vector<UInt_t> PEtrace_SPMT_time;
    t->Branch("PEtrace_SPMT_time", &PEtrace_SPMT_time);
    vector<UInt_t> PEtrace_SPMT;
    t->Branch("PEtrace_SPMT", &PEtrace_SPMT);

    vector<UInt_t> PEtrace_SSD_time;
    t->Branch("PEtrace_SSD_time", &PEtrace_SSD_time);
    vector<UInt_t> PEtrace_SSD;
    t->Branch("PEtrace_SSD", &PEtrace_SSD);

    /*
    // Simulated traces infos
    Int_t startBin[5] = { 0 };
    t->Branch("startBin", &startBin[5],"startBin[5]/I");
    Int_t stopBin[5] = { 0 };
    t->Branch("stopBin", &stopBin[5],"stopBin[5]/I");
    vector<Int_t> traceRaw_LPMT1;
    t->Branch("traceRaw_LPMT1", &traceRaw_LPMT1);
    vector<Int_t> traceRaw_LPMT2;
    t->Branch("traceRaw_LPMT2", &traceRaw_LPMT2);
    vector<Int_t> traceRaw_LPMT3;
    t->Branch("traceRaw_LPMT3", &traceRaw_LPMT3);
    vector<Int_t> traceRaw_SPMT;
    t->Branch("traceRaw_SPMT", &traceRaw_SPMT);
    vector<Int_t> traceRaw_SSD;
    t->Branch("traceRaw_SSD", &traceRaw_SSD);
    */
    // Triggered (simulated) traces infos
    Bool_t IsT1 ;
    t->Branch("IsT1", &IsT1);
    Int_t trace_LPMT1[2048] = { 0 };
    t->Branch("trace_LPMT1", &trace_LPMT1[2048],"trace_LPMT1[2048]/I");
    Int_t trace_LPMT2[2048] = { 0 };
    t->Branch("trace_LPMT2", &trace_LPMT2[2048],"trace_LPMT2[2048]/I");
    Int_t trace_LPMT3[2048] = { 0 };
    t->Branch("trace_LPMT3", &trace_LPMT3[2048],"trace_LPMT3[2048]/I");
    Int_t trace_SPMT[2048] = { 0 };
    t->Branch("trace_SPMT", &trace_SPMT[2048],"trace_SPMT[2048]/I");
    Int_t trace_SSD[2048] = { 0 };
    t->Branch("trace_SSD", &trace_SSD[2048],"trace_SSD[2048]/I");

    Double_t peak_LG[5] = { 0 };
    t->Branch("peak_LG", &peak_LG,"peak_LG[5]/D");
    Double_t charge_LG[5] = { 0 };
    t->Branch("charge_LG", &charge_LG,"charge_LG[5]/D");
    UInt_t satBins_LG[5] = { 0 };
    t->Branch("saturatedBins_LG", &satBins_LG,"saturatedBins_LG[5]/i");

    t->Write();
    f->Close();

  }


  bool
  FillSmallShowersData(const std::string title, 
		       const int sdId,
		       const sevt::Station& station, 
		       const evt::ShowerSimData& simShower,
		       int* n, int* N)
  {

    if(gSystem->AccessPathName(title.c_str())){
      cout << title << " file not found." << "\n";
      return false;
    }
    TFile* f = new TFile(title.c_str(), "UPDATE");

    if( !f->GetListOfKeys()->Contains("simCalibData") ){
      cout << "TTrees not found inside " << title << "\n";
      f->Close("R");
      delete f;
      return false;
    }
    TTree* t = (TTree*)f->Get("simCalibData");

    Int_t SdId;
    t->SetBranchAddress("SdId", &SdId);
    Int_t n_selected;
    t->SetBranchAddress("n_selected", &n_selected);
    Int_t N_simulated;
    t->SetBranchAddress("N_simulated", &N_simulated);

    Int_t primaryType;
    t->SetBranchAddress("primaryType", &primaryType);
    Double_t primaryEnergy;
    t->SetBranchAddress("primaryEnergy", &primaryEnergy);
    Double_t primaryZenith;
    t->SetBranchAddress("primaryZenith", &primaryZenith);
    Double_t primaryAzimuth;
    t->SetBranchAddress("primaryAzimuth", &primaryAzimuth);
    Double_t coreX_stationCS;
    t->SetBranchAddress("coreX_stationCS", &coreX_stationCS);
    Double_t coreY_stationCS;
    t->SetBranchAddress("coreY_stationCS", &coreY_stationCS);
    Double_t coreAltitude;
    t->SetBranchAddress("coreAltitude", &coreAltitude);

    UInt_t Nparticles = 0;
    t->SetBranchAddress("Nparticles", &Nparticles);
    vector<Int_t>* particleType = NULL;
    t->SetBranchAddress("particleType", &particleType);
    vector<Double_t>* particleEnergy = NULL;
    t->SetBranchAddress("particleEnergy", &particleEnergy);
    vector<Double_t>* particlePositionX = NULL;
    t->SetBranchAddress("particlePositionX", &particlePositionX);
    vector<Double_t>* particlePositionY = NULL;
    t->SetBranchAddress("particlePositionY", &particlePositionY);
    vector<Double_t>* particlePositionZ = NULL;
    t->SetBranchAddress("particlePositionZ", &particlePositionZ);
    vector<Double_t>* particleZenith = NULL;
    t->SetBranchAddress("particleZenith", &particleZenith);
    vector<Double_t>* particleAzimuth = NULL;
    t->SetBranchAddress("particleAzimuth", &particleAzimuth);
    vector<Double_t>* particleTime = NULL;
    t->SetBranchAddress("particleTime", &particleTime);

    Double_t baseline[5] = { 0 };
    t->SetBranchAddress("baseline", &baseline);
    Double_t baselineRMS[5] = { 0 };
    t->SetBranchAddress("baselineRMS", &baselineRMS);

    Double_t VEMcharge[4] = { 0 };
    t->SetBranchAddress("VEMcharge", &VEMcharge);
    Double_t VEMpeak[4] = { 0 };
    t->SetBranchAddress("VEMpeak", &VEMpeak);
    Double_t HG_LG_ratio[4] = { 0 };
    t->SetBranchAddress("HG_LG_ratio", &HG_LG_ratio);

    Int_t nPE[5] = { 0 };
    t->SetBranchAddress("nPE", &nPE);

    vector<UInt_t>* PEtrace_LPMT1_time = NULL;
    t->SetBranchAddress("PEtrace_LPMT1_time", &PEtrace_LPMT1_time);
    vector<UInt_t>* PEtrace_LPMT1 = NULL;
    t->SetBranchAddress("PEtrace_LPMT1", &PEtrace_LPMT1);

    vector<UInt_t>* PEtrace_LPMT2_time = NULL;
    t->SetBranchAddress("PEtrace_LPMT2_time", &PEtrace_LPMT2_time);
    vector<UInt_t>* PEtrace_LPMT2 = NULL;
    t->SetBranchAddress("PEtrace_LPMT2", &PEtrace_LPMT2);

    vector<UInt_t>* PEtrace_LPMT3_time = NULL;
    t->SetBranchAddress("PEtrace_LPMT3_time", &PEtrace_LPMT3_time);
    vector<UInt_t>* PEtrace_LPMT3 = NULL;
    t->SetBranchAddress("PEtrace_LPMT3", &PEtrace_LPMT3);

    vector<UInt_t>* PEtrace_SPMT_time = NULL;
    t->SetBranchAddress("PEtrace_SPMT_time", &PEtrace_SPMT_time);
    vector<UInt_t>* PEtrace_SPMT = NULL;
    t->SetBranchAddress("PEtrace_SPMT", &PEtrace_SPMT);

    vector<UInt_t>* PEtrace_SSD_time = NULL;
    t->SetBranchAddress("PEtrace_SSD_time", &PEtrace_SSD_time);
    vector<UInt_t>* PEtrace_SSD = NULL;
    t->SetBranchAddress("PEtrace_SSD", &PEtrace_SSD);

    /*
    Int_t startBin[5] = { 0 };
    t->SetBranchAddress("startBin", &startBin);
    Int_t stopBin[5] = { 0 };
    t->SetBranchAddress("stopBin", &stopBin);
    vector<UInt_t>* traceRaw_LPMT1 = NULL;
    t->SetBranchAddress("traceRaw_LPMT1", &traceRaw_LPMT1);
    vector<UInt_t>* traceRaw_LPMT2 = NULL;
    t->SetBranchAddress("traceRaw_LPMT2", &traceRaw_LPMT2);
    vector<UInt_t>* traceRaw_LPMT3 = NULL;
    t->SetBranchAddress("traceRaw_LPMT3", &traceRaw_LPMT3);
    vector<UInt_t>* traceRaw_SPMT = NULL;
    t->SetBranchAddress("traceRaw_SPMT", &traceRaw_SPMT);
    vector<UInt_t>* traceRaw_SSD = NULL;
    t->SetBranchAddress("traceRaw_SSD", &traceRaw_SSD);
    */
    Bool_t IsT1 = false;
    t->SetBranchAddress("IsT1", &IsT1);
    Int_t trace_LPMT1[2048] = { 0 };
    t->SetBranchAddress("trace_LPMT1", &trace_LPMT1);
    Int_t trace_LPMT2[2048] = { 0 };
    t->SetBranchAddress("trace_LPMT2", &trace_LPMT2);
    Int_t trace_LPMT3[2048] = { 0 };
    t->SetBranchAddress("trace_LPMT3", &trace_LPMT3);
    Int_t trace_SPMT[2048] = { 0 };
    t->SetBranchAddress("trace_SPMT", &trace_SPMT);
    Int_t trace_SSD[2048] = { 0 };
    t->SetBranchAddress("trace_SSD", &trace_SSD);

    Double_t peak_LG[5] = { 0 };
    t->SetBranchAddress("peak_LG", &peak_LG);
    Double_t charge_LG[5] = { 0 };
    t->SetBranchAddress("charge_LG", &charge_LG);
    UInt_t satBins_LG[5] = { 0 };
    t->SetBranchAddress("saturatedBins_LG", &satBins_LG);

    ////////

    *N = *N + 1;

    for (int i=0; i<5; i++) {
      nPE[i]=0;
      charge_LG[i]=0;
      peak_LG[i]=0;
      satBins_LG[i]=0;
    }

    ///////

    SdId = sdId;

    const sdet::Station& detStation =
      det::Detector::GetInstance().GetSDetector().GetStation(station);

    const sevt::StationSimData& stationSimData = station.GetSimData();
    string signature = stationSimData.GetSimulatorSignature();

    const CoordinateSystemPtr stationCS =
      detStation.GetLocalCoordinateSystem();

    // Fill primary infos
    primaryType = simShower.GetPrimaryParticle();
    primaryEnergy = simShower.GetEnergy();
    primaryZenith = simShower.GetGroundParticleCoordinateSystemZenith();
    // The coordinate system is rotated of 90 deg 
    // w.r.t. the CORSIKA shower azimuth during the construction
    primaryAzimuth = simShower.GetGroundParticleCoordinateSystemAzimuth() + 90*deg;

    // The axis obtaned with the GetDirection() function in SimShowerData
    // is rotated of 4.233 deg ( = default magnetic declination correction)
    // or a different value depending on the station (if the boolean
    // realistic magnetic correction is true).
    // Thus is simpler to define a custom axis here.
    Vector axis(sin(primaryZenith)*cos(primaryAzimuth),
    		sin(primaryZenith)*sin(primaryAzimuth),
    		cos(primaryZenith), stationCS);

    // The core position in SimShowerData is located at the observation level 
    // of the original CORSIKA shower, naturally along the shower axis
    Point simCore = simShower.GetPosition();
    Point coreOnGround = simCore - 
      ((simCore.GetZ(stationCS)/meter)/cos(primaryZenith)) * axis;
    coreX_stationCS = coreOnGround.GetX(stationCS)/meter;
    coreY_stationCS = coreOnGround.GetY(stationCS)/meter;

    const UTMPoint stationUTM(Point(0,0,0,stationCS), ReferenceEllipsoid::eWGS84);
    coreAltitude = stationUTM.GetHeight()/meter;
    /*
    cout << "Core on ground = ("
	 << coreX_stationCS << ", " 
	 << coreY_stationCS << ", "
         << coreAltitude << ")" 
	 << endl << endl;
    */

    // Fill shower particles infos 
    if (!station.HasSimData()){

      Nparticles = 0;

      n_selected = *n;
      N_simulated = *N;

      if(N_simulated >= 10){
	t->Fill();
	t->Write(0,TObject::kWriteDelete,0);
      }

      f->Close("R");
      delete f;

      return true;
    }

    Nparticles = station.GetSimData().ParticlesEnd() - station.GetSimData().ParticlesBegin();

    for (StationSimData::ConstParticleIterator pIt = station.GetSimData().ParticlesBegin(),
	   end = station.GetSimData().ParticlesEnd(); pIt != end; ++pIt) {
      particleType->push_back(pIt->GetType());
      particleEnergy->push_back(pIt->GetKineticEnergy()/GeV);
      const Point pos = pIt->GetPosition();
      const Vector dir = pIt->GetDirection();
      particlePositionX->push_back(pos.GetX(stationCS)/meter);
      particlePositionY->push_back(pos.GetY(stationCS)/meter);
      particlePositionZ->push_back(pos.GetZ(stationCS)/meter);
      particleZenith->push_back(dir.GetTheta(stationCS));
      particleAzimuth->push_back(dir.GetPhi(stationCS));
      particleTime->push_back(pIt->GetTime()/ns);
    }

    // Fill PMTs calibration and simulated traces infos
    for (Station::ConstPMTIterator pmtIt = station.PMTsBegin(sdet::PMTConstants::eAnyType),
	   end = station.PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {
      
      const sdet::PMT& detPMT =
        Detector::GetInstance().GetSDetector().GetStation(station).GetPMT(*pmtIt);

      const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();

      baseline[pmtId] = detPMT.GetBaseline(sdet::PMTConstants::eLowGain);
      baselineRMS[pmtId] = detPMT.GetBaselineRMS(sdet::PMTConstants::eLowGain);

      if (pmtId==0 || pmtId==1 || pmtId==2){
	VEMpeak[pmtId] = detPMT.GetVEMPeak(signature);
	VEMcharge[pmtId] = detPMT.GetVEMCharge(signature);
	HG_LG_ratio[pmtId] = detPMT.GetDA();
      } else if (pmtId==4){
	VEMpeak[pmtId-1] = detPMT.GetVEMPeak(signature);
        VEMcharge[pmtId-1] = detPMT.GetVEMCharge(signature);
        HG_LG_ratio[pmtId-1] = detPMT.GetDA();
      }

      
      if (!pmtIt->HasSimData())
        continue;
      
      const PMTSimData& pSim = pmtIt->GetSimData();

      if (!pSim.HasFADCTrace())
        continue;
      
      const TimeDistributionI& PETD = pSim.GetPETimeDistribution(sevt::StationConstants::eTotal);
      for (TimeDistributionI::SparseIterator iit = PETD.SparseBegin(), end = PETD.SparseEnd(); iit != end; ++iit){
        if(pmtId==0){
          PEtrace_LPMT1_time->push_back(iit->get<0>() * PETD.GetBinning());
          PEtrace_LPMT1->push_back(iit->get<1>());
	  nPE[pmtId] += iit->get<1>();
        } else if(pmtId==1){
          PEtrace_LPMT2_time->push_back(iit->get<0>() * PETD.GetBinning());
          PEtrace_LPMT2->push_back(iit->get<1>());
	  nPE[pmtId] += iit->get<1>();
	} else if(pmtId==2){
          PEtrace_LPMT3_time->push_back(iit->get<0>() * PETD.GetBinning());
          PEtrace_LPMT3->push_back(iit->get<1>());
	  nPE[pmtId] += iit->get<1>();
	} else if(pmtId==3){
          PEtrace_SPMT_time->push_back(iit->get<0>() * PETD.GetBinning());
          PEtrace_SPMT->push_back(iit->get<1>());
	  nPE[pmtId] += iit->get<1>();
	} else if(pmtId==4){
          PEtrace_SSD_time->push_back(iit->get<0>() * PETD.GetBinning());
          PEtrace_SSD->push_back(iit->get<1>());
	  nPE[pmtId] += iit->get<1>();
	}
      }
      
      /*
      const TimeDistributionI& fadcLow = pSim.GetFADCTrace(PMTConstants::eLowGain, sevt::StationConstants::eTotal);
      
      startBin[pmtId] = fadcLow.GetStart();
      stopBin[pmtId] = fadcLow.GetStop();
      
      if(pmtId==0){
        for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	  traceRaw_LPMT1->push_back(fadcLow.At(i));
      } else if(pmtId==1){
        for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	  traceRaw_LPMT2->push_back(fadcLow.At(i));
      } else if(pmtId==2){
        for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
          traceRaw_LPMT3->push_back(fadcLow.At(i));
      } else if(pmtId==3){
	for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
          traceRaw_SPMT->push_back(fadcLow.At(i));
      } else if(pmtId==4){
	for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
          traceRaw_SSD->push_back(fadcLow.At(i));
      }
      */
	
    }

    // Fill trigger simulated traces infos
    for (sevt::StationSimData::TriggerTimeIterator tIt = stationSimData.TriggerTimesBegin();
         tIt != stationSimData.TriggerTimesEnd(); ++tIt) {

      const TimeStamp& trigTime = *tIt;
      const sevt::StationTriggerData& candidateTrigger = stationSimData.GetTriggerData(trigTime);

      if (candidateTrigger.IsThresholdOrTimeOverThreshold() ||
	  candidateTrigger.IsTimeOverThresholdDeconvoluted() ||
	  candidateTrigger.IsMultiplicityOfPositiveSteps())
	{
	
	  IsT1 = true;
      
	  for (Station::ConstPMTIterator pmtIt = station.PMTsBegin(sdet::PMTConstants::eAnyType),
		 end = station.PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {

	    const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();

	    if (!pmtIt->HasSimData())
	      continue;

	    const PMTSimData& pSim = pmtIt->GetSimData();

	    if (!pSim.HasFADCTrace(trigTime))
	      continue;

	    const TraceI& fadcLow = pSim.GetFADCTrace(trigTime,PMTConstants::eLowGain, sevt::StationConstants::eTotal);
	    int maximum = 4095;

	    if(pmtId==0){
	      for (unsigned int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i){
		trace_LPMT1[i] = fadcLow.At(i);
		if (i>=640 && i<740) {
		  charge_LG[0] += trace_LPMT1[i] - baseline[0];
		  if(peak_LG[0]<trace_LPMT1[i]-baseline[0])
		    peak_LG[0] = trace_LPMT1[i] - baseline[0];
		  if(trace_LPMT1[i]>=maximum)
		    satBins_LG[0]++;
		}
	      }
	    } else if(pmtId==1){
	      for (unsigned int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i){
		trace_LPMT2[i] = fadcLow.At(i);
		if (i>=640 && i<740) {
		  charge_LG[1] += trace_LPMT2[i] - baseline[1];
		  if(peak_LG[1]<trace_LPMT2[i]-baseline[1])
		    peak_LG[1] = trace_LPMT2[i] - baseline[1];
		  if(trace_LPMT2[i]>=maximum)
		    satBins_LG[1]++;
		}
	      }
	    } else if(pmtId==2){
	      for (unsigned int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i){
		trace_LPMT3[i] = fadcLow.At(i);
                if (i>=640 && i<740) {
                  charge_LG[2] += trace_LPMT3[i] - baseline[2];
                  if(peak_LG[2]<trace_LPMT3[i]-baseline[2])
                    peak_LG[2] = trace_LPMT3[i] - baseline[2];
                  if(trace_LPMT3[i]>=maximum)
                    satBins_LG[2]++;
		}
	      }
	    } else if(pmtId==3){
	      for (unsigned int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i){
		trace_SPMT[i] = fadcLow.At(i);
                if (i>=640 && i<740) {
                  charge_LG[3] += trace_SPMT[i] - baseline[3];
                  if(peak_LG[3]<trace_SPMT[i]-baseline[3])
                    peak_LG[3] = trace_SPMT[i] - baseline[3];
                  if(trace_SPMT[i]>=maximum)
                    satBins_LG[3]++;
                }
	      }
	    } else if(pmtId==4){
	      for (unsigned int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i){
		trace_SSD[i] = fadcLow.At(i);
		if (i>=640 && i<740) {
		  charge_LG[4] += trace_SSD[i] - baseline[4];
                  if(peak_LG[4]<trace_SSD[i]-baseline[4])
                    peak_LG[4] = trace_SSD[i] - baseline[4];
                  if(trace_SSD[i]>=maximum)
                    satBins_LG[4]++;
		}
	      }
	    }
	    
	  }
	
	  break;
	} 
    }

    ////////

    const int THRESHOLD = 10;

    if( (peak_LG[0]>THRESHOLD && peak_LG[1]>THRESHOLD) ||
        (peak_LG[0]>THRESHOLD && peak_LG[2]>THRESHOLD) ||
        (peak_LG[1]>THRESHOLD && peak_LG[2]>THRESHOLD) ) {

      *n = *n +1;

      n_selected = *n;
      N_simulated = *N;

      t->Fill();
      t->Write(0,TObject::kWriteDelete,0);
    }
    else if( *N >= 10 ){

      n_selected = *n;
      N_simulated = *N;

      t->Fill();
      t->Write(0,TObject::kWriteDelete,0);      
    }

    f->Close("R");
    delete f;

    return true;
  }

}
