#!/bin/bash

for ((i=0; i<10; i++));
do

    if [ $i -lt 10 ]
    then
	cp bootstrapFast.xml bootstrapFast_000"$i".xml
	sed -i "s|xxxx|000$i|g" bootstrapFast_000"$i".xml
    elif [ $i -lt 100 ]
    then
	cp bootstrapFast.xml bootstrapFast_00"$i".xml
        sed -i "s|xxxx|00$i|g" bootstrapFast_00"$i".xml
    elif [ $i -lt 1000 ]
    then
        cp bootstrapFast.xml bootstrapFast_0"$i".xml
        sed -i "s|xxxx|0$i|g" bootstrapFast_0"$i".xml
    else
        cp bootstrapFast.xml bootstrapFast_"$i".xml
        sed -i "s|xxxx|$i|g" bootstrapFast_"$i".xml
    fi
done
