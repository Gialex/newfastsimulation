#!/bin/bash 

WORK=/storage/gpfs_data/auger/ganastasi/SmallShowersSimulation/old
LOGS=${WORK}/results/proton_above10to14_gamma27_10ShowersPerFile/logs

cd ${WORK}
source /storage/gpfs_data/auger/ganastasi/AugerSoftware/set_trunk_r33815_DMliner.sh
./userAugerOffline -b bootstraps/proton_above10to14_gamma27_10ShowersPerFile/bootstrapFull_xxxx.xml &> ${LOGS}/jobFull_xxxx.ou
