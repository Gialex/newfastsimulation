#!/bin/bash

for ((i=0; i<10; i++));
do

    if [ $i -lt 10 ]
    then
	cp EventGenerator.xml EventGenerator_000"$i".xml
	for ((j=0; j<100; j++));
	do
	    dataLine=$[1+100*$i+$j]
	    content=( $(sed -n "$dataLine p" ./randomCores.txt) )
	    cnx=${content[0]}
            cex=${content[1]}
	    sed -i "0,/cn$j/s//$cnx/" EventGenerator_000"$i".xml
	    sed -i "0,/ce$j/s//$cex/" EventGenerator_000"$i".xml
	done
    elif [ $i -lt 100 ]
    then
	cp EventGenerator.xml EventGenerator_00"$i".xml
        for ((j=0; j<100; j++));
        do
            dataLine=$[1+100*$i+$j]
            content=( $(sed -n "$dataLine p" ./randomCores.txt) )
            cnx=${content[0]}
            cex=${content[1]}
            sed -i "0,/cn$j/s//$cnx/" EventGenerator_00"$i".xml
            sed -i "0,/ce$j/s//$cex/" EventGenerator_00"$i".xml
        done
    elif [ $i -lt 1000 ]
    then
        cp EventGenerator.xml EventGenerator_0"$i".xml
        for ((j=0; j<100; j++));
        do
            dataLine=$[1+100*$i+$j]
            content=( $(sed -n "$dataLine p" ./randomCores.txt) )
            cnx=${content[0]}
            cex=${content[1]}
            sed -i "0,/cn$j/s//$cnx/" EventGenerator_0"$i".xml
            sed -i "0,/ce$j/s//$cex/" EventGenerator_0"$i".xml
        done
    else
        cp EventGenerator.xml EventGenerator_"$i".xml
        for ((j=0; j<100; j++));
        do
            dataLine=$[1+100*$i+$j]
            content=( $(sed -n "$dataLine p" ./randomCores.txt) )
            cnx=${content[0]}
            cex=${content[1]}
            sed -i "0,/cn$j/s//$cnx/" EventGenerator_"$i".xml
            sed -i "0,/ce$j/s//$cex/" EventGenerator_"$i".xml
        done
    fi
done
