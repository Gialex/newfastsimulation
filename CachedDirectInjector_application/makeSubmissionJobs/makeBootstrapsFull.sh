#!/bin/bash

for ((i=0; i<10; i++));
do

    if [ $i -lt 10 ]
    then
	cp bootstrapFull.xml bootstrapFull_000"$i".xml
	sed -i "s|xxxx|000$i|g" bootstrapFull_000"$i".xml
    elif [ $i -lt 100 ]
    then
	cp bootstrapFull.xml bootstrapFull_00"$i".xml
        sed -i "s|xxxx|00$i|g" bootstrapFull_00"$i".xml
    elif [ $i -lt 1000 ]
    then
        cp bootstrapFull.xml bootstrapFull_0"$i".xml
        sed -i "s|xxxx|0$i|g" bootstrapFull_0"$i".xml
    else
        cp bootstrapFull.xml bootstrapFull_"$i".xml
        sed -i "s|xxxx|$i|g" bootstrapFull_"$i".xml
    fi
done
