#ifndef _SdSimulationCalibratorOG_SmallShowersWriter_h_
#define _SdSimulationCalibratorOG_SmallShowersWriter_h_

#include <string>
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

namespace evt {
  class Event;
  class ShowerSimData;
}

namespace sevt {
  class SEvent;
  class Station;
}

namespace SdSimulationCalibratorOG {

  void MakeRootOutput(const std::string title);
  bool FillSmallShowersData(const std::string title, 
			    const int sdId,
			    const sevt::Station& station, 
			    const evt::ShowerSimData& simShower,
			    int* n, int* N);
  
}

#endif
