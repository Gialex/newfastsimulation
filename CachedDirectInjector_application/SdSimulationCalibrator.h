#ifndef _SdSimulationCalibratorOG_SdSimulationCalibrator_h_
#define _SdSimulationCalibratorOG_SdSimulationCalibrator_h_

#include <vector>
#include <fwk/VModule.h>
#include <utl/Accumulator.h>


namespace sevt {
  class Station;
}

namespace SdSimulationCalibratorOG {

  /**
    \class SdSimulationCalibrator

    \brief Extracts calibration constants for a tank/electronics sim combo.

    \author  Tom Paul
    \date    April 2004
    \version $Id: SdSimulationCalibrator.h 33232 2020-03-04 17:46:51Z darko $
  */

  class SdSimulationCalibrator : public fwk::VModule {

  public:
    virtual ~SdSimulationCalibrator() { }

    fwk::VModule::ResultFlag Init();
    fwk::VModule::ResultFlag Run(evt::Event& theEvent);
    fwk::VModule::ResultFlag Finish();

    std::string GetSVNId() const
    { return std::string("$Id: SdSimulationCalibrator.h 33232 2020-03-04 17:46:51Z darko $"); }

  private:
    unsigned int fSingleTankId = 0;
    bool fSmallShowersAnalysis = false;
    int fIsUUB = 0;
    int fHardwareType = 0;
    int fNParticles = 0;

    int fN_selected = 0;
    int fN_simulated = 0;

    std::string fSignature;
    std::string fROOTtitle;

    std::vector<double> fPeak;
    std::vector<double> fCharge;

    typedef utl::Accumulator::SampleStandardDeviation Sigma;
    std::vector<std::pair<Sigma, Sigma>> fAccPeakCharge;

    void CalculateCalibrationConstants(const sevt::Station& station);

    REGISTER_MODULE("SdSimulationCalibratorOG", SdSimulationCalibrator);

  };

}


#endif
