#ifndef _CachedDirectInjectorOG_CachedDirectInjector_h_
#define _CachedDirectInjectorOG_CachedDirectInjector_h_

#include <fwk/VModule.h>
#include <utl/Particle.h>
#include <utl/ShadowPtr.h>
#include <utl/ShowerParticleIterator.h>
#include <map>


namespace CachedDirectInjectorOG {

  /**
    \class CachedDirectInjector

    \brief Inject an unthinned shower into the array.

    Injects particles from an unthinned
    shower (particles with weight == 1) directly into tanks, without regenerating.
    Due to large numbers of particles,often rings or patches of particles are injected into selected (dense) tanks,
    causing the event not to be triggered. Therefore, a trigger can be forced by feeding a pattern of particles into
    a set of tanks.
    This is based on an ancient version of CachedShowerRegenerator.

    \author Ronald Bruijn, Gialex Anastasi
    \version $Id: CachedDirectInjector.h 33838 2020-10-16 14:50:26Z ganastasi $
    \ingroup SDSimModules
  */

  struct InjectedParticle {

    InjectedParticle() { }

    InjectedParticle(const int id, const double r, const double phi, const double height, const double stime,
                     const int type, const int source, const double posx, const double posy, const double posz,
                     const double dirx, const double diry, const double dirz, const double time,
                     const double weight, const double ek) :
      fId(id),
      fR(r),
      fPhi(phi),
      fHeight(height),
      fSTime(stime),
      fType(type),
      fSource(source),
      fPosX(posx),
      fPosY(posy),
      fPosZ(posz),
      fDirX(dirx),
      fDirY(diry),
      fDirZ(dirz),
      fTime(time),
      fWeight(weight),
      fEk(ek)
    { }

    int fId = 0;
    double fR = 0;
    double fPhi = 0;
    double fHeight = 0;
    double fSTime = 0;
    int fType = 0;
    int fSource = 0;
    double fPosX = 0;
    double fPosY = 0;
    double fPosZ = 0;
    double fDirX = 0;
    double fDirY = 0;
    double fDirZ = 0;
    double fTime = 0;
    double fWeight = 0;
    double fEk = 0;

    friend std::istream& operator>>(std::istream& is, InjectedParticle& part);

  };


  class CachedDirectInjector : public fwk::VModule {

  public:
    CachedDirectInjector() {}
    
    VModule::ResultFlag Init();
    VModule::ResultFlag Run(evt::Event& event);
    VModule::ResultFlag Finish();

  private:
    double fParticleTankDistanceCut = 10.0*utl::km;
    unsigned int fMaxParticles = 10000;
    bool fUseDenseOnly = false;
    bool fUseDense = false;
    
    utl::ShowerParticleIterator fParticleIt;
    std::map<int, utl::TimeInterval> fEarliestTime;
    std::map<int, std::vector<InjectedParticle>> fTankToInjectedParticles;

    REGISTER_MODULE("CachedDirectInjector", CachedDirectInjector);

  };

}


#endif
