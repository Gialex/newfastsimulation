// $Id: SdSimulationCalibrator.cc 33232 2020-03-04 17:46:51Z darko $
#include <sstream>

#include "SdSimulationCalibrator.h"
#include "SmallShowersWriter.h"

#include <det/Detector.h>

#include <sdet/SDetector.h>
#include <sdet/Station.h>
#include <sdet/PMTConstants.h>

#include <evt/Event.h>
#include <evt/ShowerSimData.h>

#include <fwk/CentralConfig.h>

#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sevt/StationSimData.h>
#include <sevt/StationCalibData.h>
#include <sevt/PMTCalibData.h>
#include <sevt/PMTSimData.h>
#include <sevt/PMT.h>

#include <utl/AugerUnits.h>
#include <utl/MathConstants.h>
#include <utl/Particle.h>
#include <utl/PhysicalConstants.h>
#include <utl/TimeStamp.h>
#include <utl/ErrorLogger.h>
#include <utl/Point.h>
#include <utl/Vector.h>
#include <utl/Reader.h>
#include <utl/TimeDistribution.h>
#include <utl/TimeDistributionAlgorithm.h>
#include <utl/TabularStream.h>

#include <fwk/RunController.h>

using namespace utl;
using namespace sevt;
using namespace fwk;
using namespace evt;
using namespace det;
using namespace std;


namespace SdSimulationCalibratorOG {

  VModule::ResultFlag
  SdSimulationCalibrator::Init()
  {
    INFO(".");

    const Branch simCalibBranch = CentralConfig::GetInstance()->GetTopBranch("SdSimulationCalibrator");

    simCalibBranch.GetChild("singleTankId").GetData(fSingleTankId);

    fAccPeakCharge.resize(Detector::GetInstance().GetSDetector().GetStation(fSingleTankId).GetNPMTs());

    const string hardwareType = simCalibBranch.GetChild("hardware").Get<string>();

    if (hardwareType == "wcdLarge")
      fHardwareType = sdet::PMTConstants::eWaterCherenkovLarge;
    else if (hardwareType == "wcdSmall")
      fHardwareType = sdet::PMTConstants::eWaterCherenkovSmall;
    else if (hardwareType == "ssd")
      fHardwareType = sdet::PMTConstants::eScintillator;
    else if (hardwareType == "all")
      fHardwareType = sdet::PMTConstants::eAnyType;
    else {
      ERROR("Hardware type not set in xml or not yet implemented in SdSimulationCalibrator");
      return eFailure;
    }

    // Small showers analysis
    Branch smallShowersBranch = simCalibBranch.GetChild("SmallShowersSimulation");
    if (smallShowersBranch){
      smallShowersBranch.GetData(fSmallShowersAnalysis);
      if (fSmallShowersAnalysis){
	Branch smallShowersTitle = simCalibBranch.GetChild("SmallShowersOutputTitle"); 
	if (smallShowersTitle){
	  fROOTtitle = smallShowersTitle.Get<string>();
	  MakeRootOutput(fROOTtitle);
	}
	else{
	  ERROR("Missing title for small shower analysis.");
	  return eFailure;
	}
      }
    }

    return eSuccess;
  }


  VModule::ResultFlag
  SdSimulationCalibrator::Run(Event& event)
  {
    INFO(".");

    if (!event.HasSEvent()) {
      ERROR("Non-existent SEvent.");
      return eFailure;
    }

    SEvent& sEvent = event.GetSEvent();

    if (!sEvent.HasStation(fSingleTankId)) {
      ostringstream err;
      err << "Non-existent station " << fSingleTankId;
      ERROR(err);
      return eFailure;
    }
    fSignature = sEvent.GetStation(fSingleTankId).GetSimData().GetSimulatorSignature();
    fIsUUB = Detector::GetInstance().GetSDetector().GetStation(fSingleTankId).IsUUB();

    // Tha small showers analysis is alternative to the standard
    // simulated calibration procedure, and requires a fully 
    // simulated CORSIKA shower to have been simulated
    if (fSmallShowersAnalysis){

      if (!event.HasSimShower()){
	ERROR("Non-existent simulated event for small showers analysis.");
	return eFailure;
      }

      const evt::ShowerSimData& simShower = event.GetSimShower();
      const int sdId = sEvent.GetHeader().GetId();
      if (FillSmallShowersData(fROOTtitle, sdId,
			       sEvent.GetStation(fSingleTankId),
			       simShower, &fN_selected, &fN_simulated)){

	ostringstream info;
	info << " Selected " << fN_selected << " events over "
	     << fN_simulated << " attempts for small shower id " << sdId 
	     << "\n";
	INFO(info);

	if(fN_selected >= 1 || fN_simulated >= 10){
	  fN_selected = 0;
	  fN_simulated = 0;
	  return eBreakLoop;
	}
	
	return eSuccess;
      }
      else{
	ERROR("Impossible to find ROOT file / TTree for small showers analysis output.");
	return eFailure;
      }
    }
    // end of block for small showers analysis


    CalculateCalibrationConstants(sEvent.GetStation(fSingleTankId));
    
    for (unsigned int i = 0, n = fAccPeakCharge.size(); i < n; ++i) {
      auto& pc = fAccPeakCharge[i];
      pc.first(fPeak[i]);
      pc.second(fCharge[i]);
    }
    
    // Now copy the results of the calibration to a new Event, then copy back
    // into the old event to ensure only the calibration information is kept
    
    Event newEvent;
    newEvent.MakeSEvent();
    
    SEvent& newSEvent = newEvent.GetSEvent();
    
    for (SEvent::StationIterator sIt = sEvent.StationsBegin(), end = sEvent.StationsEnd();
	 sIt != end; ++sIt) {
      
      if (!sIt->HasCalibData())
	continue;
      
      const int sId = sIt->GetId();
      newSEvent.MakeStation(sId);
      Station& newStation = newSEvent.GetStation(sId);
      newStation.MakeCalibData();
      StationCalibData& newCalibData = newStation.GetCalibData();
      newCalibData = sIt->GetCalibData();
      
      for (Station::ConstPMTIterator pmtIt = sIt->PMTsBegin(sdet::PMTConstants::eAnyType),
	     end = sIt->PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {
	
	if (!pmtIt->HasCalibData())
	  continue;
	
	// This throws an exception if there isn't a PMT for this id. There
	// should be, since they are created when stations are created, so
	// the code should die here if one doesn't exist.
	
	PMT& pmt = newStation.GetPMT(pmtIt->GetId());
	
	if (!pmt.HasCalibData())
	  pmt.MakeCalibData();
	
	PMTCalibData& newPMTCalib = pmt.GetCalibData();
	newPMTCalib = pmtIt->GetCalibData();
	
	const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();
	const auto& pc = fAccPeakCharge[pmtId];
	newPMTCalib.SetVEMPeak(pc.first.GetAverage());
	newPMTCalib.SetVEMCharge(pc.second.GetAverage());
	
      }
    }
    
    event = newEvent;

    return eSuccess;
  }


  VModule::ResultFlag
  SdSimulationCalibrator::Finish()
  {

    if (fSmallShowersAnalysis)
      return eSuccess;

    TabularStream tab(" r r r | . . . | . . .");
    tab << "PMT" << endc << "N" << endc << "NPar" << endc << "<Peak>" << endc
        << "Err<>" << endc << "StdDev" << endc << "<Charge>" << endc << "Err<>"
        << endc << "StdDev" << endr << hline;
    int pmtId = 1;
    unsigned int numRuns = 0;
    for (vector<pair<Sigma, Sigma> >::iterator it = fAccPeakCharge.begin(),
         end = fAccPeakCharge.end(); it != end; ++it, ++pmtId) {
      tab << pmtId << endc
          << it->first.GetN() << endc
          << fNParticles << endc
          << it->first.GetAverage() << endc
          << it->first.GetAverageError() << endc
          << it->first.GetStandardDeviation() << endc
          << it->second.GetAverage() << endc
          << it->second.GetAverageError() << endc
          << it->second.GetStandardDeviation() << endr;

      numRuns = it->first.GetN();
    }
    tab << delr;

    // Pull some info from the ParticleInjector for documentation purposes.
    // NB this expects one to use ParticleInjectorNEU!
    Branch injB = CentralConfig::GetInstance()->GetTopBranch("ParticleInjector");
    const double muEnergy = injB.GetChild("Energy").GetChild("Discrete").GetChild("x").Get<double>();

    ostringstream info;
    info << "The simulated calibration constants were set as follows:\n\n"
         << tab << "\n\n"
            "The following XML snipped should be pasted into "
            "Framework/SDetector/SdSimCalibrationConstants.xml.in "
            "with the other PMT calibrations from the same simModule:\n\n"
         << "<!-- "
         << fSignature << " tank sim, "
         << numRuns << " runs, "
         << fNParticles << " particle(s) per run, "
            "energy = " << muEnergy / GeV
         << " GeV -->\n"
            "<simModule name='" << fSignature << "'>\n"
            "  <electronics isUUB='" << fIsUUB << "'>\n";

    int pId = 0;
    for (const auto& pc : fAccPeakCharge) {
      info << "    <PMT id='" << ++pId << "'>\n"
              "      <peak> " << pc.first.GetAverage() << " </peak>\n"
              "      <charge> " << pc.second.GetAverage() << " </charge>\n"
              "    </PMT>\n";
    }
    info << "  </electronics>\n</simModule>\n";

    INFO(info);

    return eSuccess;
  }


  void
  SdSimulationCalibrator::CalculateCalibrationConstants(const sevt::Station& station)
  {
    const unsigned int nPMTs =
      Detector::GetInstance().GetSDetector().GetStation(station).GetNPMTs();
    fPeak.assign(nPMTs, 0);
    fCharge.assign(nPMTs, 0);

    const unsigned int numParticles =
      station.HasSimData() ?
      station.GetSimData().ParticlesEnd() - station.GetSimData().ParticlesBegin() : 0;

    if (numParticles <= 0)
      return;

    fNParticles = numParticles;

    if (numParticles != 1 && fHardwareType != sdet::PMTConstants::eWaterCherenkovSmall) {
      ERROR("Simulation calibrator is throwing more than one particle. Only use with smallPMT!");
      exit(1);
    }

    for (Station::ConstPMTIterator pmtIt = station.PMTsBegin(sdet::PMTConstants::eAnyType),
         end = station.PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {

      if (!pmtIt->HasSimData())
        continue;

      const PMTSimData& pSim = pmtIt->GetSimData();

      if (!pSim.HasFADCTrace())
        continue;

      const sdet::PMT& detPMT =
        Detector::GetInstance().GetSDetector().GetStation(station).GetPMT(*pmtIt);

      const double baselineHG = detPMT.GetBaseline(sdet::PMTConstants::eHighGain);

      const TimeDistributionI& fadcHigh = pSim.GetFADCTrace();

      const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();

      fPeak[pmtId] = 0;
      fCharge[pmtId] = 0;

      // only process desired pmt type, unless all
      if (!(fHardwareType == sdet::PMTConstants::eAnyType || detPMT.GetType() == fHardwareType))
        continue;

      // narrow down the signal region
      const double threshold = baselineHG + 15;
      const int start = fadcHigh.GetStart();
      int signalStart = start;
      const int stop = fadcHigh.GetStop();
      for ( ; signalStart <= stop; ++signalStart)
        if (fadcHigh.At(signalStart) >= threshold)
          break;
      int signalStop = start;
      for (int i = signalStart + 1; i <= stop; ++i) {
        if (fadcHigh.At(i) >= threshold) {
          signalStop = start;
          continue;
        }
        if (signalStop == start && fadcHigh.At(i) < threshold)
          signalStop = i;
      }
      if (signalStop == start) {
        ostringstream warn;
        warn << "No signal found in PMT " << pmtId;
        WARNING(warn);
        continue;
      }
      signalStart = max(signalStart - 10, start);
      signalStop = min(signalStop + 20, stop);
      double maxVal = -baselineHG;
      double charge = 0;
      for (int i = signalStart; i <= signalStop; ++i) {
        const double val = fadcHigh.At(i) - baselineHG;
        if (val > maxVal)
          maxVal = val;
        charge += val;
      }

      fPeak[pmtId] = maxVal;
      fCharge[pmtId] = charge;
      if (numParticles != 1) {
        const double correctionFactor = 1. / numParticles;
        fPeak[pmtId] *= correctionFactor;
        fCharge[pmtId] *= correctionFactor;
      }

    }
  }


}
