#ifndef _EventGeneratorOG_EventGenerator_h_
#define _EventGeneratorOG_EventGenerator_h_

#include <fwk/VModule.h>
#include <sdet/Station.h>
#include <utl/TimeStamp.h>
#include <utl/UTMPoint.h>
#include <evt/ShowerSimData.h>
#include <evt/RadioSimulation.h>
#include <utl/config.h>
#include <boost/tuple/tuple.hpp>
#include <string>
#include <list>
#include <TH2.h>

namespace utl {
  class RandomEngine;
  class Branch;
}

namespace evt {
  class Event;
}

namespace EventGeneratorOG {

  class EventGenerator : public fwk::VModule {

    enum EMode {
      eUndefined,
      eSD,
      eFD,
      eHy,
      eMD, // MD only
      eXD, // SD+MD    (stands for eXtended SD)
      eXH // SD+MD+FD (stands for eXtended Hy)
    };

  public:
    EventGenerator() { }
    virtual ~EventGenerator() { }

    fwk::VModule::ResultFlag Init();
    fwk::VModule::ResultFlag Run(evt::Event& event);
    fwk::VModule::ResultFlag Finish();

    std::string GetSVNId() const
    { return std::string("$Id: EventGenerator.h 33743 2020-08-04 07:20:37Z fschlueter $"); }

  private:
    boost::tuple<double, double> GenerateArrayCentricRandomizedCore();
    boost::tuple<double, double> GenerateArrayCentricRandomizedCoreAroundRandomStation();
    boost::tuple<double, double, double> GenerateArrayCentricListedCore();
    boost::tuple<double, double> GenerateEyeCentricCore(const double energy);
    boost::tuple<double, double, double> GenerateSphereCentricCore(evt::ShowerSimData& theShower);

    sdet::Station::StationIdCollection GetInfillCrown(const sdet::Station& centralStation);
    boost::tuple<double, double> GenerateCoreAroundStation();

    double Rcutoff(const double lgE);
    double RcutoffCherenkovHECO(const double lgE);
    double MaximumRp(const double lgE);
    void FlagHoleStations(evt::Event& event) const;

    /// Calculates average offset between the locations of sim. radio pulses and the position of the RDetector stations 
    utl::Vector GetCoreShiftForRadioSimulation(evt::RadioSimulation& radioSim);

    // Returns id of clostest station to a given point
    int FindClosestStationFromPoint(const utl::Point& pt, const rdet::RDetector& rDet, double maxDistanceFactor);
    
    EMode fMode = eUndefined;

    bool fUseRandomStation = false;
    bool fUseRandomInfillStation = false;
    int fStationId = 0;
    double fTileRadius = 0;
    double fDeltaNorthing = 0;
    double fDeltaEasting = 0; // Shifts in N and E over the array
    double fNorthing = 0;
    double fEasting = 0;
    double fAltitude = 0;  // Core location of the shower in UTM
    int fZone = -1; // UTM zone
    char fBand = '\0'; // UTM band

    typedef std::list<boost::tuple<double, double, double, int, char>> CoreList;
    CoreList fCorePositions;
    CoreList::iterator fCorePositionsIt;

    // TimeStamp variables
    int fNEvents = 0;
    bool fTimeOrdered = false;
    bool fTimeRandomized = false;
    utl::TimeStamp fStartDate;
    utl::TimeStamp fEndDate;
    bool fSampleTimes = false;
    std::list<double> fTimeList;

    std::string fLibraryIdentifier;
    std::string fFormat;
    std::vector<unsigned int> fSdIdFormat;
    unsigned int fEventNumber = 0;

    // Slice mode variables
    bool fUseSimCores = false;
    int fSimCoreCount = 0;
    bool fEyeCentric = false;
    bool fInSphere = false;
    double fSphereRadius = 0;
    bool fSkipUpgoing = false;
    bool fLimitRp = false;
    bool fUpgoingShower = false;
    bool fRpTooLarge = false;
    boost::tuple<double, double, double, int, char> fSphereCenter;
    int fEyeid = 0;
    double fTelid = 0;
    double fMinDist = 0;
    double fMaxDist = 0;
    bool fRMaxEnergyDependent = false;
    double fDeltaPhi = 0;
    bool fGeometryCheckCherenkovHECO = false;
    TH2D* fMaxVA = nullptr;

    // Updating behaviour.
    bool fInvalidateData = false;
    bool fInvalidateComponents = false;

    bool fUseRadioCorePosition = false;
    bool fUseRadioEventTime = false;

    int fMaximumDistance = 10;

    utl::RandomEngine* fRandomEngine = nullptr;

    REGISTER_MODULE("EventGeneratorOG", EventGenerator);

  };

}


#endif
