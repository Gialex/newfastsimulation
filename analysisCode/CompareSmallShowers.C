#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TPaveStats.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <THistPainter.h>
#include <TMath.h>
#include <TVector3.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;


void CompareSmallShowers(const char* rootTitle1, const char* rootTitle2,
						 const char* name1 = "fast", const char* name2 = "full"){
  
  if(gSystem->AccessPathName(rootTitle1)){
    cout << "Root file " << rootTitle1 << " not found." << "\n";
    exit(1);
  }
  TFile* f1 = TFile::Open(rootTitle1);
  
  if(gSystem->AccessPathName(rootTitle2)){
    cout << "Root file " << rootTitle2 << " not found." << "\n";
    exit(1);
  }
  TFile* f2 = TFile::Open(rootTitle2);
  
  // Open TTree and set Branch Addresses
  
  if( !f1->GetListOfKeys()->Contains("simCalibData") ){
    cout << "TTree -simCalibData- not found inside " << rootTitle1 << "\n";
    exit(1);
  }
  
  if( !f2->GetListOfKeys()->Contains("simCalibData") ){
    cout << "TTree -simCalibData- not found inside " << rootTitle2 << "\n";
    exit(1);
  }
  
	
  TTree* t1 = (TTree*)f1->Get("simCalibData");
  unsigned int nEntries1 = t1->GetEntries();
  Int_t SdId1;
  t1->SetBranchAddress("SdId", &SdId1);
  Int_t primaryType1;
  t1->SetBranchAddress("primaryType", &primaryType1);
  Double_t primaryEnergy1;
  t1->SetBranchAddress("primaryEnergy", &primaryEnergy1);
  Double_t primaryZenith1;
  t1->SetBranchAddress("primaryZenith", &primaryZenith1);
  Double_t primaryAzimuth1;
  t1->SetBranchAddress("primaryAzimuth", &primaryAzimuth1);
  Double_t coreX_stationCS1;
  t1->SetBranchAddress("coreX_stationCS", &coreX_stationCS1);
  Double_t coreY_stationCS1;
  t1->SetBranchAddress("coreY_stationCS", &coreY_stationCS1);
  Double_t coreAltitude1;
  t1->SetBranchAddress("coreAltitude", &coreAltitude1);
  UInt_t Nparticles1;
  t1->SetBranchAddress("Nparticles", &Nparticles1);
  /*
  vector<Double_t>* particleEnergy1 = NULL;
  t1->SetBranchAddress("particleEnergy", &particleEnergy1);
  vector<Double_t>* particleTime1 = NULL;
  t1->SetBranchAddress("particleTime", &particleTime1);
  vector<Double_t>* particleZenith1 = NULL;
  t1->SetBranchAddress("particleZenith", &particleZenith1);
  vector<Double_t>* particleAzimuth1 = NULL;
  t1->SetBranchAddress("particleAzimuth", &particleAzimuth1);
  vector<Double_t>* particlePositionX1 = NULL;
  t1->SetBranchAddress("particlePositionX", &particlePositionX1);
  vector<Double_t>* particlePositionY1 = NULL;
  t1->SetBranchAddress("particlePositionY", &particlePositionY1);
  vector<Double_t>* particlePositionZ1 = NULL;
  t1->SetBranchAddress("particlePositionZ", &particlePositionZ1);
  */
  
  Double_t VEMcharge1[4] = { 0 };
  t1->SetBranchAddress("VEMcharge", &VEMcharge1);
  Double_t VEMpeak1[4] = { 0 };
  t1->SetBranchAddress("VEMpeak", &VEMpeak1);
  Double_t HG_LG_ratio1[4] = { 0 };
  t1->SetBranchAddress("HG_LG_ratio", &HG_LG_ratio1);

  Int_t nPE1[5] = { 0 };
  t1->SetBranchAddress("nPE", &nPE1);

  Bool_t IsT11;
  t1->SetBranchAddress("IsT1", &IsT11);
  Double_t peak_LG1[5] = { 0 };
  t1->SetBranchAddress("peak_LG", &peak_LG1);
  Double_t charge_LG1[5] = { 0 };
  t1->SetBranchAddress("charge_LG", &charge_LG1);
  UInt_t satBins_LG1[5] = { 0 };
  t1->SetBranchAddress("saturatedBins_LG", &satBins_LG1);


  TTree* t2 = (TTree*)f2->Get("simCalibData");
  unsigned int nEntries2 = t2->GetEntries();
  Int_t SdId2;
  t2->SetBranchAddress("SdId", &SdId2);
  Int_t primaryType2;
  t2->SetBranchAddress("primaryType", &primaryType2);
  Double_t primaryEnergy2;
  t2->SetBranchAddress("primaryEnergy", &primaryEnergy2);
  Double_t primaryZenith2;
  t2->SetBranchAddress("primaryZenith", &primaryZenith2);
  Double_t primaryAzimuth2;
  t2->SetBranchAddress("primaryAzimuth", &primaryAzimuth2);
  Double_t coreX_stationCS2;
  t2->SetBranchAddress("coreX_stationCS", &coreX_stationCS2);
  Double_t coreY_stationCS2;
  t2->SetBranchAddress("coreY_stationCS", &coreY_stationCS2);
  Double_t coreAltitude2;
  t2->SetBranchAddress("coreAltitude", &coreAltitude2);
  UInt_t Nparticles2;
  t2->SetBranchAddress("Nparticles", &Nparticles2);
  /*
  vector<Double_t>* particleEnergy2 = NULL;
  t2->SetBranchAddress("particleEnergy", &particleEnergy2);
  vector<Double_t>* particleTime2 = NULL;
  t2->SetBranchAddress("particleTime", &particleTime2);
  vector<Double_t>* particleZenith2 = NULL;
  t2->SetBranchAddress("particleZenith", &particleZenith2);
  vector<Double_t>* particleAzimuth2 = NULL;
  t2->SetBranchAddress("particleAzimuth", &particleAzimuth2);
  vector<Double_t>* particlePositionX2 = NULL;
  t2->SetBranchAddress("particlePositionX", &particlePositionX2);
  vector<Double_t>* particlePositionY2 = NULL;
  t2->SetBranchAddress("particlePositionY", &particlePositionY2);
  vector<Double_t>* particlePositionZ2 = NULL;
  t2->SetBranchAddress("particlePositionZ", &particlePositionZ2);
  */
  Double_t VEMcharge2[4] = { 0 };
  t2->SetBranchAddress("VEMcharge", &VEMcharge2);
  Double_t VEMpeak2[4] = { 0 };
  t2->SetBranchAddress("VEMpeak", &VEMpeak2);
  Double_t HG_LG_ratio2[4] = { 0 };
  t2->SetBranchAddress("HG_LG_ratio", &HG_LG_ratio2);

  Int_t nPE2[5] = { 0 };
  t2->SetBranchAddress("nPE", &nPE2);

  Bool_t IsT12;
  t2->SetBranchAddress("IsT1", &IsT12);
  Double_t peak_LG2[5] = { 0 };
  t2->SetBranchAddress("peak_LG", &peak_LG2);
  Double_t charge_LG2[5] = { 0 };
  t2->SetBranchAddress("charge_LG", &charge_LG2);
  UInt_t satBins_LG2[5] = { 0 };
  t2->SetBranchAddress("saturatedBins_LG", &satBins_LG2);

		
//////////////////////////////////////////////////////////////////////////////////////////	
// Histograms definitions

  TH1D *hPEDiff1 = new TH1D("hPEDiff1","hPEDiff1",101,-99,99);	
  TH1D *hPEDiff2 = new TH1D("hPEDiff2","hPEDiff2",101,-99,99);	
  TH1D *hPEDiff3 = new TH1D("hPEDiff3","hPEDiff3",101,-99,99);
  TH1D *hPEDiff4 = new TH1D("hPEDiff4","hPEDiff4",101,-99,99);	
  TH1D *hPEDiff5 = new TH1D("hPEDiff5","hPEDiff5",101,-99,99);
  
  TGraph *gPEDiff1 = new TGraph();	
  TGraph *gPEDiff2 = new TGraph();	
  TGraph *gPEDiff3 = new TGraph();
  TGraph *gPEDiff4 = new TGraph();	
  TGraph *gPEDiff5 = new TGraph();	
  
  TProfile* pPEDiff1 = new TProfile("pPEDiff1","pPEDiff1",35,0,7,-999,999); 
  	//pPEDiff1->SetErrorOption("s");
  pPEDiff1->SetLineColor(kRed+1); pPEDiff1->SetMarkerColor(kRed+1); pPEDiff1->SetMarkerStyle(24);
  TProfile* pPEDiff2 = new TProfile("pPEDiff2","pPEDiff2",35,0,7,-999,999); 
  	//pPEDiff2->SetErrorOption("s");
  pPEDiff2->SetLineColor(kRed+1); pPEDiff2->SetMarkerColor(kRed+1); pPEDiff2->SetMarkerStyle(24);
  TProfile* pPEDiff3 = new TProfile("pPEDiff3","pPEDiff3",35,0,7,-999,999); 
  	//pPEDiff3->SetErrorOption("s");
  pPEDiff3->SetLineColor(kRed+1); pPEDiff3->SetMarkerColor(kRed+1); pPEDiff3->SetMarkerStyle(24);  
  TProfile* pPEDiff4 = new TProfile("pPEDiff4","pPEDiff4",30,0,6,-999,999); 
  	//pPEDiff4->SetErrorOption("s");
  pPEDiff4->SetLineColor(kRed+1); pPEDiff4->SetMarkerColor(kRed+1); pPEDiff4->SetMarkerStyle(24); 


  TH1D *hChargeDiff1 = new TH1D("hChargeDiff1","hChargeDiff1",101,-99,99);	
  TH1D *hChargeDiff2 = new TH1D("hChargeDiff2","hChargeDiff2",101,-99,99);	
  TH1D *hChargeDiff3 = new TH1D("hChargeDiff3","hChargeDiff3",101,-99,99);
  TH1D *hChargeDiff4 = new TH1D("hChargeDiff4","hChargeDiff4",101,-99,99);	
  TH1D *hChargeDiff5 = new TH1D("hChargeDiff5","hChargeDiff5",101,-99,99);
  
  TGraph *gChargeDiff1 = new TGraph();	
  TGraph *gChargeDiff2 = new TGraph();	
  TGraph *gChargeDiff3 = new TGraph();
  TGraph *gChargeDiff4 = new TGraph();	
  TGraph *gChargeDiff5 = new TGraph();	
  
  TProfile* pChargeDiff1 = new TProfile("pChargeDiff1","pChargeDiff1",25,0,5,-10,10); 
  //pChargeDiff1->SetErrorOption("s");
  pChargeDiff1->SetLineColor(kRed+1); pChargeDiff1->SetMarkerColor(kRed+1); pChargeDiff1->SetMarkerStyle(24);
  TProfile* pChargeDiff2 = new TProfile("pChargeDiff2","pChargeDiff2",25,0,5,-10,10); 
  //pChargeDiff2->SetErrorOption("s");
  pChargeDiff2->SetLineColor(kRed+1); pChargeDiff2->SetMarkerColor(kRed+1); pChargeDiff2->SetMarkerStyle(24);
  TProfile* pChargeDiff3 = new TProfile("pChargeDiff3","pChargeDiff3",25,0,5,-10,10); 
  //pChargeDiff3->SetErrorOption("s");
  pChargeDiff3->SetLineColor(kRed+1); pChargeDiff3->SetMarkerColor(kRed+1); pChargeDiff3->SetMarkerStyle(24);  
  TProfile* pChargeDiff4 = new TProfile("pChargeDiff4","pChargeDiff4",25,0,5,-10,10); 
  //pChargeDiff4->SetErrorOption("s");
  pChargeDiff4->SetLineColor(kRed+1); pChargeDiff4->SetMarkerColor(kRed+1); pChargeDiff4->SetMarkerStyle(24);   
  
	
  TH1D *hSignDiff1 = new TH1D("hSignDiff1","hSignDiff1",101,-100,100);	
  TH1D *hSignDiff2 = new TH1D("hSignDiff2","hSignDiff2",101,-100,100);	
  TH1D *hSignDiff3 = new TH1D("hSignDiff3","hSignDiff3",101,-100,100);
  
  TGraph *gSignDiff1 = new TGraph();	
  TGraph *gSignDiff2 = new TGraph();	
  TGraph *gSignDiff3 = new TGraph();
	
  TProfile* pSignDiff1 = new TProfile("pSignDiff1","pSignDiff1",12,0.5,2.9,-1000,1000); 
  //pSignDiff1->SetErrorOption("s");
  pSignDiff1->SetLineColor(kBlack); pSignDiff1->SetMarkerColor(kBlack); pSignDiff1->SetMarkerStyle(24);
  TProfile* pSignDiff2 = new TProfile("pSignDiff2","pSignDiff2",12,0.5,2.9,-1000,1000); 
  //pSignDiff2->SetErrorOption("s");
  pSignDiff2->SetLineColor(kBlack); pSignDiff2->SetMarkerColor(kBlack); pSignDiff2->SetMarkerStyle(24);
  TProfile* pSignDiff3 = new TProfile("pSignDiff3","pSignDiff3",12,0.5,2.9,-1000,1000); 
  //pSignDiff3->SetErrorOption("s");
  pSignDiff3->SetLineColor(kBlack); pSignDiff3->SetMarkerColor(kBlack); pSignDiff3->SetMarkerStyle(24);


  TGraph *gSignDiff1_vsCoreDist = new TGraph();	
  TGraph *gSignDiff2_vsCoreDist = new TGraph();	
  TGraph *gSignDiff3_vsCoreDist = new TGraph();
  
  TProfile* pSignDiff1_vsCoreDist = new TProfile("pSignDiff1_vsCoreDist","pSignDiff1_vsCoreDist",12,0.8,2,-1000,1000); 
  //pSignDiff1_vsCoreDist->SetErrorOption("s");
  pSignDiff1_vsCoreDist->SetLineColor(kBlack); pSignDiff1_vsCoreDist->SetMarkerColor(kBlack); pSignDiff1_vsCoreDist->SetMarkerStyle(24);
  TProfile* pSignDiff2_vsCoreDist = new TProfile("pSignDiff2_vsCoreDist","pSignDiff2_vsCoreDist",12,0.8,2,-1000,1000); 
  //pSignDiff2_vsCoreDist->SetErrorOption("s");
  pSignDiff2_vsCoreDist->SetLineColor(kBlack); pSignDiff2_vsCoreDist->SetMarkerColor(kBlack); pSignDiff2_vsCoreDist->SetMarkerStyle(24);
  TProfile* pSignDiff3_vsCoreDist = new TProfile("pSignDiff3_vsCoreDist","pSignDiff3_vsCoreDist",12,0.8,2,-1000,1000); 
  //pSignDiff3_vsCoreDist->SetErrorOption("s");
  pSignDiff3_vsCoreDist->SetLineColor(kBlack); pSignDiff3_vsCoreDist->SetMarkerColor(kBlack); pSignDiff3_vsCoreDist->SetMarkerStyle(24);

  /*
    TH1D *hDiffPartPosX = new TH1D("hDiffPartPosX","hDiffPartPosX",201,-0.01,0.01);	
    TH1D *hDiffPartPosY = new TH1D("hDiffPartPosY","hDiffPartPosY",201,-0.01,0.01);	
    TH1D *hDiffPartPosZ = new TH1D("hDiffPartPosZ","hDiffPartPosZ",201,-0.1,0.1);		
    TH1D *hDiffPartTime = new TH1D("hDiffPartTime","hDiffPartTime",201,-0.0001,0.0001);
    TH1D *hDiffPartEnergy = new TH1D("hDiffPartEnergy","hDiffPartEnergy",101,-100,100);		
    TH1D *hDiffPartZenith = new TH1D("hDiffPartZenith","hDiffPartZenith",101,-100,100);	
    TH1D *hDiffPartAzimuth = new TH1D("hDiffPartAzimuth","hDiffPartAzimuth",101,-100,100);	
  */
  
  //////////////////////////////////////////////////////////////////////////////////////////	
  // Loop over the events 
	
  int fivepercent = ceil(float(nEntries1)/20);
  int counterFound = 0;
  int counterSelected = 0;
  int event_with_different_SD_signals = 0;
  
  int lastEntryFound = 0;
  int c1 = 0, c2 = 0, c3 = 0, cA = 0, c4 = 0, c5 = 0;
  int cPE1 = 0, cPE2 = 0, cPE3 = 0, cPEA = 0, cPE4 = 0, cPE5 = 0;
  
  for(unsigned int n = 0; n < nEntries1; n++){
    
    t1->GetEntry(n);
    if(n != 0 && n % fivepercent == 0) 
      cout << endl << 100 * float(n)/nEntries1 << " %" ;
          
    for(unsigned int i = lastEntryFound; i < lastEntryFound+100; i++){
      
      t2->GetEntry(i);
      
      //if( SdId1 < SdId2 && primaryEnergy1 < primaryEnergy2 ) break;
      
      if( SdId2 == SdId1 && abs(log10(primaryEnergy1) - log10(primaryEnergy2)) < 0.001 ){
	
	counterFound++;
	lastEntryFound = i;
	
	if( Nparticles1 != Nparticles2) cout << "Particles difference = " << Nparticles1 - Nparticles2 << " in entries " << n << " vs " << i << endl;				
	if( coreX_stationCS1 != coreX_stationCS2) cout << "Core X difference = " << coreX_stationCS1 - coreX_stationCS2 <<  " in entries " << n << " vs " << i << endl;     
	if( coreY_stationCS1 != coreY_stationCS2) cout << "Core Y difference = " << coreY_stationCS1 - coreY_stationCS2 <<  " in entries " << n << " vs " << i << endl;	
	
	if(Nparticles1 == 0 || Nparticles1 == 10000) continue;
	
	//bool diffPart = false;
	/*
	  for(int k=0; k<Nparticles1; k++){
	  if( particlePositionX1->at(k) == particlePositionX2->at(k) &&
	  particlePositionY1->at(k) == particlePositionY2->at(k) ){
	  hDiffPartTime->Fill(particleTime1->at(k)-particleTime2->at(k));
	  }
	  //hDiffPartEnergy->Fill(100*(particleEnergy1->at(k)-particleEnergy2->at(k))/particleEnergy1->at(k));
	  //hDiffPartZenith->Fill(100*(particleZenith1->at(k)-particleZenith2->at(k))/particleZenith1->at(k));
	  //hDiffPartAzimuth->Fill(100*(particleAzimuth1->at(k)-particleAzimuth2->at(k))/particleAzimuth1->at(k));
	  hDiffPartPosX->Fill(1000*(particlePositionX1->at(k)-particlePositionX2->at(k)));
	  hDiffPartPosY->Fill(1000*(particlePositionY1->at(k)-particlePositionY2->at(k)));
	  hDiffPartPosZ->Fill(1000*(particlePositionZ1->at(k)-particlePositionZ2->at(k)));
	  hDiffPartTime->Fill(particleTime1->at(k)-particleTime2->at(k));
	  }
	*/
	
	//if(totS1<10 || totS2<10) break;
	//if(Nparticles1<1 || Nparticles1>=5000) break;
	counterSelected++;
	
	/*
	int energeticParticles = 0;
	for(int k=0; k<Nparticles1; k++){
	  if(particleEnergy1->at(k) > 0.04) energeticParticles++;
	}
	*/	
	
	double S1_1 = ((HG_LG_ratio1[0]*charge_LG1[0])/VEMcharge1[0]);	
	double S1_2 = ((HG_LG_ratio2[0]*charge_LG2[0])/VEMcharge2[0]);
	
	double S2_1 = ((HG_LG_ratio1[1]*charge_LG1[1])/VEMcharge1[1]);	
	double S2_2 = ((HG_LG_ratio2[1]*charge_LG2[1])/VEMcharge2[1]);
	
	double S3_1 = ((HG_LG_ratio1[2]*charge_LG1[2])/VEMcharge1[2]);	
	double S3_2 = ((HG_LG_ratio2[2]*charge_LG2[2])/VEMcharge2[2]);	

	if(nPE1[0]>99){
	  hPEDiff1->Fill( 100*float(nPE1[0]-nPE2[0])/nPE1[0] );
	  gPEDiff1->SetPoint(cPE1, log10(nPE1[0]), 100*float(nPE1[0]-nPE2[0])/nPE1[0] );
	  pPEDiff1->Fill(log10(nPE1[0]), 100*float(nPE1[0]-nPE2[0])/nPE1[0] );	
	  cPE1++;
	}
	if(nPE1[1]>99){
	  hPEDiff2->Fill( 100*float(nPE1[1]-nPE2[1])/nPE1[1] );
	  gPEDiff2->SetPoint(cPE2, log10(nPE1[1]), 100*float(nPE1[1]-nPE2[1])/nPE1[1] );
	  pPEDiff2->Fill(log10(nPE1[1]), 100*float(nPE1[1]-nPE2[1])/nPE1[1] );
	  cPE2++;
	}
	if(nPE1[2]>99){
	  hPEDiff3->Fill( 100*float(nPE1[2]-nPE2[2])/nPE1[2] );
	  gPEDiff3->SetPoint(cPE3, log10(nPE1[2]), 100*float(nPE1[2]-nPE2[2])/nPE1[2] );
	  pPEDiff3->Fill(log10(nPE1[2]), 100*float(nPE1[2]-nPE2[2])/nPE1[2] );
	  cPE3++;
	}
	if(nPE1[3]>9){
	  hPEDiff4->Fill( 100*float(nPE1[3]-nPE2[3])/nPE1[3] );
	  gPEDiff4->SetPoint(cPE4, log10(nPE1[3]), 100*float(nPE1[3]-nPE2[3])/nPE1[3] );
	  pPEDiff4->Fill(log10(nPE1[3]), 100*float(nPE1[3]-nPE2[3])/nPE1[3] );
	  cPE4++;	
	}

	if(charge_LG1[0]
	   //&& charge_LG2[0] 
	   //&& satBins_LG1[0]<2 && satBins_LG2[0]<2
	   ){

	  hChargeDiff1->Fill( 100*float(charge_LG1[0]-charge_LG2[0])/charge_LG1[0] );
	  gChargeDiff1->SetPoint(c1, log10(charge_LG1[0]), 100*float(charge_LG1[0]-charge_LG2[0])/charge_LG1[0] );
	  pChargeDiff1->Fill(log10(charge_LG1[0]), 100*float(charge_LG1[0]-charge_LG2[0])/charge_LG1[0] );
	  
	  hSignDiff1->Fill(100*(S1_1-S1_2)/S1_1);
	  gSignDiff1->SetPoint(c1, log10(S1_1), (S1_1-S1_2)/S1_1);
	  pSignDiff1->Fill(log10(S1_1), (S1_1-S1_2)/S1_1);
	  
	  gSignDiff1_vsCoreDist->SetPoint(c1, log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S1_1-S1_2)/S1_1);
	  pSignDiff1_vsCoreDist->Fill(log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S1_1-S1_2)/S1_1);
	  c1++;
	}
	//if(charge_LG1[0]==0 && charge_LG2[0]==0) hChargeDiff1->Fill( 0 );
				
	if(charge_LG1[1]>0
	   //&& charge_LG2[1] 
	   //&& satBins_LG1[1]<2 && satBins_LG2[1]<2
	   ){
	  hChargeDiff2->Fill( 100*float(charge_LG1[1]-charge_LG2[1])/charge_LG1[1] );
	  gChargeDiff2->SetPoint(c2, log10(charge_LG1[1]), 100*float(charge_LG1[1]-charge_LG2[1])/charge_LG1[1] );
	  pChargeDiff2->Fill(log10(charge_LG1[1]), 100*float(charge_LG1[1]-charge_LG2[1])/charge_LG1[1] );
	  
	  hSignDiff2->Fill(100*(S2_1-S2_2)/S2_1);
	  gSignDiff2->SetPoint(c2, log10(S2_1), (S2_1-S2_2)/S2_1);
	  pSignDiff2->Fill(log10(S2_1), (S2_1-S2_2)/S2_1);
	  
	  gSignDiff2_vsCoreDist->SetPoint(c2, log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S2_1-S2_2)/S2_1);
	  pSignDiff2_vsCoreDist->Fill(log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S2_1-S2_2)/S2_1);
	  c2++;
	}
	//if(charge_LG1[1]==0 && charge_LG2[1]==0) hChargeDiff2->Fill( 0 );
	
	if(charge_LG1[2]>0
	   //&& charge_LG2[2] 
	   //&& satBins_LG1[2]<2 && satBins_LG2[2]<2
	   ){
	  hChargeDiff3->Fill( 100*float(charge_LG1[2]-charge_LG2[2])/charge_LG1[2] );
	  gChargeDiff3->SetPoint(c3, log10(charge_LG1[2]), 100*float(charge_LG1[2]-charge_LG2[2])/charge_LG1[2] );
	  pChargeDiff3->Fill(log10(charge_LG1[2]), 100*float(charge_LG1[2]-charge_LG2[2])/charge_LG1[2] );
	  
	  hSignDiff3->Fill(100*(S3_1-S3_2)/S3_1);
	  gSignDiff3->SetPoint(c3, log10(S3_1), (S3_1-S3_2)/S3_1);
	  pSignDiff3->Fill(log10(S3_1), (S3_1-S3_2)/S3_1);
	  
	  gSignDiff3_vsCoreDist->SetPoint(c3, log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S3_1-S3_2)/S3_1);
	  pSignDiff3_vsCoreDist->Fill(log10(sqrt(coreX_stationCS1*coreX_stationCS1+coreY_stationCS1*coreY_stationCS1)), (S3_1-S3_2)/S3_1);
	  c3++;
	}
	//if(charge_LG1[2]==0 && charge_LG2[2]==0) hChargeDiff3->Fill( 0 );
	
	if(charge_LG1[3]>0
	   //&& charge_LG2[3]
	   ){
	  hChargeDiff4->Fill( 100*float(charge_LG1[3]-charge_LG2[3])/charge_LG1[3] );
	  gChargeDiff4->SetPoint(c4, log10(charge_LG1[3]), 100*float(charge_LG1[3]-charge_LG2[3])/charge_LG1[3] );
	  pChargeDiff4->Fill(log10(charge_LG1[3]), 100*float(charge_LG1[3]-charge_LG2[3])/charge_LG1[3] );
	  c4++;
	}
	//if(charge_LG1[3]==0 && charge_LG2[3]==0) hChargeDiff4->Fill( 0 );
	
	if(charge_LG1[4]>0
	   //&& charge_LG2[4]
	   ){
	  hChargeDiff5->Fill( 100*float(charge_LG1[4]-charge_LG2[4])/charge_LG1[4] );
	  gChargeDiff5->SetPoint(c5, log10(charge_LG1[4]), 100*float(charge_LG1[4]-charge_LG2[4])/charge_LG1[4] );
	  c5++;
	}
	//if(charge_LG1[4]==0 && charge_LG2[4]==0) hChargeDiff5->Fill( 0 );			

	/*
	bool different_signal = false;			
	
	if(charge_LG1[0] != charge_LG2[0]){
	  different_signal = true;
	  if(charge_LG1[0] && !charge_LG2[0])
	    cout << "\n Event " << SdId1 << " (n1=" << n << ", n2=" << i << ")"
		 << "E = " << primaryEnergy1 << " GeV, "
		 << "theta = " << primaryZenith1
							 << ", phi = " << primaryAzimuth1
		 << ", N part = " << energeticParticles
		 << " --> ch1 = " << charge_LG1[0] << " / ch2 = " << charge_LG2[0] << endl;	
	}
	
	if(charge_LG1[1] != charge_LG2[1]){
	  different_signal = true;
	  if(charge_LG1[1] && !charge_LG2[1])
	    cout << "Event " << SdId1 << " (n1=" << n << ", n2=" << i << ")"
		 << "E = " << primaryEnergy1 << " GeV, "
		 << "theta = " << primaryZenith1
		 << ", phi = " << primaryAzimuth1
		 << ", N part = " << energeticParticles
		 << " --> ch1 = " << charge_LG1[1] << " / ch2 = " << charge_LG2[1] << endl;					
	}
	
	if(charge_LG1[2] != charge_LG2[2]){
	  different_signal = true;
	  if(charge_LG1[2] && !charge_LG2[2])
	    cout << "Event " << SdId1 << " (n1=" << n << ", n2=" << i << ")"
		 << "E = " << primaryEnergy1 << " GeV, "
		 << "theta = " << primaryZenith1
		 << ", phi = " << primaryAzimuth1
		 << ", N part = " << energeticParticles
		 << " --> ch1 = " << charge_LG1[2] << " / ch2 = " << charge_LG2[2] << endl;
	}
	
	if(different_signal) event_with_different_SD_signals++;
	*/			
	break;
      }
      
    } //close nEntries2
  } //close nEntries1
  
	
  cout << endl << "Found " << counterFound << " over " << nEntries1 << " events" << endl;
  cout << endl << "Selected " << counterSelected << " over " << counterFound << " events" << endl;
  //cout << endl << "Event with different SD signals " << event_with_different_SD_signals << "/" << counterSelected << endl;

  /*
	cout<<endl;
	for(unsigned int i=1;i<=pSignDiffA->GetNbinsX();i++)
		cout << "[" << pSignDiffA->GetBinCenter(i)-(pSignDiffA->GetBinWidth(i)/2.)
			 << ", " << pSignDiffA->GetBinCenter(i)+(pSignDiffA->GetBinWidth(i)/2.) << "]"
			 << "\t" << pSignDiffA->GetBinEntries(i)
			 << "\t" << 100*pSignDiffA->GetBinContent(i) 
			 << "\t" << 100*pSignDiffA->GetBinError(i)*sqrt(pSignDiffA->GetBinEntries(i)) << "\n";
	
	cout<<endl;		 
	for(unsigned int i=1;i<=pSignDiffA_vsNpart->GetNbinsX();i++)
		cout << pSignDiffA_vsNpart->GetBinCenter(i) << "\t" << pSignDiffA_vsNpart->GetBinEntries(i) << "\t"
			 << pSignDiffA_vsNpart->GetBinContent(i) << "\t" << pSignDiffA_vsNpart->GetBinError(i) << "\n";
			 
	cout<<endl;		 
	for(unsigned int i=1;i<=pSignDiffA_vsCoreDist->GetNbinsX();i++)
		cout << "[" << pSignDiffA_vsCoreDist->GetBinCenter(i)-(pSignDiffA_vsCoreDist->GetBinWidth(i)/2.)
			 << ", " << pSignDiffA_vsCoreDist->GetBinCenter(i)+(pSignDiffA_vsCoreDist->GetBinWidth(i)/2.) << "]"
			 << "\t" << pSignDiffA_vsCoreDist->GetBinEntries(i)
			 << "\t" << 100*pSignDiffA_vsCoreDist->GetBinContent(i) 
			 << "\t" << 100*pSignDiffA_vsCoreDist->GetBinError(i)*sqrt(pSignDiffA_vsCoreDist->GetBinEntries(i)) << "\n";
  */

//////////////////////////////////////////////////////////////////////////////////////////		
// Graphical part

	//gStyle->SetOptStat(2211);
	gStyle->SetOptStat("nemruo");
	gStyle->SetStatW(0.16); gStyle->SetStatH(0.06);
	gStyle->SetTitleAlign(23);
	
	TPaveStats *st1 = NULL; TPaveStats *st2 = NULL;
	TPaveStats *st3 = NULL; TPaveStats *st4 = NULL;
	
	Float_t maxValue;
	TLegend* leg = new TLegend(0.625, 0.475, .95, .6);
	
	TLine* lineZero = new TLine(1.8,0,7.2,0);
	lineZero->SetLineWidth(2); lineZero->SetLineColor(kGreen+1);// lineZero->SetLineStyle(8);

	TLine* lineZeroSPMT = new TLine(0.8,0,5.2,0);
	lineZeroSPMT->SetLineWidth(2); lineZeroSPMT->SetLineColor(kGreen+1);// lineZeroSPMT->SetLineStyle(8);
	
	TLine* line_plus1 = new TLine(1.8,1,7.2,1);
	line_plus1->SetLineWidth(2); line_plus1->SetLineColor(kBlue); line_plus1->SetLineStyle(8); 	
	TLine* line_minus1 = new TLine(1.8,-1,7.2,-1);
	line_minus1->SetLineWidth(2); line_minus1->SetLineColor(kBlue); line_minus1->SetLineStyle(8); 
	
	TLine* line_plus1_SPMT = new TLine(0.8,1,5.3,1);
	line_plus1_SPMT->SetLineWidth(2); line_plus1_SPMT->SetLineColor(kBlue); line_plus1_SPMT->SetLineStyle(8); 	
	TLine* line_minus1_SPMT = new TLine(0.8,-1,5.3,-1);
	line_minus1_SPMT->SetLineWidth(2); line_minus1_SPMT->SetLineColor(kBlue); line_minus1_SPMT->SetLineStyle(8); 
	
	bool zoom = true;
	char labelTitle[100];

//////////

	TCanvas *cPEDiff = new TCanvas("cPEDiff","cPEDiff",1200,750);
	cPEDiff->Divide(2,2);
	
	cPEDiff->cd(1);
	//cPEDiff->cd(1)->SetLogy();
	hPEDiff1->SetLineWidth(2); 
	hPEDiff1->SetTitle("Difference in integrated number of photoelectrons - LPMT1");
	sprintf (labelTitle, "(p.e._{%s} - p.e._{%s}) / p.e._{%s} [%%]", name1, name2, name1);
	hPEDiff1->GetXaxis()->SetTitle(labelTitle); hPEDiff1->GetXaxis()->SetTitleSize(0.045); hPEDiff1->GetXaxis()->SetTitleOffset(0.95);
	hPEDiff1->Draw("hist");	
	gPad->Update();
    st1 = (TPaveStats*)hPEDiff1->FindObject("stats");
            st1->SetX1NDC(0.625); st1->SetX2NDC(0.925);
            st1->SetY1NDC(0.9); st1->SetY2NDC(0.6);
	gPad->Update();
	
	cPEDiff->cd(2);
	//cPEDiff->cd(2)->SetLogy();
	hPEDiff2->SetLineWidth(2); 
	hPEDiff2->SetTitle("Difference in integrated number of photoelectrons - LPMT2");
	hPEDiff2->GetXaxis()->SetTitle(labelTitle); hPEDiff2->GetXaxis()->SetTitleSize(0.045); hPEDiff2->GetXaxis()->SetTitleOffset(0.95);
	hPEDiff2->Draw("hist");	
	gPad->Update();
    st1 = (TPaveStats*)hPEDiff2->FindObject("stats");
            st1->SetX1NDC(0.625); st1->SetX2NDC(0.925);
            st1->SetY1NDC(0.9); st1->SetY2NDC(0.6);
	gPad->Update();
	
	cPEDiff->cd(3);
	//cPEDiff->cd(3)->SetLogy();
	hPEDiff3->SetLineWidth(2); 
	hPEDiff3->SetTitle("Difference in integrated number of photoelectrons - LPMT3");
	hPEDiff3->GetXaxis()->SetTitle(labelTitle); hPEDiff3->GetXaxis()->SetTitleSize(0.045); hPEDiff3->GetXaxis()->SetTitleOffset(0.95);
	hPEDiff3->Draw("hist");
	gPad->Update();	
    st1 = (TPaveStats*)hPEDiff3->FindObject("stats");
            st1->SetX1NDC(0.625); st1->SetX2NDC(0.925);
            st1->SetY1NDC(0.9); st1->SetY2NDC(0.6);
	gPad->Update();

	cPEDiff->cd(4);
	//cPEDiff->cd(4)->SetLogy();
	hPEDiff4->SetLineWidth(2); 
	hPEDiff4->SetTitle("Difference in integrated number of photoelectrons - SPMT");
	hPEDiff4->GetXaxis()->SetTitle(labelTitle); hPEDiff4->GetXaxis()->SetTitleSize(0.045); hPEDiff4->GetXaxis()->SetTitleOffset(0.95);
	hPEDiff4->Draw("hist");	
	gPad->Update();
    st1 = (TPaveStats*)hPEDiff4->FindObject("stats");
            st1->SetX1NDC(0.625); st1->SetX2NDC(0.925);
            st1->SetY1NDC(0.9); st1->SetY2NDC(0.6);
	gPad->Update();

	//cPEDiff->Close();

//////////

	TCanvas *cPEDiff2 = new TCanvas("cPEDiff2","cPEDiff2",1200,750);
	cPEDiff2->Divide(2,2);
	
	cPEDiff2->cd(1);
	gPEDiff1->SetTitle("Difference in integrated number of photoelectrons - LPMT1");
	sprintf (labelTitle, "(log_{10}(p.e._{%s})", name1);
	gPEDiff1->GetXaxis()->SetTitle(labelTitle); gPEDiff1->GetXaxis()->SetTitleSize(0.045); gPEDiff1->GetXaxis()->SetTitleOffset(1.05);
	sprintf (labelTitle, "(p.e._{%s} - p.e._{%s}) / p.e._{%s} [%%]", name1, name2, name1);
	gPEDiff1->GetYaxis()->SetTitle(labelTitle); gPEDiff1->GetYaxis()->SetTitleSize(0.05); gPEDiff1->GetYaxis()->SetTitleOffset(0.85);
	gPEDiff1->SetMarkerStyle(6); gPEDiff1->SetMarkerColor(16); 
	gPEDiff1->Draw("ap");
	if(zoom){
		lineZero->Draw("lsame");
		line_plus1->Draw("lsame");
		line_minus1->Draw("lsame");
	}
	pPEDiff1->Draw("psame");
	if(zoom) gPEDiff1->GetYaxis()->SetRangeUser(-9,9);
	else gPEDiff1->GetYaxis()->SetRangeUser(-169,119);
	gPad->Update();
	
	cPEDiff2->cd(2);
	gPEDiff2->SetTitle("Difference in integrated number of photoelectrons - LPMT2");
	sprintf (labelTitle, "(log_{10}(p.e._{%s})", name1);
	gPEDiff2->GetXaxis()->SetTitle(labelTitle); gPEDiff2->GetXaxis()->SetTitleSize(0.045); gPEDiff2->GetXaxis()->SetTitleOffset(1.05);
	sprintf (labelTitle, "(p.e._{%s} - p.e._{%s}) / p.e._{%s} [%%]", name1, name2, name1);
	gPEDiff2->GetYaxis()->SetTitle(labelTitle); gPEDiff2->GetYaxis()->SetTitleSize(0.05); gPEDiff2->GetYaxis()->SetTitleOffset(0.85);
	gPEDiff2->SetMarkerStyle(6); gPEDiff2->SetMarkerColor(16); 
	gPEDiff2->Draw("ap");
	if(zoom){
		lineZero->Draw("lsame");
		line_plus1->Draw("lsame");
		line_minus1->Draw("lsame");
	}
	pPEDiff2->Draw("psame");
	if(zoom) gPEDiff2->GetYaxis()->SetRangeUser(-9,9);
	else gPEDiff2->GetYaxis()->SetRangeUser(-169,119);
	gPad->Update();
	
	cPEDiff2->cd(3);
	gPEDiff3->SetTitle("Difference in integrated number of photoelectrons - LPMT3");
	sprintf (labelTitle, "(log_{10}(p.e._{%s})", name1);
	gPEDiff3->GetXaxis()->SetTitle(labelTitle); gPEDiff3->GetXaxis()->SetTitleSize(0.045); gPEDiff3->GetXaxis()->SetTitleOffset(1.05);
	sprintf (labelTitle, "(p.e._{%s} - p.e._{%s}) / p.e._{%s} [%%]", name1, name2, name1);
	gPEDiff3->GetYaxis()->SetTitle(labelTitle); gPEDiff3->GetYaxis()->SetTitleSize(0.05); gPEDiff3->GetYaxis()->SetTitleOffset(0.85);
	gPEDiff3->SetMarkerStyle(6); gPEDiff3->SetMarkerColor(16); 
	gPEDiff3->Draw("ap");
	if(zoom){
		lineZero->Draw("lsame");
		line_plus1->Draw("lsame");
		line_minus1->Draw("lsame");
	}
	pPEDiff3->Draw("psame");
	if(zoom) gPEDiff3->GetYaxis()->SetRangeUser(-9,9);
	else gPEDiff3->GetYaxis()->SetRangeUser(-169,119);
	gPad->Update();

	cPEDiff2->cd(4);
	gPEDiff4->SetTitle("Difference in integrated number of photoelectrons - SPMT");
	sprintf (labelTitle, "(log_{10}(p.e._{%s})", name1);
	gPEDiff4->GetXaxis()->SetTitle(labelTitle); gPEDiff4->GetXaxis()->SetTitleSize(0.045); gPEDiff4->GetXaxis()->SetTitleOffset(1.05);
	sprintf (labelTitle, "(p.e._{%s} - p.e._{%s}) / p.e._{%s} [%%]", name1, name2, name1);
	gPEDiff4->GetYaxis()->SetTitle(labelTitle); gPEDiff4->GetYaxis()->SetTitleSize(0.05); gPEDiff4->GetYaxis()->SetTitleOffset(0.85);
	gPEDiff4->SetMarkerStyle(6); gPEDiff4->SetMarkerColor(16); 
	gPEDiff4->Draw("ap");
	if(zoom){
		lineZeroSPMT->Draw("lsame");
		line_plus1_SPMT->Draw("lsame");
		line_minus1_SPMT->Draw("lsame");
	}
	pPEDiff4->Draw("psame");
	if(zoom) gPEDiff4->GetYaxis()->SetRangeUser(-9,9);
	else gPEDiff4->GetYaxis()->SetRangeUser(-169,119);
	gPad->Update();

	//cPEDiff2->Close();


//////////
/*
	TCanvas *cChargeDiff = new TCanvas("cChargeDiff","cChargeDiff",1200,800);
	cChargeDiff->Divide(2,2);
	
	cChargeDiff->cd(1);
	cChargeDiff->cd(1)->SetLogy();
	hChargeDiff1->SetLineWidth(2); 
	hChargeDiff1->SetTitle("Charge differences LPMT1 [%]");
	hChargeDiff1->GetXaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast}");
	hChargeDiff1->Draw("hist");	
	gPad->Update();
	
	cChargeDiff->cd(2);
	cChargeDiff->cd(2)->SetLogy();
	hChargeDiff2->SetLineWidth(2); 
	hChargeDiff2->SetTitle("Charge differences LPMT2 [%]");
	hChargeDiff2->GetXaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast}");
	hChargeDiff2->Draw("hist");	
	gPad->Update();
	
	cChargeDiff->cd(3);
	cChargeDiff->cd(3)->SetLogy();
	hChargeDiff3->SetLineWidth(2); 
	hChargeDiff3->SetTitle("Charge differences LPMT3 [%]");
	hChargeDiff3->GetXaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast}");
	hChargeDiff3->Draw("hist");	
	gPad->Update();

	cChargeDiff->cd(4);
	cChargeDiff->cd(4)->SetLogy();
	hChargeDiff4->SetLineWidth(2); 
	hChargeDiff4->SetTitle("Charge differences SPMT");
	hChargeDiff4->GetXaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast}");
	hChargeDiff4->Draw("hist");	
	gPad->Update();

	//cChargeDiff->Close();
*/
//////////
/*
	TCanvas *cChargeDiff2 = new TCanvas("cChargeDiff2","cChargeDiff2",1200,800);
	cChargeDiff2->Divide(2,2);
	
	cChargeDiff2->cd(1);
	gChargeDiff1->SetTitle("Charge differences LPMT1");
	gChargeDiff1->GetXaxis()->SetTitle("log_{10}(charge/FADC counts)");
	gChargeDiff1->GetYaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast} [%]");
	gChargeDiff1->SetMarkerStyle(6); gChargeDiff1->SetMarkerColor(16); 
	gChargeDiff1->Draw("ap");
	pChargeDiff1->Draw("psame");
	gChargeDiff1->GetYaxis()->SetRangeUser(-99,99);
	gPad->Update();
	
	cChargeDiff2->cd(2);
	gChargeDiff2->SetTitle("Charge differences LPMT2");
	gChargeDiff2->GetXaxis()->SetTitle("log_{10}(charge/FADC counts)");
	gChargeDiff2->GetYaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast} [%]");
	gChargeDiff2->SetMarkerStyle(6); gChargeDiff2->SetMarkerColor(16); 
	gChargeDiff2->Draw("ap");
	pChargeDiff2->Draw("psame");
	gChargeDiff2->GetYaxis()->SetRangeUser(-99,99);
	gPad->Update();
	
	cChargeDiff2->cd(3);
	gChargeDiff3->SetTitle("Charge differences LPMT3");
	gChargeDiff3->GetXaxis()->SetTitle("log_{10}(charge/FADC counts)");
	gChargeDiff3->GetYaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast} [%]");
	gChargeDiff3->SetMarkerStyle(6); gChargeDiff3->SetMarkerColor(16); 
	gChargeDiff3->Draw("ap");
	pChargeDiff3->Draw("psame");
	gChargeDiff3->GetYaxis()->SetRangeUser(-99,99);
	gPad->Update();

	cChargeDiff2->cd(4);
	gChargeDiff4->SetTitle("Charge differences SPMT");
	gChargeDiff4->GetXaxis()->SetTitle("log_{10}(charge/FADC counts)");
	gChargeDiff4->GetYaxis()->SetTitle("(ch_{fast} - ch_{full})/ch_{fast} [%]");
	gChargeDiff4->SetMarkerStyle(6); gChargeDiff4->SetMarkerColor(16); 
	gChargeDiff4->Draw("ap");
	pChargeDiff4->Draw("psame");
	gChargeDiff4->GetYaxis()->SetRangeUser(-99,99);
	gPad->Update();

	//cChargeDiff2->Close();
*/
//////////

	TLine* lineSign = new TLine(0.1,0,3.3,0);
	lineSign->SetLineWidth(2); lineSign->SetLineColor(kGreen+1); 
	
	TLine* linePart = new TLine(1.5,0,3.9,0);
	linePart->SetLineWidth(2); linePart->SetLineColor(kGreen+1); 
	
	TLine* lineCore = new TLine(0.4,0,2.1,0);
	lineCore->SetLineWidth(2); lineCore->SetLineColor(kGreen+1); 

//////////

	TCanvas *cSignDiff = new TCanvas("cSignDiff","cSignDiff",1200,800);
	cSignDiff->Divide(2,2);
	
	cSignDiff->cd(1);
	cSignDiff->cd(1)->SetLogy();
	hSignDiff1->SetLineWidth(2); 
	hSignDiff1->SetTitle("Signal differences LPMT1 [%]");
	hSignDiff1->GetXaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	hSignDiff1->Draw("hist");	
	gPad->Update();
	
	cSignDiff->cd(2);
	cSignDiff->cd(2)->SetLogy();
	hSignDiff2->SetLineWidth(2); 
	hSignDiff2->SetTitle("Signal differences LPMT2 [%]");
	hSignDiff2->GetXaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	hSignDiff2->Draw("hist");	
	gPad->Update();
	
	cSignDiff->cd(3);
	cSignDiff->cd(3)->SetLogy();
	hSignDiff3->SetLineWidth(2); 
	hSignDiff3->SetTitle("Signal differences LPMT3 [%]");
	hSignDiff3->GetXaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	hSignDiff3->Draw("hist");	
	gPad->Update();
	
	cSignDiff->Close();

//////////

	TCanvas *cSignDiff2 = new TCanvas("cSignDiff2","cSignDiff2",1200,800);
	cSignDiff2->Divide(2,2);
	
	cSignDiff2->cd(1);
	cSignDiff2->cd(1)->SetGrid();
	gSignDiff1->SetTitle("Signal differences LPMT1");
	gSignDiff1->GetXaxis()->SetTitle("log_{10}(S1/VEM)");
	gSignDiff1->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff1->SetMarkerStyle(6); gSignDiff1->SetMarkerColor(16); 
	gSignDiff1->Draw("ap");
	lineSign->Draw("same");
	pSignDiff1->Draw("same");
	gSignDiff1->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();
	
	cSignDiff2->cd(2);
	cSignDiff2->cd(2)->SetGrid();
	gSignDiff2->SetTitle("Signal differences LPMT2");
	gSignDiff2->GetXaxis()->SetTitle("log_{10}(S1/VEM)");
	gSignDiff2->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff2->SetMarkerStyle(6); gSignDiff2->SetMarkerColor(16); 
	gSignDiff2->Draw("ap");
	lineSign->Draw("same");
	pSignDiff2->Draw("same");
	gSignDiff2->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();
	
	cSignDiff2->cd(3);
	cSignDiff2->cd(3)->SetGrid();
	gSignDiff3->SetTitle("Signal differences LPMT3");
	gSignDiff3->GetXaxis()->SetTitle("log_{10}(S3/VEM)");
	gSignDiff3->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff3->SetMarkerStyle(6); gSignDiff3->SetMarkerColor(16); 
	gSignDiff3->Draw("ap");
	lineSign->Draw("same");
	pSignDiff3->Draw("same");
	gSignDiff3->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();
	
	cSignDiff2->Close();

//////////

/*
	TCanvas *cSignDiff_vsCoreDist = new TCanvas("cSignDiff_vsCoreDist","cSignDiff_vsCoreDist",1200,800);
	cSignDiff_vsCoreDist->Divide(2,2);
	
	cSignDiff_vsCoreDist->cd(1);
	cSignDiff_vsCoreDist->cd(1)->SetGrid();
	gSignDiff1_vsCoreDist->SetTitle("Signal differences LPMT1");
	gSignDiff1_vsCoreDist->GetXaxis()->SetTitle("log_{10}(CoreDistance)");
	gSignDiff1_vsCoreDist->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff1_vsCoreDist->SetMarkerStyle(6); gSignDiff1_vsCoreDist->SetMarkerColor(16); 
	gSignDiff1_vsCoreDist->Draw("ap");
	lineCore->Draw("same");
	pSignDiff1_vsCoreDist->Draw("same");
	gSignDiff1_vsCoreDist->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();
	
	cSignDiff_vsCoreDist->cd(2);
	cSignDiff_vsCoreDist->cd(2)->SetGrid();
	gSignDiff2_vsCoreDist->SetTitle("Signal differences LPMT2");
	gSignDiff2_vsCoreDist->GetXaxis()->SetTitle("log_{10}(CoreDistance)");
	gSignDiff2_vsCoreDist->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff2_vsCoreDist->SetMarkerStyle(6); gSignDiff2_vsCoreDist->SetMarkerColor(16); 
	gSignDiff2_vsCoreDist->Draw("ap");
	lineCore->Draw("same");
	pSignDiff2_vsCoreDist->Draw("same");
	gSignDiff2_vsCoreDist->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();
	
	cSignDiff_vsCoreDist->cd(3);
	cSignDiff_vsCoreDist->cd(3)->SetGrid();
	gSignDiff3_vsCoreDist->SetTitle("Signal differences LPMT3");
	gSignDiff3_vsCoreDist->GetXaxis()->SetTitle("log_{10}(CoreDistance)");
	gSignDiff3_vsCoreDist->GetYaxis()->SetTitle("(S_{fast}-S_{full})/S_{full}");
	gSignDiff3_vsCoreDist->SetMarkerStyle(6); gSignDiff3_vsCoreDist->SetMarkerColor(16); 
	gSignDiff3_vsCoreDist->Draw("ap");
	lineCore->Draw("same");
	pSignDiff3_vsCoreDist->Draw("same");
	gSignDiff3_vsCoreDist->GetYaxis()->SetRangeUser(-1,1);
	gPad->Update();

	cSignDiff_vsCoreDist->Close();
*/	
	
//////////
/*
	TLine* lineSign_plus1 = new TLine(0.1,0.01,3.3,0.01);
	lineSign_plus1->SetLineWidth(2); lineSign_plus1->SetLineColor(kRed); lineSign_plus1->SetLineStyle(8);
	TLine* lineSign_minus1 = new TLine(0.1,-0.01,3.3,-0.01);
	lineSign_minus1->SetLineWidth(2); lineSign_minus1->SetLineColor(kRed); lineSign_minus1->SetLineStyle(8);
	
	TLine* linePart_plus1 = new TLine(1.5,0.01,3.9,0.01);
	linePart_plus1->SetLineWidth(2); linePart_plus1->SetLineColor(kRed); linePart_plus1->SetLineStyle(8); 	
	TLine* linePart_minus1 = new TLine(1.5,-0.01,3.9,-0.01);
	linePart_minus1->SetLineWidth(2); linePart_minus1->SetLineColor(kRed); linePart_minus1->SetLineStyle(8); 
	
	TLine* lineCore_plus1 = new TLine(0.4,0.01,2.1,0.01);
	lineCore_plus1->SetLineWidth(2); lineCore_plus1->SetLineColor(kRed); lineCore_plus1->SetLineStyle(8); 	
	TLine* lineCore_minus1 = new TLine(0.4,-0.01,2.1,-0.01);
	lineCore_minus1->SetLineWidth(2); lineCore_minus1->SetLineColor(kRed); lineCore_minus1->SetLineStyle(8); 


	TCanvas *cParticlesDiff = new TCanvas("cParticlesDiff","cParticlesDiff",1100,750);
	cParticlesDiff->Divide(2,2);
	
	cParticlesDiff->cd(1);
	cParticlesDiff->cd(1)->SetLogy();
	hDiffPartPosX->SetLineWidth(2); 
	hDiffPartPosX->SetTitle("Particle position x differences");
	hDiffPartPosX->GetXaxis()->SetTitle("(x1 - x2) [mm]");
	hDiffPartPosX->Draw("hist");	
	gPad->Update();
	
	cParticlesDiff->cd(2);
	cParticlesDiff->cd(2)->SetLogy();
	hDiffPartPosY->SetLineWidth(2); 
	hDiffPartPosY->SetTitle("Particle position y differences");
	hDiffPartPosY->GetXaxis()->SetTitle("(y1 - y2) [mm]");
	hDiffPartPosY->Draw("hist");	
	gPad->Update();
	
	cParticlesDiff->cd(3);
	cParticlesDiff->cd(3)->SetLogy();
	hDiffPartPosZ->SetLineWidth(2); 
	hDiffPartPosZ->SetTitle("Particle position z differences");
	hDiffPartPosZ->GetXaxis()->SetTitle("(z1 - z2) [mm]");
	hDiffPartPosZ->Draw("hist");	
	gPad->Update();
	
	cParticlesDiff->cd(4);
	cParticlesDiff->cd(4)->SetLogy();
	hDiffPartTime->SetLineWidth(2); 
	hDiffPartTime->SetTitle("Particle time differences");
	hDiffPartTime->GetXaxis()->SetTitle("(t1 - t2) [ns]");
	hDiffPartTime->Draw("hist");	
	gPad->Update();

	cParticlesDiff->cd(5);
	cParticlesDiff->cd(5)->SetLogy();
	hDiffPartEnergy->SetLineWidth(2); 
	hDiffPartEnergy->SetTitle("Particle energy differences");
	hDiffPartEnergy->GetXaxis()->SetTitle("(e1 - e2)/e1");
	hDiffPartEnergy->Draw("hist");	
	gPad->Update();
	
	cParticlesDiff->cd(6);
	cParticlesDiff->cd(6)->SetLogy();
	hDiffPartZenith->SetLineWidth(2); 
	hDiffPartZenith->SetTitle("Particle zenith differences");
	hDiffPartZenith->GetXaxis()->SetTitle("(theta1 - theta2)/theta1");
	hDiffPartZenith->Draw("hist");	
	gPad->Update();
	
	cParticlesDiff->cd(7);
	cParticlesDiff->cd(7)->SetLogy();
	hDiffPartAzimuth->SetLineWidth(2); 
	hDiffPartAzimuth->SetTitle("Particle azimuth differences");
	hDiffPartAzimuth->GetXaxis()->SetTitle("(phi1 - phi2)/phi1");
	hDiffPartAzimuth->Draw("hist");	
	gPad->Update();	
	
	//cParticlesDiff->Close();	
*/

}
