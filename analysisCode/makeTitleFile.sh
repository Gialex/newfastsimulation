#!/bin/sh

SUB=/storage/gpfs_data/auger/ganastasi/SmallShowersSimulation/newFastSimulation/CachedDirectInjector_standard/results/proton_14.5_15.0_0m_fast

for ((i=0; i<500; i++));
do

    if [ $i -lt 10 ]
    then
	echo "$SUB/smallShowers000$i.root" >> rootTitlesFile_standard_fast.txt
    elif [ $i -lt 100 ]
    then
        echo "$SUB/smallShowers00$i.root" >> rootTitlesFile_standard_fast.txt
    elif [ $i -lt 1000 ]
    then
        echo "$SUB/smallShowers0$i.root" >> rootTitlesFile_standard_fast.txt
    else
        echo "$SUB/smallShowers$i.root" >> rootTitlesFile_standard_fast.txt
    fi

done
