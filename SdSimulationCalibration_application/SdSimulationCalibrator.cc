// $Id: SdSimulationCalibrator.cc 33232 2020-03-04 17:46:51Z darko $
#include <sstream>

#include "SdSimulationCalibrator.h"

#include <det/Detector.h>

#include <sdet/SDetector.h>
#include <sdet/Station.h>
#include <sdet/PMTConstants.h>

#include <evt/Event.h>

#include <fwk/CentralConfig.h>

#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sevt/StationSimData.h>
#include <sevt/StationCalibData.h>
#include <sevt/PMTCalibData.h>
#include <sevt/PMTSimData.h>
#include <sevt/PMT.h>

#include <utl/AugerUnits.h>
#include <utl/MathConstants.h>
#include <utl/Particle.h>
#include <utl/PhysicalConstants.h>
#include <utl/TimeStamp.h>
#include <utl/ErrorLogger.h>
#include <utl/Point.h>
#include <utl/Vector.h>
#include <utl/Reader.h>
#include <utl/TimeDistribution.h>
#include <utl/TimeDistributionAlgorithm.h>
#include <utl/TabularStream.h>

#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>

#include <fwk/RunController.h>

using namespace utl;
using namespace sevt;
using namespace fwk;
using namespace evt;
using namespace det;
using namespace std;


namespace SdSimulationCalibratorOG {

  VModule::ResultFlag
  SdSimulationCalibrator::Init()
  {
    INFO(".");

    const Branch simCalibBranch = CentralConfig::GetInstance()->GetTopBranch("SdSimulationCalibrator");

    simCalibBranch.GetChild("singleTankId").GetData(fSingleTankId);

    fAccPeakCharge.resize(Detector::GetInstance().GetSDetector().GetStation(fSingleTankId).GetNPMTs());

    const string hardwareType = simCalibBranch.GetChild("hardware").Get<string>();

    if (hardwareType == "wcdLarge")
      fHardwareType = sdet::PMTConstants::eWaterCherenkovLarge;
    else if (hardwareType == "wcdSmall")
      fHardwareType = sdet::PMTConstants::eWaterCherenkovSmall;
    else if (hardwareType == "ssd")
      fHardwareType = sdet::PMTConstants::eScintillator;
    else if (hardwareType == "all")
      fHardwareType = sdet::PMTConstants::eAnyType;
    else {
      ERROR("Hardware type not set in xml or not yet implemented in SdSimulationCalibrator");
      return eFailure;
    }

    fROOTtitle = simCalibBranch.GetChild("titleOutputROOT").Get<string>();

    TFile* f = new TFile(fROOTtitle.c_str(), "RECREATE");
    TTree* t = new TTree("simCalibData", "Simulated calibration data");

    UInt_t Nparticles;
    t->Branch("Nparticles", &Nparticles);

    vector<Int_t> particleType;
    t->Branch("particleType", &particleType);
    vector<Double_t> particleEnergy;
    t->Branch("particleEnergy", &particleEnergy);
    vector<Double_t> particlePositionX;
    t->Branch("particlePositionX", &particlePositionX);
    vector<Double_t> particlePositionY;
    t->Branch("particlePositionY", &particlePositionY);
    vector<Double_t> particlePositionZ;
    t->Branch("particlePositionZ", &particlePositionZ);
    vector<Double_t> particleZenith;
    t->Branch("particleZenith", &particleZenith);
    vector<Double_t> particleAzimuth;
    t->Branch("particleAzimuth", &particleAzimuth);
    vector<Double_t> particleTime;
    t->Branch("particleTime", &particleTime);

    Int_t PE[5] = { 0 };
    t->Branch("PE", &PE,"PE[5]/I");
    Double_t peak[5] = { 0 };
    t->Branch("peak", &peak,"peak[5]/D");
    Double_t charge[5] = { 0 };
    t->Branch("charge", &charge,"charge[5]/D");
    UInt_t satBins[5] = { 0 };
    t->Branch("saturatedBins", &satBins,"saturatedBins[5]/i");

    Int_t startBin[5] = { 0 };
    t->Branch("startBin", &startBin[5],"startBin[5]/I");
    Int_t stopBin[5] = { 0 };
    t->Branch("stopBin", &stopBin[5],"stopBin[5]/I");
    vector<UInt_t> trace_LPMT1;
    t->Branch("trace_LPMT1", &trace_LPMT1);
    vector<UInt_t> trace_LPMT2;
    t->Branch("trace_LPMT2", &trace_LPMT2);
    vector<UInt_t> trace_LPMT3;
    t->Branch("trace_LPMT3", &trace_LPMT3);
    vector<UInt_t> trace_SPMT;
    t->Branch("trace_SPMT", &trace_SPMT);
    vector<UInt_t> trace_SSD;
    t->Branch("trace_SSD", &trace_SSD);

    t->Write();
    f->Close();
    
    return eSuccess;
  }


  VModule::ResultFlag
  SdSimulationCalibrator::Run(Event& event)
  {
    if (!event.HasSEvent()) {
      ERROR("Non-existent SEvent.");
      return eFailure;
    }

    SEvent& sEvent = event.GetSEvent();

    if (!sEvent.HasStation(fSingleTankId)) {
      ostringstream err;
      err << "Non-existent station " << fSingleTankId;
      ERROR(err);
      return eFailure;
    }
    fSignature = sEvent.GetStation(fSingleTankId).GetSimData().GetSimulatorSignature();
    fIsUUB = Detector::GetInstance().GetSDetector().GetStation(fSingleTankId).IsUUB();

    CalculateCalibrationConstants(sEvent.GetStation(fSingleTankId));

    for (unsigned int i = 0, n = fAccPeakCharge.size(); i < n; ++i) {
      auto& pc = fAccPeakCharge[i];
      pc.first(fPeak[i]);
      pc.second(fCharge[i]);
    }

    // Now copy the results of the calibration to a new Event, then copy back
    // into the old event to ensure only the calibration information is kept

    Event newEvent;
    newEvent.MakeSEvent();

    SEvent& newSEvent = newEvent.GetSEvent();

    for (SEvent::StationIterator sIt = sEvent.StationsBegin(), end = sEvent.StationsEnd();
         sIt != end; ++sIt) {

      if (!sIt->HasCalibData())
        continue;

      const int sId = sIt->GetId();
      newSEvent.MakeStation(sId);
      Station& newStation = newSEvent.GetStation(sId);
      newStation.MakeCalibData();
      StationCalibData& newCalibData = newStation.GetCalibData();
      newCalibData = sIt->GetCalibData();

      for (Station::ConstPMTIterator pmtIt = sIt->PMTsBegin(sdet::PMTConstants::eAnyType),
           end = sIt->PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {

        if (!pmtIt->HasCalibData())
          continue;

        // This throws an exception if there isn't a PMT for this id. There
        // should be, since they are created when stations are created, so
        // the code should die here if one doesn't exist.

        PMT& pmt = newStation.GetPMT(pmtIt->GetId());

        if (!pmt.HasCalibData())
          pmt.MakeCalibData();

        PMTCalibData& newPMTCalib = pmt.GetCalibData();
        newPMTCalib = pmtIt->GetCalibData();

        const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();
        const auto& pc = fAccPeakCharge[pmtId];
        newPMTCalib.SetVEMPeak(pc.first.GetAverage());
        newPMTCalib.SetVEMCharge(pc.second.GetAverage());

      }

    }

    event = newEvent;

    return eSuccess;
  }


  VModule::ResultFlag
  SdSimulationCalibrator::Finish()
  {

    TabularStream tab(" r r r | . . . | . . .");
    tab << "PMT" << endc << "N" << endc << "NPar" << endc << "<Peak>" << endc
        << "Err<>" << endc << "StdDev" << endc << "<Charge>" << endc << "Err<>"
        << endc << "StdDev" << endr << hline;
    int pmtId = 1;
    unsigned int numRuns = 0;
    for (vector<pair<Sigma, Sigma> >::iterator it = fAccPeakCharge.begin(),
         end = fAccPeakCharge.end(); it != end; ++it, ++pmtId) {
      tab << pmtId << endc
          << it->first.GetN() << endc
          << fNParticles << endc
          << it->first.GetAverage() << endc
          << it->first.GetAverageError() << endc
          << it->first.GetStandardDeviation() << endc
          << it->second.GetAverage() << endc
          << it->second.GetAverageError() << endc
          << it->second.GetStandardDeviation() << endr;

      numRuns = it->first.GetN();
    }
    tab << delr;

    // Pull some info from the ParticleInjector for documentation purposes.
    // NB this expects one to use ParticleInjectorNEU!
    Branch injB = CentralConfig::GetInstance()->GetTopBranch("ParticleInjector");
    const double muEnergy = injB.GetChild("Energy").GetChild("Discrete").GetChild("x").Get<double>();

    ostringstream info;
    info << "The simulated calibration constants were set as follows:\n\n"
         << tab << "\n\n"
            "The following XML snipped should be pasted into "
            "Framework/SDetector/SdSimCalibrationConstants.xml.in "
            "with the other PMT calibrations from the same simModule:\n\n"
         << "<!-- "
         << fSignature << " tank sim, "
         << numRuns << " runs, "
         << fNParticles << " particle(s) per run, "
            "energy = " << muEnergy / GeV
         << " GeV -->\n"
            "<simModule name='" << fSignature << "'>\n"
            "  <electronics isUUB='" << fIsUUB << "'>\n";

    int pId = 0;
    for (const auto& pc : fAccPeakCharge) {
      info << "    <PMT id='" << ++pId << "'>\n"
              "      <peak> " << pc.first.GetAverage() << " </peak>\n"
              "      <charge> " << pc.second.GetAverage() << " </charge>\n"
              "    </PMT>\n";
    }
    info << "  </electronics>\n</simModule>\n";

    INFO(info);

    return eSuccess;
  }


  void
  SdSimulationCalibrator::CalculateCalibrationConstants(const sevt::Station& station)
  {
    const unsigned int nPMTs =
      Detector::GetInstance().GetSDetector().GetStation(station).GetNPMTs();
    fPeak.assign(nPMTs, 0);
    fCharge.assign(nPMTs, 0);

    const unsigned int numParticles =
      station.HasSimData() ?
      station.GetSimData().ParticlesEnd() - station.GetSimData().ParticlesBegin() : 0;

    if (numParticles <= 0)
      return;

    fNParticles = numParticles;

    if (numParticles != 1 &&
	!(fHardwareType == sdet::PMTConstants::eWaterCherenkovSmall || fHardwareType == sdet::PMTConstants::eAnyType)
	){
      ERROR("Simulation calibrator is throwing more than one particle. Only use with smallPMT!");
      exit(1);
    }

    /////////////////////////
    // Opening the fake small showers TTree
    
    TFile* f = new TFile(fROOTtitle.c_str(), "UPDATE");
    TTree* t = (TTree*)f->Get("simCalibData");

    UInt_t Nparticles = 0;
    t->SetBranchAddress("Nparticles", &Nparticles);

    vector<Int_t>* particleType = NULL;
    t->SetBranchAddress("particleType", &particleType);
    vector<Double_t>* particleEnergy = NULL;
    t->SetBranchAddress("particleEnergy", &particleEnergy);
    vector<Double_t>* particlePositionX = NULL;
    t->SetBranchAddress("particlePositionX", &particlePositionX);
    vector<Double_t>* particlePositionY = NULL;
    t->SetBranchAddress("particlePositionY", &particlePositionY);
    vector<Double_t>* particlePositionZ = NULL;
    t->SetBranchAddress("particlePositionZ", &particlePositionZ);
    vector<Double_t>* particleZenith = NULL;
    t->SetBranchAddress("particleZenith", &particleZenith);
    vector<Double_t>* particleAzimuth = NULL;
    t->SetBranchAddress("particleAzimuth", &particleAzimuth);
    vector<Double_t>* particleTime = NULL;
    t->SetBranchAddress("particleTime", &particleTime);

    Int_t PE[5] = { 0 };
    t->SetBranchAddress("PE", &PE);
    Double_t peak[5] = { 0 };
    t->SetBranchAddress("peak", &peak);
    Double_t charge[5] = { 0 };
    t->SetBranchAddress("charge", &charge);
    UInt_t satBins[5] = { 0 };
    t->SetBranchAddress("saturatedBins", &satBins);

    Int_t startBin[5] = { 0 };
    t->SetBranchAddress("startBin", &startBin);
    Int_t stopBin[5] = { 0 };
    t->SetBranchAddress("stopBin", &stopBin);
    vector<UInt_t>* trace_LPMT1 = NULL;
    t->SetBranchAddress("trace_LPMT1", &trace_LPMT1);
    vector<UInt_t>* trace_LPMT2 = NULL;
    t->SetBranchAddress("trace_LPMT2", &trace_LPMT2);
    vector<UInt_t>* trace_LPMT3 = NULL;
    t->SetBranchAddress("trace_LPMT3", &trace_LPMT3);
    vector<UInt_t>* trace_SPMT = NULL;
    t->SetBranchAddress("trace_SPMT", &trace_SPMT);
    vector<UInt_t>* trace_SSD = NULL;
    t->SetBranchAddress("trace_SSD", &trace_SSD);

    /////////////////////////
    // Filling 'particles part' of the fake small showers TTree

    Nparticles = numParticles;

    const sdet::Station& detStation =
      det::Detector::GetInstance().GetSDetector().GetStation(station);
    const CoordinateSystemPtr stationCS =
      detStation.GetLocalCoordinateSystem();

    for (StationSimData::ConstParticleIterator pIt = station.GetSimData().ParticlesBegin(),
         end = station.GetSimData().ParticlesEnd(); pIt != end; ++pIt) {
      particleType->push_back(pIt->GetType());
      particleEnergy->push_back(pIt->GetKineticEnergy()/GeV);
      const Point pos = pIt->GetPosition();
      const Vector dir = pIt->GetDirection();
      particlePositionX->push_back(pos.GetX(stationCS)/meter);
      particlePositionY->push_back(pos.GetY(stationCS)/meter);
      particlePositionZ->push_back(pos.GetZ(stationCS)/meter);
      particleZenith->push_back(dir.GetTheta(stationCS));
      particleAzimuth->push_back(dir.GetPhi(stationCS));
      particleTime->push_back(pIt->GetTime()/ns);
    }
    
    /////////////////////////

    int overallStart = 100000;
    int overallStop = -10000;
    
    for (Station::ConstPMTIterator pmtIt = station.PMTsBegin(sdet::PMTConstants::eAnyType),
         end = station.PMTsEnd(sdet::PMTConstants::eAnyType); pmtIt != end; ++pmtIt) {

      if (!pmtIt->HasSimData())
        continue;

      const PMTSimData& pSim = pmtIt->GetSimData();

      if (!pSim.HasFADCTrace())
        continue;

      const unsigned int pmtId = pmtIt->GetId() - sdet::Station::GetFirstPMTId();
      const int saturationValue = detStation.GetSaturationValue();

      /////////////////////////
      // Filling 'simulated signal' part of the fake small showers TTree

      const TimeDistributionI& PETD = pSim.GetPETimeDistribution(sevt::StationConstants::eTotal);
      int totPE = 0;
      for (TimeDistributionI::SparseIterator iit = PETD.SparseBegin(), end = PETD.SparseEnd(); iit != end; ++iit)
        totPE += iit->get<1>();
      PE[pmtId] = totPE;

      /////////////////////////
      
      const sdet::PMT& detPMT =
        Detector::GetInstance().GetSDetector().GetStation(station).GetPMT(*pmtIt);

      const double baselineHG = detPMT.GetBaseline(sdet::PMTConstants::eHighGain);
      const double baselineLG = detPMT.GetBaseline(sdet::PMTConstants::eLowGain);

      const TimeDistributionI& fadcHigh = pSim.GetFADCTrace(PMTConstants::eHighGain, sevt::StationConstants::eTotal);
      const TimeDistributionI& fadcLow = pSim.GetFADCTrace(PMTConstants::eLowGain, sevt::StationConstants::eTotal);

      fPeak[pmtId] = 0;
      fCharge[pmtId] = 0;
      
      // only process desired pmt type, unless all
      //if (!(fHardwareType == sdet::PMTConstants::eAnyType || detPMT.GetType() == fHardwareType))
      //continue;
	
      // HIGH GAIN part
      // narrow down the signal region
      const double threshold = baselineHG + 15;
      const int start = fadcHigh.GetStart();
      int signalStart = start;
      const int stop = fadcHigh.GetStop();
      for ( ; signalStart <= stop; ++signalStart)
	if (fadcHigh.At(signalStart) >= threshold)
	  break;
      int signalStop = start;
      for (int i = signalStart + 1; i <= stop; ++i) {
	if (fadcHigh.At(i) >= threshold) {
	  signalStop = start;
	  continue;
	}
	if (signalStop == start && fadcHigh.At(i) < threshold)
	  signalStop = i;
      }
      if (signalStop == start) {
	ostringstream warn;
	warn << "No signal found in PMT " << pmtId;
	WARNING(warn);
	continue;
      }
      signalStart = max(signalStart - 10, start);
      signalStop = min(signalStop + 20, stop);

      if( detPMT.GetType() == sdet::PMTConstants::eWaterCherenkovLarge ){
	if(overallStart > signalStart) overallStart = signalStart;
	if(overallStop < signalStop) overallStop = signalStop;
      }
      
      double maxVal = -baselineHG;
      double ch = 0;
      unsigned int saturatedBins = 0;
      for (int i = signalStart; i <= signalStop; ++i) {
	const double val = fadcHigh.At(i) - baselineHG;
	if (val > maxVal)
	  maxVal = val;
	ch += val;
	if(fadcHigh.At(i) >= saturationValue) saturatedBins++;
      }
      
      fPeak[pmtId] = maxVal;
      fCharge[pmtId] = ch;
      if (numParticles != 1) {
	const double correctionFactor = 1. / numParticles;
	fPeak[pmtId] *= correctionFactor;
	fCharge[pmtId] *= correctionFactor;
      }
      
      if( fHardwareType == sdet::PMTConstants::eWaterCherenkovLarge ){
	
	if(pmtId==0){
	  for (int i = fadcHigh.GetStart(); i < fadcHigh.GetStop(); ++i)
	    trace_LPMT1->push_back(fadcHigh.At(i));
	} else if(pmtId==1){
	  for (int i = fadcHigh.GetStart(); i < fadcHigh.GetStop(); ++i)
	    trace_LPMT2->push_back(fadcHigh.At(i));
	} else if(pmtId==2){
	  for (int i = fadcHigh.GetStart(); i < fadcHigh.GetStop(); ++i)
	    trace_LPMT3->push_back(fadcHigh.At(i));
	}
	
	startBin[pmtId] = signalStart - fadcHigh.GetStart();
	stopBin[pmtId] = signalStop - fadcHigh.GetStart();
	peak[pmtId] = maxVal;
	charge[pmtId] = ch;
	satBins[pmtId] = saturatedBins;
	
      } // close HIGH GAIN for LPMTs
      else{

	if(pmtId==0){
	  for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	    trace_LPMT1->push_back(fadcLow.At(i));
	} else if(pmtId==1){
	  for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	    trace_LPMT2->push_back(fadcLow.At(i));
	} else if(pmtId==2){
	  for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	    trace_LPMT3->push_back(fadcLow.At(i));
	} else if(pmtId==3){
	  for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	    trace_SPMT->push_back(fadcLow.At(i));
	} else if(pmtId==4){
	  for (int i = fadcLow.GetStart(); i < fadcLow.GetStop(); ++i)
	    trace_SSD->push_back(fadcLow.At(i));
	}

	maxVal = -baselineLG;
	ch = 0;
	saturatedBins = 0;

	if( detPMT.GetType() != sdet::PMTConstants::eWaterCherenkovLarge ){
	  signalStart = overallStart;
	  signalStop = overallStop;
	}
	
	for (int i = signalStart; i <= signalStop; ++i) {
	  const double val = fadcLow.At(i) - baselineLG;
	  if (val > maxVal)
	    maxVal = val;
	  ch += val;
	  if(fadcLow.At(i) >= saturationValue) saturatedBins++;
	}
	
        startBin[pmtId] = signalStart - fadcLow.GetStart();
        stopBin[pmtId] = signalStop - fadcLow.GetStart();
        peak[pmtId] = maxVal;
        charge[pmtId] = ch;
        satBins[pmtId] = saturatedBins;

      } // close LOW GAIN
    } // close loop over station PMTs

    t->Fill();
    t->Write(0,TObject::kWriteDelete,0);
    f->Close("R");
    delete f;
    
  }

}
